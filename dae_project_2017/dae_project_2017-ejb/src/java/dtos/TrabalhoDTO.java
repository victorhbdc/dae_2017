package dtos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import utils.EstadoTrabalho;
import utils.TipoProponente;
import utils.TipoTrabalho;
import utils.embeddable.Documento;

@XmlRootElement(name = "Trabalho")
@XmlAccessorType(XmlAccessType.FIELD)
public class TrabalhoDTO implements Serializable {

    private Long id;
    private TipoTrabalho tipoTrabalho;
    private String titulo;
    private String resumo;
    private EstadoTrabalho estadoTrabalho;
    private List<String> professorUsernames;
    private String supervisorNome;
    private String supervisorEmail;
    private Date dataEntrega;
    private String estudanteUsername;
    private Long provaPublicaId;
    private DocumentoDTO provaPublicaAta;
    private PropostaDTO proposta;
    private List<Long> candidaturaIds;
    private TipoProponente tipoProponente;
    private List<DocumentoDTO> artefactos;

    public TrabalhoDTO() {
        this.professorUsernames = new LinkedList<>();
        this.candidaturaIds = new LinkedList<>();
        this.artefactos = new LinkedList<>();
    }

    public DocumentoDTO getProvaPublicaAta() {
        return provaPublicaAta;
    }

    public void setProvaPublicaAta(DocumentoDTO provaPublicaAta) {
        this.provaPublicaAta = provaPublicaAta;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoTrabalho getTipoTrabalho() {
        return tipoTrabalho;
    }

    public void setTipoTrabalho(TipoTrabalho tipoTrabalho) {
        this.tipoTrabalho = tipoTrabalho;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public EstadoTrabalho getEstadoTrabalho() {
        return estadoTrabalho;
    }

    public void setEstadoTrabalho(EstadoTrabalho estadoTrabalho) {
        this.estadoTrabalho = estadoTrabalho;
    }

    public List<String> getProfessorUsernames() {
        return professorUsernames;
    }

    public void setProfessorUsernames(List<String> professorUsernames) {
        this.professorUsernames = professorUsernames;
    }
    
    public void addProfessorUsername(String professorUsername){
        if(professorUsername == null || professorUsername.isEmpty()){
            return;
        }
        this.professorUsernames.add(professorUsername);
    }
    
    public void removeProfessorUsername(String professorUsername){
        this.professorUsernames.remove(professorUsername);
    }

    public String getSupervisorNome() {
        return supervisorNome;
    }

    public void setSupervisorNome(String supervisorNome) {
        this.supervisorNome = supervisorNome;
    }

    public String getSupervisorEmail() {
        return supervisorEmail;
    }

    public void setSupervisorEmail(String supervisorEmail) {
        this.supervisorEmail = supervisorEmail;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }
    
    public String getDataEntregaString(){
            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
            fmt.setCalendar(new GregorianCalendar());
            return fmt.format(dataEntrega);
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public String getEstudanteUsername() {
        return estudanteUsername;
    }

    public void setEstudanteUsername(String estudanteUsername) {
        this.estudanteUsername = estudanteUsername;
    }

    public Long getProvaPublicaId() {
        return provaPublicaId;
    }

    public void setProvaPublicaId(Long provaPublicaId) {
        this.provaPublicaId = provaPublicaId;
    }

    public PropostaDTO getProposta() {
        return proposta;
    }

    public void setProposta(PropostaDTO proposta) {
        this.proposta = proposta;
    }

    public List<Long> getCandidaturaIds() {
        return candidaturaIds;
    }

    public void setCandidaturaIds(List<Long> candidaturaIds) {
        this.candidaturaIds = candidaturaIds;
    }
    
    public void addCandidaturaId(Long candidaturaId){
        if(candidaturaId == null){
            return;
        }
        this.candidaturaIds.add(candidaturaId);
    }
    
    public void removeCandidaturaId(Long candidaturaId){
        this.candidaturaIds.remove(candidaturaId);
    }

    public TipoProponente getTipoProponente() {
        return tipoProponente;
    }

    public void setTipoProponente(TipoProponente tipoProponente) {
        this.tipoProponente = tipoProponente;
    }

    public List<DocumentoDTO> getArtefactos() {
        return artefactos;
    }
    
    public List<DocumentoDTO> getArtefactosPublicos(){
        List<DocumentoDTO> artefactosPublicos = new LinkedList<>();
        for(DocumentoDTO d : artefactos){
            if(d.getTipoDeDocumento() == Documento.TipoDeDocumento.RELATORIO){
                artefactosPublicos.add(d);
            }
        }
        return artefactosPublicos;
    }

    public void setArtefactos(List<DocumentoDTO> artefactos) {
        this.artefactos = artefactos;
    }
    
    public void addArtefacto(DocumentoDTO artefacto){
        if(artefacto == null){
            return;
        }
        this.artefactos.add(artefacto);
    }
    
    public void removeArtefacto(DocumentoDTO artefacto){
        this.artefactos.remove(artefacto);
    }
    
    public void reset(){
        id = null;
        tipoTrabalho = null;
        titulo = null;
        resumo = null;
        estadoTrabalho = null;
        professorUsernames = new LinkedList<>();
        supervisorNome = null;
        supervisorEmail = null;
        dataEntrega = null;
        estudanteUsername = null;
        provaPublicaId = null;
        proposta = null;
        candidaturaIds = new LinkedList<>();
        this.tipoProponente = null;
        this.artefactos = new LinkedList<>();
    }
}
