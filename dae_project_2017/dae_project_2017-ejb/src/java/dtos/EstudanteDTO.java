package dtos;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Estudante") 
@XmlAccessorType(XmlAccessType.FIELD)
public class EstudanteDTO extends UserDTO {

    public EstudanteDTO() {
        super();
    }

    public EstudanteDTO(String mestrado, String username, String password, String nome, String email) {
        super(username, password, nome, email);
    }
    
    
}