/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dtos;

import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import utils.embeddable.Documento.TipoDeDocumento;

@XmlRootElement(name = "Documento")
@XmlAccessorType(XmlAccessType.FIELD)
public class DocumentoDTO implements Serializable {

    private String nome;
    private Date uploadedAt;
    private String path;
    private String mimeType;
    private TipoDeDocumento tipoDeDocumento;

    public DocumentoDTO() {
    }

    public DocumentoDTO(String nome, Date uploadedAt, String path, String mimeType) {
        this.nome = nome;
        this.uploadedAt = uploadedAt;
        this.path = path;
        this.mimeType = mimeType;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getUploadedAt() {
        return uploadedAt;
    }

    public void setUploadedAt(Date uploadedAt) {
        this.uploadedAt = uploadedAt;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public TipoDeDocumento getTipoDeDocumento() {
        return tipoDeDocumento;
    }

    public void setTipoDeDocumento(TipoDeDocumento tipoDeDocumento) {
        this.tipoDeDocumento = tipoDeDocumento;
    }
    
    
}
