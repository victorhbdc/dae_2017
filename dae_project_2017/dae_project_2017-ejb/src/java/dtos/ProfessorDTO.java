package dtos;

import java.util.Collection;
import java.util.LinkedList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Professor") 
@XmlAccessorType(XmlAccessType.FIELD)
public class ProfessorDTO extends UserDTO {

    Collection<Long> propostasIds;

    public ProfessorDTO() {
        super();
        propostasIds = new LinkedList<>();
    }

    public ProfessorDTO(String username, String password, String nome, String email) {
        super(username, password, nome, email);
        propostasIds = new LinkedList<>();
    }

    @Override
    public void reset() {
        super.reset(); 
        this.setPropostasIds(new LinkedList<>());
    }

    
    public Collection<Long> getPropostasIds() {
        return propostasIds;
    }

    public void setPropostasIds(Collection<Long> propostasIds) {
        this.propostasIds = propostasIds;
    }
    
    public void addPropostaId(Long propostaId){
        if(propostaId == null){
            return;
        }
        this.propostasIds.add(propostaId);
    }
    
    public void removePropostaId(Long propostaId){
        this.propostasIds.remove(propostaId);
    }
}