package dtos;

import java.util.GregorianCalendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import utils.embeddable.EstadoConcurso;

@XmlRootElement(name = "Concurso")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConcursoDTO {

    private Long id;
    private EstadoConcurso estadoConcurso;
    private GregorianCalendar dataAberturaDoConcurso;

    // todo those below are not being converted
    private GregorianCalendar periodoPropostas_inicio;
    private GregorianCalendar periodoPropostas_fim;
    private GregorianCalendar periodoValidacao_inicio;
    private GregorianCalendar periodoValidacao_fim;
    private GregorianCalendar periodoCandidaturas_inicio;
    private GregorianCalendar periodoCandidaturas_fim;
    private GregorianCalendar periodoSeriacao_inicio;
    private GregorianCalendar periodoSeriacao_fim;
    private GregorianCalendar periodoEntregas_inicio;
    private GregorianCalendar periodoEntregas_fim;
    private GregorianCalendar periodoMarcacaoProvasPublicas_inicio;
    private GregorianCalendar periodoMarcacaoProvasPublicas_fim;

    public ConcursoDTO() {
    }

    public ConcursoDTO(Long id, EstadoConcurso estadoConcurso, GregorianCalendar dataAberturaDoConcurso, GregorianCalendar periodoPropostas_inicio, GregorianCalendar periodoPropostas_fim, GregorianCalendar periodoValidacao_inicio, GregorianCalendar periodoValidacao_fim, GregorianCalendar periodoCandidaturas_inicio, GregorianCalendar periodoCandidaturas_fim, GregorianCalendar periodoSeriacao_inicio, GregorianCalendar periodoSeriacao_fim, GregorianCalendar periodoEntregas_inicio, GregorianCalendar periodoEntregas_fim, GregorianCalendar periodoMarcacaoProvasPublicas_inicio, GregorianCalendar periodoMarcacaoProvasPublicas_fim) {
        this.id = id;
        this.estadoConcurso = estadoConcurso;
        this.dataAberturaDoConcurso = dataAberturaDoConcurso;
        this.periodoPropostas_inicio = periodoPropostas_inicio;
        this.periodoPropostas_fim = periodoPropostas_fim;
        this.periodoValidacao_inicio = periodoValidacao_inicio;
        this.periodoValidacao_fim = periodoValidacao_fim;
        this.periodoCandidaturas_inicio = periodoCandidaturas_inicio;
        this.periodoCandidaturas_fim = periodoCandidaturas_fim;
        this.periodoSeriacao_inicio = periodoSeriacao_inicio;
        this.periodoSeriacao_fim = periodoSeriacao_fim;
        this.periodoEntregas_inicio = periodoEntregas_inicio;
        this.periodoEntregas_fim = periodoEntregas_fim;
        this.periodoMarcacaoProvasPublicas_inicio = periodoMarcacaoProvasPublicas_inicio;
        this.periodoMarcacaoProvasPublicas_fim = periodoMarcacaoProvasPublicas_fim;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoConcurso getEstadoConcurso() {
        return estadoConcurso;
    }

    public void setEstadoConcurso(EstadoConcurso estadoConcurso) {
        this.estadoConcurso = estadoConcurso;
    }

    public GregorianCalendar getDataAberturaDoConcurso() {
        return dataAberturaDoConcurso;
    }

    public void setDataAberturaDoConcurso(GregorianCalendar dataAberturaDoConcurso) {
        this.dataAberturaDoConcurso = dataAberturaDoConcurso;
    }

    public GregorianCalendar getPeriodoPropostas_inicio() {
        return periodoPropostas_inicio;
    }

    public void setPeriodoPropostas_inicio(GregorianCalendar periodoPropostas_inicio) {
        this.periodoPropostas_inicio = periodoPropostas_inicio;
    }

    public GregorianCalendar getPeriodoPropostas_fim() {
        return periodoPropostas_fim;
    }

    public void setPeriodoPropostas_fim(GregorianCalendar periodoPropostas_fim) {
        this.periodoPropostas_fim = periodoPropostas_fim;
    }

    public GregorianCalendar getPeriodoValidacao_inicio() {
        return periodoValidacao_inicio;
    }

    public void setPeriodoValidacao_inicio(GregorianCalendar periodoValidacao_inicio) {
        this.periodoValidacao_inicio = periodoValidacao_inicio;
    }

    public GregorianCalendar getPeriodoValidacao_fim() {
        return periodoValidacao_fim;
    }

    public void setPeriodoValidacao_fim(GregorianCalendar periodoValidacao_fim) {
        this.periodoValidacao_fim = periodoValidacao_fim;
    }

    public GregorianCalendar getPeriodoCandidaturas_inicio() {
        return periodoCandidaturas_inicio;
    }

    public void setPeriodoCandidaturas_inicio(GregorianCalendar periodoCandidaturas_inicio) {
        this.periodoCandidaturas_inicio = periodoCandidaturas_inicio;
    }

    public GregorianCalendar getPeriodoCandidaturas_fim() {
        return periodoCandidaturas_fim;
    }

    public void setPeriodoCandidaturas_fim(GregorianCalendar periodoCandidaturas_fim) {
        this.periodoCandidaturas_fim = periodoCandidaturas_fim;
    }

    public GregorianCalendar getPeriodoSeriacao_inicio() {
        return periodoSeriacao_inicio;
    }

    public void setPeriodoSeriacao_inicio(GregorianCalendar periodoSeriacao_inicio) {
        this.periodoSeriacao_inicio = periodoSeriacao_inicio;
    }

    public GregorianCalendar getPeriodoSeriacao_fim() {
        return periodoSeriacao_fim;
    }

    public void setPeriodoSeriacao_fim(GregorianCalendar periodoSeriacao_fim) {
        this.periodoSeriacao_fim = periodoSeriacao_fim;
    }

    public GregorianCalendar getPeriodoEntregas_inicio() {
        return periodoEntregas_inicio;
    }

    public void setPeriodoEntregas_inicio(GregorianCalendar periodoEntregas_inicio) {
        this.periodoEntregas_inicio = periodoEntregas_inicio;
    }

    public GregorianCalendar getPeriodoEntregas_fim() {
        return periodoEntregas_fim;
    }

    public void setPeriodoEntregas_fim(GregorianCalendar periodoEntregas_fim) {
        this.periodoEntregas_fim = periodoEntregas_fim;
    }

    public GregorianCalendar getPeriodoMarcacaoProvasPublicas_inicio() {
        return periodoMarcacaoProvasPublicas_inicio;
    }

    public void setPeriodoMarcacaoProvasPublicas_inicio(GregorianCalendar periodoMarcacaoProvasPublicas_inicio) {
        this.periodoMarcacaoProvasPublicas_inicio = periodoMarcacaoProvasPublicas_inicio;
    }

    public GregorianCalendar getPeriodoMarcacaoProvasPublicas_fim() {
        return periodoMarcacaoProvasPublicas_fim;
    }

    public void setPeriodoMarcacaoProvasPublicas_fim(GregorianCalendar periodoMarcacaoProvasPublicas_fim) {
        this.periodoMarcacaoProvasPublicas_fim = periodoMarcacaoProvasPublicas_fim;
    }
    
    
}
