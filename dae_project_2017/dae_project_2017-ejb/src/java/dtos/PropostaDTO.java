package dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import utils.Estado;
import utils.TipoTrabalho;
import utils.embeddable.ItemBibliografico;

@XmlRootElement(name = "Proposta")
@XmlAccessorType(XmlAccessType.FIELD)
public class PropostaDTO implements Serializable {

    //------Campos obrigatorios------
    Long id;
    String mestrado;
    Long concursoId;
    TipoTrabalho tipoTrabalho;
    String titulo;
    String resumo;
    String planoTrabalho;
    BigDecimal orcamentoBibliografia;
    BigDecimal orcamentoMaterial;
    BigDecimal orcamentoEquipamento;
    BigDecimal orcamentoOutros;
    List<String> proponenteUsernames;
    List<String> areasCientificas;
    Estado estado;
    //----------Campos nao obrigatorio---------
    List<String> objectivos;
    List<String> requisitosTrabalho;
    List<String> apoios;
    String localRealizacao;
    List<ItemBibliografico> bibliografia;
    //---------Campos preenchidos por um mccp
    String observacoes;
    Long trabalhoId;

    public PropostaDTO() {
        this.proponenteUsernames = new LinkedList<>();
        this.areasCientificas = new LinkedList<>();
        this.objectivos = new LinkedList<>();
        this.requisitosTrabalho = new LinkedList<>();
        this.apoios = new LinkedList<>();
        this.bibliografia = new LinkedList<>();
    }

    public PropostaDTO(String mestrado, Long concursoId,
            TipoTrabalho tipoTrabalho, String titulo, String resumo,
            BigDecimal orcamento_bibliografia, BigDecimal orcamento_material,
            BigDecimal orcamento_equipamento, BigDecimal orcamento_outros, 
            String planoTrabalho, List<String> areasCientificas, List<String> proponenteUsernames
    ) {
        this.objectivos = new LinkedList<>();
        this.requisitosTrabalho = new LinkedList<>();
        this.apoios = new LinkedList<>();
        this.bibliografia = new LinkedList<>();
        this.areasCientificas = areasCientificas;
        this.proponenteUsernames = proponenteUsernames;
        this.estado = Estado.ANALISE;
        this.mestrado = mestrado;
        this.concursoId = concursoId;
        this.tipoTrabalho = tipoTrabalho;
        this.titulo = titulo;
        this.resumo = resumo;
        this.planoTrabalho = planoTrabalho;
        this.orcamentoBibliografia = orcamento_bibliografia;
        this.orcamentoMaterial = orcamento_material;
        this.orcamentoEquipamento = orcamento_equipamento;
        this.orcamentoOutros = orcamento_outros;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMestrado() {
        return mestrado;
    }

    public void setMestrado(String mestrado) {
        this.mestrado = mestrado;
    }

    public Long getConcursoId() {
        return concursoId;
    }

    public void setConcursoId(Long concursoId) {
        this.concursoId = concursoId;
    }

    public TipoTrabalho getTipoTrabalho() {
        return tipoTrabalho;
    }

    public void setTipoTrabalho(TipoTrabalho tipoTrabalho) {
        this.tipoTrabalho = tipoTrabalho;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public BigDecimal getOrcamentoBibliografia() {
        return orcamentoBibliografia;
    }

    public void setOrcamentoBibliografia(BigDecimal orcamentoBibliografia) {
        this.orcamentoBibliografia = orcamentoBibliografia;
    }

    public BigDecimal getOrcamentoMaterial() {
        return orcamentoMaterial;
    }

    public void setOrcamentoMaterial(BigDecimal orcamentoMaterial) {
        this.orcamentoMaterial = orcamentoMaterial;
    }

    public BigDecimal getOrcamentoEquipamento() {
        return orcamentoEquipamento;
    }

    public void setOrcamentoEquipamento(BigDecimal orcamentoEquipamento) {
        this.orcamentoEquipamento = orcamentoEquipamento;
    }

    public BigDecimal getOrcamentoOutros() {
        return orcamentoOutros;
    }

    public void setOrcamentoOutros(BigDecimal orcamentoOutros) {
        this.orcamentoOutros = orcamentoOutros;
    }

    public List<String> getProponenteUsernames() {
        return proponenteUsernames;
    }

    public void setProponenteUsernames(List<String> proponenteUsernames) {
        this.proponenteUsernames = proponenteUsernames;
    }

    public void addProponenteUsername(String proponenteUsername) {
        if(proponenteUsername == null || proponenteUsername.isEmpty()){
            return;
        }
        this.proponenteUsernames.add(proponenteUsername);
    }

    public void removeProponenteUsername(String proponenteUsername) {
        this.proponenteUsernames.remove(proponenteUsername);
    }

    public String getPlanoTrabalho() {
        return planoTrabalho;
    }

    public void setPlanoTrabalho(String planoTrabalho) {
        this.planoTrabalho = planoTrabalho;
    }
    
    public List<String> getAreasCientificas() {
        return areasCientificas;
    }

    public void setAreasCientificas(List<String> areasCientificas) {
        this.areasCientificas = areasCientificas;
    }

    public void addAreaCientificas(String areaCientifica) {
        if(areaCientifica == null || areaCientifica.isEmpty()){
            return;
        }
        this.areasCientificas.add(areaCientifica);
    }

    public void removeAreaCientificas(String areaCientifica) {
        this.areasCientificas.remove(areaCientifica);
    }
    // --------------------NAO OBRIGATORIOS---------------------

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Long getTrabalhoId() {
        return trabalhoId;
    }

    public void setTrabalhoId(Long trabalhoId) {
        this.trabalhoId = trabalhoId;
    }

    public List<String> getRequisitosTrabalho() {
        return requisitosTrabalho;
    }

    public void setRequisitosTrabalho(List<String> requisitosTrabalho) {
        this.requisitosTrabalho = requisitosTrabalho;
    }

    public void addRequisitoTrabalho(String requisitoTrabalho) {
        if(requisitoTrabalho == null || requisitoTrabalho.isEmpty()){
            return;
        }
        this.requisitosTrabalho.add(requisitoTrabalho);
    }

    public void removeRequisitoTrabalho(String requisitoTrabalho) {
        this.requisitosTrabalho.remove(requisitoTrabalho);
    }

    public List<String> getApoios() {
        return apoios;
    }

    public void setApoios(List<String> apoios) {
        this.apoios = apoios;
    }

    public void addApoio(String apoio) {
        if(apoio == null || apoio.isEmpty()){
            return;
        }
        this.apoios.add(apoio);
    }

    public void removeApoio(String apoio) {
        this.apoios.remove(apoio);
    }

    public String getLocalRealizacao() {
        return localRealizacao;
    }

    public void setLocalRealizacao(String localRealizacao) {
        this.localRealizacao = localRealizacao;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public List<ItemBibliografico> getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(List<ItemBibliografico> bibliografia) {
        this.bibliografia = bibliografia;
    }

    public void addItemBibliografico(ItemBibliografico itemBibliografico) {
        if(itemBibliografico == null || itemBibliografico.getAutor().isEmpty() || 
                itemBibliografico.getTitulo().isEmpty()){
            return;
        }
        this.bibliografia.add(itemBibliografico);
    }
    
    public void addItemBibliograficoStrings(String titulo, String autor) {
        if(titulo == null || titulo.isEmpty() || autor == null || autor.isEmpty()){
            return;
        }
        this.bibliografia.add(new ItemBibliografico(titulo, autor));
    }

    public void removeItemBibliografico(ItemBibliografico itemBibliografico) {
        this.bibliografia.remove(itemBibliografico);
    }

    public List<String> getObjectivos() {
        return objectivos;
    }

    public void setObjectivos(List<String> objectivos) {
        this.objectivos = objectivos;
    }

    public void addObjectivos(String o) {
        if(o == null || o.isEmpty()){
            return;
        }
        this.objectivos.add(o);
    }

    public void removeObjectivos(String o) {
        this.objectivos.remove(o);
    }

    public void reset() {
        this.id = null;
        this.mestrado = null;
        this.concursoId = null;
        this.tipoTrabalho = null;
        this.titulo = null;
        this.resumo = null;
        this.orcamentoBibliografia = null;
        this.orcamentoMaterial = null;
        this.orcamentoEquipamento = null;
        this.orcamentoOutros = null;
        this.planoTrabalho = null;
        this.areasCientificas = new LinkedList<>();
        this.proponenteUsernames = new LinkedList<>();
        this.estado = Estado.ANALISE;
        //NAO OBRIGATORIOS
        this.bibliografia = new LinkedList<>();
        this.requisitosTrabalho = new LinkedList<>();
        this.apoios = new LinkedList<>();
        this.objectivos = new LinkedList<>();
        this.localRealizacao = null;
        //MCCP
        this.trabalhoId = null;
        this.observacoes = null;
    }

    @Override
    public String toString() {
        return "PropostaDTO{" + "id=" + id + ", mestrado=" + mestrado + 
                ", concursoId=" + concursoId + ", tipoTrabalho=" + tipoTrabalho + 
                ", titulo=" + titulo + ", resumo=" + resumo + ", orcamento_bibliografia=" + orcamentoBibliografia + 
                ", orcamento_material=" + orcamentoMaterial + 
                ", orcamento_equipamento=" + orcamentoEquipamento + ", orcamento_outros=" + orcamentoOutros + 
                ", proponenteUsernames=" + proponenteUsernames + ", estado=" + estado + 
                ", areasCientificas=" + areasCientificas + ", objectivos=" + objectivos + 
                ", requisitosTrabalho=" + requisitosTrabalho + ", apoios=" + apoios + 
                ", localRealizacao=" + localRealizacao + ", observacoes=" + observacoes + 
                ", planoTrabalho=" + planoTrabalho + ", bibliografia=" + bibliografia +
                ", trabalhoId=" + trabalhoId + '}';
    }  
}
