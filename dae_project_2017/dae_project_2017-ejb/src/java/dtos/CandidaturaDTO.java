package dtos;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import utils.Estado;

@XmlRootElement(name = "Candidatura") 
@XmlAccessorType(XmlAccessType.FIELD)
public class CandidaturaDTO {

    Long id;
    String estudanteUsername;
    Long trabalhoId;
    String trabalhoTitulo;
    BigDecimal media;
    Estado estado;
    private List<DocumentoDTO> documentos;
    
    public CandidaturaDTO(){
        this.documentos = new LinkedList<>();
    }
    
    public CandidaturaDTO(String estudanteUsername, Long trabalhoId,
            String trabalhoTitulo, BigDecimal media) {
        this();
        this.estudanteUsername = estudanteUsername;
        this.trabalhoTitulo = trabalhoTitulo;
        this.trabalhoId = trabalhoId;
        this.media = media;
        this.estado = Estado.ANALISE;
    }
    
    public CandidaturaDTO(String estudanteUsername, Long trabalhoId,
            String trabalhoTitulo, BigDecimal media, List<DocumentoDTO> documentos) {
        this.estudanteUsername = estudanteUsername;
        this.trabalhoId = trabalhoId;
        this.trabalhoTitulo = trabalhoTitulo;
        this.documentos = documentos;
        this.media = media;
        this.estado = Estado.ANALISE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<DocumentoDTO> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoDTO> documentos) {
        this.documentos = documentos;
    }
    
    public void addDocumento(DocumentoDTO documento) {
        if(documento == null){
            return;
        }
        this.documentos.add(documento);
    }

    public void removeDocumento(DocumentoDTO documento) {
        this.documentos.remove(documento);
    }
    
    public String getEstudanteUsername() {
        return estudanteUsername;
    }

    public void setEstudanteUsername(String estudanteUsername) {
        this.estudanteUsername = estudanteUsername;
    }

    public Long getTrabalhoId() {
        return trabalhoId;
    }

    public void setTrabalhoId(Long trabalhoId) {
        this.trabalhoId = trabalhoId;
    }

    public String getTrabalhoTitulo() {
        return trabalhoTitulo;
    }

    public void setTrabalhoTitulo(String trabalhoTitulo) {
        this.trabalhoTitulo = trabalhoTitulo;
    }

    public BigDecimal getMedia() {
        return media;
    }

    public void setMedia(BigDecimal media) {
        this.media = media;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
    public void reset() {
        this.id = null;
        this.estudanteUsername = null;
        this.trabalhoId= null;
        this.documentos = new LinkedList<>();
        this.trabalhoTitulo = null;
        this.media = null;
        this.estado = Estado.ANALISE;
    }
}