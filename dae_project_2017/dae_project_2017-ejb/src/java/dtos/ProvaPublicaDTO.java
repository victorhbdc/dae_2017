package dtos;

import entities.trabalhos.ProvaPublica.EstadoProvaPublica;
import java.io.Serializable;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ProvaPublica")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProvaPublicaDTO implements Serializable {

    private Long id;

    private Date data;

    private String local;

    private String juriOrientadorUsername;
    
    private String juriOrientadorNome;

    private String juriProfessorEmail;
    
    private String juriProfessorNome;

    private String juriMembroCCPUsername;
    
    private String juriMembroCCPNome;

    private Long trabalhoId;

    private DocumentoDTO ata;

    private EstadoProvaPublica estadoProvaPublica;

    //***
    public ProvaPublicaDTO() {
    }

    //***
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Long getTrabalhoId() {
        return trabalhoId;
    }

    public void setTrabalhoId(Long trabalhoId) {
        this.trabalhoId = trabalhoId;
    }
    
    public DocumentoDTO getAta() {
        return ata;
    }

    public void setAta(DocumentoDTO ata) {
        this.ata = ata;
    }

    public EstadoProvaPublica getEstadoProvaPublica() {
        return estadoProvaPublica;
    }

    public void setEstadoProvaPublica(EstadoProvaPublica estadoProvaPublica) {
        this.estadoProvaPublica = estadoProvaPublica;
    }

    public String getJuriOrientadorUsername() {
        return juriOrientadorUsername;
    }

    public void setJuriOrientadorUsername(String juriOrientadorUsername) {
        this.juriOrientadorUsername = juriOrientadorUsername;
    }

    public String getJuriProfessorEmail() {
        return juriProfessorEmail;
    }

    public void setJuriProfessorEmail(String juriProfessorEmail) {
        this.juriProfessorEmail = juriProfessorEmail;
    }

    public String getJuriMembroCCPUsername() {
        return juriMembroCCPUsername;
    }

    public void setJuriMembroCCPUsername(String juriMembroCCPUsername) {
        this.juriMembroCCPUsername = juriMembroCCPUsername;
    }

    public String getJuriOrientadorNome() {
        return juriOrientadorNome;
    }

    public void setJuriOrientadorNome(String juriOrientadorNome) {
        this.juriOrientadorNome = juriOrientadorNome;
    }

    public String getJuriProfessorNome() {
        return juriProfessorNome;
    }

    public void setJuriProfessorNome(String juriProfessorNome) {
        this.juriProfessorNome = juriProfessorNome;
    }

    public String getJuriMembroCCPNome() {
        return juriMembroCCPNome;
    }

    public void setJuriMembroCCPNome(String juriMembroCCPNome) {
        this.juriMembroCCPNome = juriMembroCCPNome;
    }

    
    public void reset() {
        setId(null);
        setData(null);
        setLocal(null);
        setJuriOrientadorUsername(null);
        setJuriProfessorEmail(null);
        setJuriMembroCCPUsername(null);
        setJuriOrientadorNome(null);
        setJuriProfessorNome(null);
        setJuriMembroCCPNome(null);
        setTrabalhoId(null);
        setAta(null);
        setEstadoProvaPublica(null);
    }

}
