package dtos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Instituicao")
@XmlAccessorType(XmlAccessType.FIELD)
public class InstituicaoDTO extends UserDTO {
    
    private String tipoInstituicao;
    
    public InstituicaoDTO() {
        super();
    }
    
    public InstituicaoDTO(String username, String password,
            String nome, String email, String tipoInstituicao) {
        super(username, password, nome, email);
        this.tipoInstituicao = tipoInstituicao;
    }
    
    public String getTipoInstituicao() {
        return tipoInstituicao;
    }
    
    public void setTipoInstituicao(String tipoInstituicao) {
        this.tipoInstituicao = tipoInstituicao;
    }
    
    @Override
    public void reset() {
        super.reset();        
        this.setTipoInstituicao(null);
    }
    
}
