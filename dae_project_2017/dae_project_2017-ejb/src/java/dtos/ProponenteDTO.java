package dtos;

import java.util.Collection;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Proponente") 
@XmlAccessorType(XmlAccessType.FIELD)
public class ProponenteDTO extends UserDTO {

    Collection<Long> propostasIds;

    public ProponenteDTO() {
        super();
    }

    public ProponenteDTO(String username, String password,
            String nome, String email, Collection<Long> propostasIds) {
        super(username, password, nome, email);
        this.propostasIds = propostasIds;
    }

    public Collection<Long> getPropostasIds() {
        return propostasIds;
    }

    public void setPropostasIds(Collection<Long> propostasIds) {
        this.propostasIds = propostasIds;
    }

}
