package dtos;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Juri")
@XmlAccessorType(XmlAccessType.FIELD)
public class JuriDTO implements Serializable {

    private Long id;

    private MembroComissaoCPDTO membroCCP;

    private ProfessorDTO orientador;

    private String professorExternoNome;
    private String professorExternoEmail;

    private Long provaPublica;

    public JuriDTO(Long provaPublicaID, ProfessorDTO professorDTO,
            ProfessorDTO orientadorDTO, MembroComissaoCPDTO membroCCPDTO) {
        //this.professor = professorDTO;
        this.orientador = orientadorDTO;
        this.membroCCP = membroCCPDTO;
        this.provaPublica = provaPublicaID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public String getProfessorExternoNome() {
        return professorExternoNome;
    }

    public void setProfessorExternoNome(String professorExternoNome) {
        this.professorExternoNome = professorExternoNome;
    }

    public String getProfessorExternoEmail() {
        return professorExternoEmail;
    }

    public void setProfessorExternoEmail(String professorExternoEmail) {    
        this.professorExternoEmail = professorExternoEmail;
    }

    public ProfessorDTO getOrientador() {
        return orientador;
    }

    public void setOrientador(ProfessorDTO orientador) {
        this.orientador = orientador;
    }

    public MembroComissaoCPDTO getMembroCCP() {
        return membroCCP;
    }

    public void setMembroCCP(MembroComissaoCPDTO membroCCP) {
        this.membroCCP = membroCCP;
    }

    public Long getProvaPublica() {
        return provaPublica;
    }

    public void setProvaPublica(Long provaPublica) {
        this.provaPublica = provaPublica;
    }

}
