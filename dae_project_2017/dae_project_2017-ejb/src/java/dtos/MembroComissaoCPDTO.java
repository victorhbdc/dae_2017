package dtos;

import java.util.Collection;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MembroComissaoCP") 
@XmlAccessorType(XmlAccessType.FIELD)
public class MembroComissaoCPDTO extends UserDTO {

    public MembroComissaoCPDTO() {
        super();
    }
}