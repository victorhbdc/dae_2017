package dtos;

import java.io.Serializable;

public class UserDTO implements Serializable {

    private String username;
    private String password;
    private String nome;
    private String email;

    public UserDTO() {
    }

    public UserDTO(String username, String password, String nome, String email) {
        this.username = username;
        this.password = password;
        this.nome = nome;
        this.email = email;
    }

    public void reset() {
        this.setEmail(null);
        this.setNome(null);
        this.setPassword(null);
        this.setUsername(null);
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
