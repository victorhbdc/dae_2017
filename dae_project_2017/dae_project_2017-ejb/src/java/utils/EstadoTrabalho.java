package utils;


public enum EstadoTrabalho {
    NAOINICIADO("Nao iniciado"),
    INICIADO("Iniciado"),
    ENTREGUE("Entregue"),
    FINALIZADO("Finalizado");

    private String estado;

    private EstadoTrabalho(String estado) {
        this.estado = estado;
    }

    public String getNomeTipo() {
        return estado;
    }
}