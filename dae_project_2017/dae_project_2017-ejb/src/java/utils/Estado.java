/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;


public enum Estado {
    ACEITE("Aceite"), REJEITADO("Rejeitado"), ANALISE("Análise");

    private String estado;

    private Estado(String estado) {
        this.estado = estado;
    }

    public String getNomeTipo() {
        return estado;
    }

    public Estado getEstado(String estado) {
        switch (estado) {
            case "Aceite":
                return Estado.ACEITE;
            case "Rejeitado":
                return Estado.REJEITADO;
            case "Por avaliar":
                return Estado.ANALISE;
        }
        return null;
    }
}
