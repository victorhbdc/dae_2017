/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import ejbs.EmailBean;
import java.util.Random;
import javax.ejb.EJBException;
import javax.mail.MessagingException;
import javax.validation.constraints.Max;

/**
 *
 * @author vsc
 */
public class CreateUserUtil {

    public static String generateRandomPassword(@Max(256) int size) {
        Random r = new Random();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < 12; i++) {
            sb.append((char) (65 + r.nextInt(25)));
        }

        return sb.toString();
    }

    public static void sendNotificationEmail(EmailBean emailBean, String nome, String email, String username, String password)
            throws MessagingException {
        try {
            emailBean.send(
                    email,
                    "Conta criada na Plataforma de gestão",
                    "Ola, " + nome
                    + " a sua conta foi criada na plataforma de gestão de DAE, \n"
                    + "o seu username é: " + username + "\n"
                    + "a sua password é: " + password + "\n"
                    + "Obrigado."
            );
        } catch (MessagingException e) {
            throw e;
        }
    }
}
