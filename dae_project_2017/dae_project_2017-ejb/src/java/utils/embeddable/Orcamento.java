/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.embeddable;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Embeddable;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vsc
 */
@Embeddable
public class Orcamento implements Serializable{

    @NotNull
    @DecimalMin("0.0")
    BigDecimal bibliografia;

    @NotNull
    @DecimalMin("0.0")
    BigDecimal material;

    @NotNull
    @DecimalMin("0.0")
    BigDecimal equipamento;

    @NotNull
    @DecimalMin("0.0")
    BigDecimal outros;

    public Orcamento() {
    }

    public Orcamento(BigDecimal bibliografia, BigDecimal material, 
            BigDecimal equipamento, BigDecimal outros) {
        this.bibliografia = bibliografia;
        this.material = material;
        this.equipamento = equipamento;
        this.outros = outros;
    }

    public BigDecimal getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(BigDecimal bibliografia) {
        this.bibliografia = bibliografia;
    }

    public BigDecimal getMaterial() {
        return material;
    }

    public void setMaterial(BigDecimal material) {
        this.material = material;
    }

    public BigDecimal getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(BigDecimal equipamento) {
        this.equipamento = equipamento;
    }

    public BigDecimal getOutros() {
        return outros;
    }

    public void setOutros(BigDecimal outros) {
        this.outros = outros;
    }
 
    
}
