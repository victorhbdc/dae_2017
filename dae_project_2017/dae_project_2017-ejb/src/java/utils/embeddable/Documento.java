/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.embeddable;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import utils.TipoTrabalho;

/**
 *
 * @author vsc
 */
@Embeddable
public class Documento implements Serializable {

    @Transient // todo 
    public static final String BASE_PATH = System.getProperty("user.dir");

    public enum TipoDeDocumento {
        ATA("Ata"), ARTEFACTO("Artefacto"), RELATORIO("Relatorio"), CANDIDATURA("Candidatura");

        private String pasta;

        private TipoDeDocumento(String pasta) {
            this.pasta = pasta;
        }

        public String getPasta() {
            return pasta;
        }
    }

    @NotNull(message = "Documento tem de ter um nome")
    private String nome;

    @NotNull(message = "Documento tem de ter um tipo")
    @Enumerated(EnumType.STRING)
    private TipoDeDocumento tipoDeDocumento;
    
    @NotNull
    private Date uploadedAt;

    @NotNull
    private String path;
    
    private String mimeType;

    public Documento() {
    }

    public Documento(TipoDeDocumento tipoDeDocumento, String nome, String path) {
        this.nome = nome;
        this.uploadedAt = new Date();
        this.tipoDeDocumento = tipoDeDocumento;
        this.path = path;
        
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getUploadedAt() {
        return uploadedAt;
    }

    public void setUploadedAt(Date uploadedAt) {
        this.uploadedAt = uploadedAt;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public TipoDeDocumento getTipoDeDocumento() {
        return tipoDeDocumento;
    }

    public void setTipoDeDocumento(TipoDeDocumento tipoDeDocumento) {
        this.tipoDeDocumento = tipoDeDocumento;
    }
    

}
