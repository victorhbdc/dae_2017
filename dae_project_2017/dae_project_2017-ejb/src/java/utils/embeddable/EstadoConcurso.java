/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.embeddable;


public enum EstadoConcurso {

    ANUNCIADO("Anunciado"),
    PERIODO_PROPOSTAS("Periodo de Propostas"),
    PERIODO_VALIDACAO("Periodo de Validação"),
    PERIODO_CANDIDATURAS("Periodo de Candidaturas"),
    PERIODO_SERIACAO("Periodo de Seriaçao"),
    PERIODO_ENTREGA_TRABALHOS("Periodo de Entrega Trabalhos"),
    PERIODO_MARCACAO_PROVAS_PUBLICAS("Periodo de Marcação de Provas Públicas"),
    FINALIZADO("Fim de Trabalhos");
    
    private String descricao;
    
    private EstadoConcurso(String descricao) {
        this.descricao = descricao;
    }
 
    public String getNomeTipo() {
        return descricao;
    }
}
