/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.embeddable;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vsc
 */
@Embeddable
public class ItemBibliografico implements Serializable {

    @NotNull(message = "Item bibliografico tem que ter titulo")
    String titulo;

    @NotNull(message = "Item bibliografico tem que ter um autor")
    String autor;

    public ItemBibliografico() {
    }

    public ItemBibliografico(String titulo, String autor) {
        this.titulo = titulo;
        this.autor = autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
}
