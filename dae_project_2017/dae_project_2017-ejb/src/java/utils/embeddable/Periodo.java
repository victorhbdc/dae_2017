/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.embeddable;

import java.io.Serializable;
import java.util.GregorianCalendar;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vsc
 */
@Embeddable
@XmlRootElement(name = "Periodo")
@XmlAccessorType(XmlAccessType.FIELD)
public class Periodo implements Serializable {

    @NotNull(message = "Periodo tem de ter um início")
    @Temporal(TemporalType.DATE)
    GregorianCalendar inicio;
    
    @NotNull(message = "Periodo tem de ter um fim")
    @Temporal(TemporalType.DATE)
    GregorianCalendar fim;

    public Periodo() {
    }
    
    public Periodo(GregorianCalendar inicio, GregorianCalendar fim) throws IllegalArgumentException{
        this.inicio = inicio;
        this.fim = fim;
        
        if(inicio.after(fim)){
            throw new IllegalArgumentException("Inicio é posterior "
                    + "ao fim do periodo");
        }
    }

    public GregorianCalendar getInicio() {
        return inicio;
    }

    public void setInicio(GregorianCalendar inicio) {
        this.inicio = inicio;
    }

    public GregorianCalendar getFim() {
        return fim;
    }

    public void setFim(GregorianCalendar fim) {
        this.fim = fim;
    }

    public boolean overlaps(Periodo p) {
        return this.contains(p) 
                || this.isInside(p) 
                || p.inicio.after(this.inicio) && p.inicio.before(this.fim)
                || p.fim.after(this.inicio) && p.fim.before(this.fim);
    }

    public boolean contains(Periodo p) {
        return p.inicio.after(this.inicio) && p.fim.before(this.fim);
    }

    public boolean isInside(Periodo p) {
        return this.inicio.after(p.inicio) && this.fim.before(p.fim);
    }

    public boolean contains(GregorianCalendar d) {
        return d.after(this.inicio) && d.before(this.fim);
    }

    public boolean isBefore(Periodo p) {
        return this.fim.before(p.inicio);
    }
    
    public boolean isAfter(Periodo p) {
        return this.inicio.after(p.fim);
    }
    
    public boolean isBefore(GregorianCalendar d) {
        return this.fim.before(d);
    }
    
    public boolean isAfter(GregorianCalendar d) {
        return this.inicio.after(d);
    }
}
