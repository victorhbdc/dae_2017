/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author vsc
 */
public enum TipoTrabalho {
    DISSERTACAO("Dissertação"), PROJECTO("Projecto"), ESTAGIO("Estágio");

    private String nomeTipo;

    private TipoTrabalho(String nomeTipo) {
        this.nomeTipo = nomeTipo;
    }

    public String getNomeTipo() {
        return nomeTipo;
    }

    public TipoTrabalho getTipoTrabalho(String nomeTipo) {
        switch (nomeTipo) {
            case "Dissertação":
                return TipoTrabalho.DISSERTACAO;
            case "Projecto":
                return TipoTrabalho.PROJECTO;
            case "Estágio":
                return TipoTrabalho.ESTAGIO;
        }
        return null;
    }
}
