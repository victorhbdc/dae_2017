/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import exceptions.OnlyProfessorsException;
import exceptions.NoProponentsException;
import dtos.PropostaDTO;
import entities.roles.Professor;
import entities.roles.Proponente;
import entities.trabalhos.Concurso;
import entities.trabalhos.Proposta;
import exceptions.EntityDoesNotExistException;
import exceptions.MyConstraintViolationException;
import exceptions.PeriodoInvalido;
import exceptions.PropostaAceiteException;
import exceptions.Utils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.Estado;
import utils.TipoTrabalho;
import utils.embeddable.EstadoConcurso;
import utils.embeddable.ItemBibliografico;
import utils.embeddable.Orcamento;

/**
 *
 * @author vsc
 */
@Stateless
@Path(PropostaBean.BASE_REST_PATH)
public class PropostaBean extends Bean<Proposta> {
    
    public static final String BASE_REST_PATH = "/propostas";

    public static final String REST_PATH_ALL = BASE_REST_PATH + "/all";
    
    public static final String REST_PATH_CREATE = BASE_REST_PATH + "/create";
    
    public static final String REST_PATH_UPDATE = BASE_REST_PATH + "/update";
   
    public static final String REST_PATH_REJEITAR = BASE_REST_PATH + "/rejeitar";
    
    public static final String REST_PATH_DELETE(String propostaId) {
        return BASE_REST_PATH + "/delete/" + propostaId;
    }
    
    @EJB
    EmailBean emailBean;
    // ####################################################
    
    public PropostaBean() {
    }
    
    // ####################################################
    
    public void createCB(PropostaDTO propostaDTO) throws 
                                        NoProponentsException, OnlyProfessorsException,
                                        EntityDoesNotExistException, MyConstraintViolationException,
                                        PeriodoInvalido {
        this.create(propostaDTO);
    }
    
    @POST
    @RolesAllowed({"Proponente"})
    @Path("create")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(PropostaDTO propostaDTO) throws 
                                        NoProponentsException, OnlyProfessorsException,
                                        EntityDoesNotExistException, MyConstraintViolationException,
                                        PeriodoInvalido {
        
            String mestrado = propostaDTO.getMestrado();
            Long concursoId = propostaDTO.getConcursoId();
            TipoTrabalho tipoTrabalho = propostaDTO.getTipoTrabalho();
            String titulo = propostaDTO.getTitulo();
            String resumo = propostaDTO.getResumo();
            String planoTrabalho = propostaDTO.getPlanoTrabalho();
            BigDecimal orcamento_bibliografia = propostaDTO.getOrcamentoBibliografia();
            BigDecimal orcamento_material = propostaDTO.getOrcamentoMaterial();
            BigDecimal orcamento_equipamento = propostaDTO.getOrcamentoEquipamento();
            BigDecimal orcamento_outros = propostaDTO.getOrcamentoOutros();
            List<String> proponenteUsernames = propostaDTO.getProponenteUsernames();
            List<String> areasCientificas = propostaDTO.getAreasCientificas();
            //----------------CAMPOS NAO OBRIGATORIOS------------------------------
            List<String> objectivos = propostaDTO.getObjectivos();
            List<String> requisitosTrabalho = propostaDTO.getRequisitosTrabalho();
            List<String> apoios = propostaDTO.getApoios();
            String localRealizacao = propostaDTO.getLocalRealizacao();
            List<ItemBibliografico> bibliografia = propostaDTO.getBibliografia();
            
        try {

            List<String> proponenteUsername = proponenteUsernames;
            List<Proponente> proponentes = new LinkedList<>();

            int proponentesUsernameSize = proponenteUsername.size();

            if (proponentesUsernameSize <= 0) {
                throw new NoProponentsException();
            }

            Proponente proponente;

            if (proponentesUsernameSize == 1) {
                proponente
                        = em.find(Proponente.class, proponenteUsername.get(0));

                if (proponente == null) {
                    throw new NoProponentsException();
                }
                proponentes.add(proponente);

            } else { // professores
                // conjunto de proponenteUsernames so pode ser contituido por professores
                for (int i = 0; i < proponentesUsernameSize; i++) {
                    proponente
                            = em.find(Professor.class, proponenteUsername.get(i));

                    if (proponente == null) {
                        throw new OnlyProfessorsException();
                    }

                    proponentes.add(proponente);
                }
            }

            Concurso concurso = em.find(Concurso.class, concursoId);

            if (concurso == null) {
                throw new EntityDoesNotExistException("Concurso especificado não existe");
            }

            // propostas so são aceites se o cuncurso estiver na fase de propostas
            if (concurso.getEstadoConcurso() != EstadoConcurso.PERIODO_PROPOSTAS) {
                throw new PeriodoInvalido("A fase de entrega de propostas não está activa");
            }
            
            if(areasCientificas.isEmpty()){
                throw new IllegalArgumentException("Uma proposta tem de ter uma area cientifica");
            }
            
            if(planoTrabalho.isEmpty()){
                throw new IllegalArgumentException("Uma proposta tem de ter um plano de trabalho");
            }

            Orcamento orcamento
                    = new Orcamento(
                            orcamento_bibliografia,
                            orcamento_material,
                            orcamento_equipamento,
                            orcamento_outros
                    );

            Proposta proposta
                    = new Proposta(mestrado, concurso,
                            tipoTrabalho, titulo, resumo, orcamento, planoTrabalho, areasCientificas, proponentes);
            
            /////////////////////////////CAMPOS NAO OBRIGATORIOS/////////////////
            proposta.setObjectivos(objectivos);
            proposta.setRequisitosTrabalho(requisitosTrabalho);
            proposta.setApoios(apoios);
            proposta.setBibliografia(bibliografia);
            proposta.setLocalRealizacao(localRealizacao);
            
            em.persist(proposta);

            proponentes.forEach((p) -> {
                p.addProposta(proposta);
                em.merge(p);
            });

            concurso.addProposta(proposta);
            em.merge(concurso);
            
            return;

        } catch (NoProponentsException | OnlyProfessorsException | EntityDoesNotExistException | PeriodoInvalido e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            System.err.println(e.toString());
            throw new EJBException(e.toString());
        }

    }

    @Override
    protected Collection<Proposta> getAll() {
        try {
            return em.createNamedQuery(Proposta.getAll, Proposta.class)
                    .getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all")
    public Collection<PropostaDTO> getAllDtos() {
        try {
            return toDTOs(getAll(), PropostaDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    @PUT
    @RolesAllowed({"Proponente"})
    @Path("update")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void update(PropostaDTO propostaDTO)
            throws EntityDoesNotExistException, MyConstraintViolationException, PropostaAceiteException {

            Long propostaID = propostaDTO.getId();
            String mestrado = propostaDTO.getMestrado();
            Long concursoId = propostaDTO.getConcursoId();
            TipoTrabalho tipoTrabalho = propostaDTO.getTipoTrabalho();
            String titulo = propostaDTO.getTitulo();
            String resumo = propostaDTO.getResumo();
            String planoTrabalho = propostaDTO.getPlanoTrabalho();
            BigDecimal orcamento_bibliografia = propostaDTO.getOrcamentoBibliografia();
            BigDecimal orcamento_material = propostaDTO.getOrcamentoMaterial();
            BigDecimal orcamento_equipamento = propostaDTO.getOrcamentoEquipamento();
            BigDecimal orcamento_outros = propostaDTO.getOrcamentoOutros();
            List<String> proponenteUsernames = propostaDTO.getProponenteUsernames();
            List<String> areasCientificas = propostaDTO.getAreasCientificas();
            //----------------CAMPOS NAO OBRIGATORIOS------------------------------
            List<String> objectivos = propostaDTO.getObjectivos();
            List<String> requisitosTrabalho = propostaDTO.getRequisitosTrabalho();
            List<String> apoios = propostaDTO.getApoios();
            String localRealizacao = propostaDTO.getLocalRealizacao();
            List<ItemBibliografico> bibliografia = propostaDTO.getBibliografia();
            
        try {
    
            //////////////////////////VALIDACAO///////////////////////////////////////////////////////
            
            Proposta proposta = em.find(Proposta.class, propostaID);
            if(proposta == null){
                throw new EntityDoesNotExistException("A proposta nao existe!");
            }
            
            if(proposta.getEstado() == Estado.ACEITE){
                throw new PropostaAceiteException();
            }
            
            List<String> proponenteUsername = proponenteUsernames;
            List<Proponente> proponentes = new LinkedList<>();

            int proponentesUsernameSize = proponenteUsername.size();
            if (proponentesUsernameSize <= 0) {
                throw new NoProponentsException();
            }

            Proponente proponente;
            if (proponentesUsernameSize == 1) {
                
                proponente = em.find(Proponente.class, proponenteUsername.get(0));
                if (proponente == null) {
                    throw new NoProponentsException();
                }
                proponentes.add(proponente);

            } else { // professores
                // conjunto de proponenteUsernames so pode ser contituido por professores
                for (int i = 0; i < proponentesUsernameSize; i++) {
                    
                    proponente = em.find(Professor.class, proponenteUsername.get(i));
                    if (proponente == null) {
                        throw new OnlyProfessorsException();
                    }

                    proponentes.add(proponente);
                }
            }

            Concurso concurso = em.find(Concurso.class, concursoId);
            if (concurso == null) {
                throw new EntityDoesNotExistException("Concurso especificado não existe");
            }
            if (concurso.getEstadoConcurso() != EstadoConcurso.PERIODO_PROPOSTAS &&
                    concurso.getEstadoConcurso() != EstadoConcurso.PERIODO_VALIDACAO) {
                throw new PeriodoInvalido("A fase de atualizacao de propostas não está activa");
            }
            
            if(areasCientificas.isEmpty() || areasCientificas.get(0).isEmpty()){
                throw new IllegalArgumentException("Uma proposta tem de ter uma area cientifica");
            }
            
            if(planoTrabalho.isEmpty()){
                throw new IllegalArgumentException("Uma proposta tem de ter um plano de trabalho");
            }

            /////////////////////////////////////////PERSISTENCIA/////////////////////////////////////////////
            
            
            proposta.getProponentes().forEach((propAntigo) -> {
                propAntigo.removeProposta(proposta);
                em.merge(propAntigo);
            });
            proponentes.forEach((propNovo) -> {
                propNovo.addProposta(proposta);
                em.merge(propNovo);
            });
            proposta.setProponentes(proponentes);
            
            proposta.getConcurso().removeProposta(proposta);
            concurso.addProposta(proposta);
            proposta.setConcurso(concurso);
            
            proposta.setMestrado(mestrado);
            proposta.setTipoTrabalho(tipoTrabalho);
            proposta.setTitulo(titulo);
            proposta.setAreasCientificas(areasCientificas);
            proposta.setResumo(resumo);
            proposta.setObjectivos(objectivos);
            proposta.setBibliografia(bibliografia);
            proposta.setPlanoTrabalho(planoTrabalho);
            proposta.setLocalRealizacao(localRealizacao);
            proposta.setRequisitosTrabalho(requisitosTrabalho);
            Orcamento orcamento
                    = new Orcamento(
                            orcamento_bibliografia,
                            orcamento_material,
                            orcamento_equipamento,
                            orcamento_outros
                    );
            proposta.setOrcamento(orcamento);
            proposta.setApoios(propostaDTO.getApoios());
            //Observacoes so ao aceitar e rejeitar
            //Trabalho so ao aceitar
                    
            em.merge(proposta);
 
        } catch (EntityDoesNotExistException | PropostaAceiteException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
     
    @DELETE
    @RolesAllowed({"Proponente"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("delete/{propostaId}")
    public void remove(@PathParam("propostaId") Long propostaId) throws 
            EntityDoesNotExistException, PeriodoInvalido, PropostaAceiteException, MyConstraintViolationException {
        
        try{
            Proposta proposta = em.find(Proposta.class, propostaId);
            if (proposta == null) {
                throw new EntityDoesNotExistException("[PropostaBean.remove]");
            }

            EstadoConcurso estado = proposta.getConcurso().getEstadoConcurso();
            if(estado != EstadoConcurso.PERIODO_PROPOSTAS && estado != EstadoConcurso.PERIODO_VALIDACAO){
                throw new PeriodoInvalido("Nao se pode apagar propostas fora dos periodos de propostas e validacao");
            }

            if(proposta.getEstado() == Estado.ACEITE){
                throw new PropostaAceiteException();
            }

            proposta.getProponentes().forEach((p) -> {
                p.removeProposta(proposta);
                em.merge(p);
            });

            proposta.getConcurso().removeProposta(proposta);
            em.merge(proposta.getConcurso());
            em.remove(proposta);

        }catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        }
    }
    
    @PUT
    @RolesAllowed({"MembroCCP"})
    @Path("rejeitar")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void rejeitar(PropostaDTO propostaDTO) throws MessagingException, 
            EntityDoesNotExistException, PeriodoInvalido, MyConstraintViolationException{
        try{
            Proposta proposta = em.find(Proposta.class, propostaDTO.getId());
            String observacoes = propostaDTO.getObservacoes();

            if(proposta == null){
                throw new EntityDoesNotExistException("Proposta nao existe");
            }            

            Concurso concurso = proposta.getConcurso();
            if (concurso.getEstadoConcurso() != EstadoConcurso.PERIODO_VALIDACAO) {
                throw new PeriodoInvalido("A fase de validacao de propostas não está activa");
            }
            
           
            proposta.setObservacoes(observacoes);
            proposta.setEstado(Estado.REJEITADO);
            em.persist(proposta);
           
            List<Proponente> proponentes = proposta.getProponentes();
            for(Proponente proponente : proponentes){
                emailBean.send(
                            proponente.getEmail(),
                            "Proposta rejeitada",
                            "Ola " + proponente.getNome() + ", pedimos desculpa mas a sua proposta foi rejeitada."
                            + "\nDetalhes: " + (observacoes == null ? " (sem observações) " : observacoes));
            }
        } catch (MessagingException | EntityDoesNotExistException | PeriodoInvalido e) {
            throw e;
        }  catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
}
