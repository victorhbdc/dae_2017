/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.ProfessorDTO;
import entities.roles.Professor;
import entities.roles.User;
import exceptions.EntityDoesNotExistException;
import exceptions.EntityExistsException;
import exceptions.MyConstraintViolationException;
import exceptions.Utils;
import java.util.Collection;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.CreateUserUtil;

/**
 *
 * @author vsc
 */
@Stateless
@Path(ProfessorBean.BASE_REST_PATH)
public class ProfessorBean extends Bean<Professor> {

    public static final String BASE_REST_PATH = "/professores";

    public static final String REST_PATH_GET_ALL = BASE_REST_PATH + "/all";

    public static final String REST_PATH_PUT_CREATE = BASE_REST_PATH + "/create";

    public static final String REST_PATH_PUT_UPDATE = BASE_REST_PATH + "/update";

    public static final String REST_PATH_DELETE(String username) {
        return BASE_REST_PATH + "/delete/" + username;
    }

    // ####################################################
    @EJB
    EmailBean emailBean;
    // ####################################################

    public ProfessorBean() {
    }

    // ####################################################
    public Professor create(
            String username,
            String nome,
            String email)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {

        String generatedPassword = CreateUserUtil.generateRandomPassword(12);
        return this.create(username, generatedPassword, nome, email);

    }

    public Professor create(
            String username,
            String password,
            String nome,
            String email)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {
        try {

            User user = em.find(User.class, username);
            if (user != null) {
                throw new EntityExistsException("O estudante já existe");
            }

            Professor professor
                    = new Professor(username, password, nome, email);

            em.persist(professor);

            CreateUserUtil.sendNotificationEmail(emailBean, nome, email, username, password);
            return professor;
        } catch (EntityExistsException | MessagingException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @PUT
    @RolesAllowed({"MembroCCP"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("create")
    public void create(ProfessorDTO professorDTO)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {

        this.create(
                professorDTO.getUsername(),
                professorDTO.getNome(),
                professorDTO.getEmail());
    }

    @Override
    protected List<Professor> getAll() {
        try {
            return em.createNamedQuery(Professor.getAll, Professor.class)
                    .getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"Proponente", "MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all")
    public Collection<ProfessorDTO> getAllDTOs()
            throws EntityDoesNotExistException {
        try {

            return toDTOs(getAll(), ProfessorDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @PUT
    @RolesAllowed({"MembroCCP"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("update")
    public void update(ProfessorDTO professorDTO)
            throws EntityDoesNotExistException, MyConstraintViolationException {
        try {

            String username = professorDTO.getUsername();
            String password = professorDTO.getPassword();
            String nome = professorDTO.getNome();
            String email = professorDTO.getEmail();

            Professor professor = em.find(Professor.class, username);
            if (professor == null) {
                throw new EntityDoesNotExistException("O professor não existe");
            }

            if (!professor.getEmail().equals(email)) {
                professor.setEmail(email);
            }

            if (!professor.getNome().equals(nome)) {
                professor.setNome(nome);
            }

            if (password != null && !password.isEmpty()) {
                professor.setPassword(password);
            }

            // todo inserir novas propostas
            em.merge(professor);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @DELETE
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("delete/{username}")
    public void remove(@PathParam("username") String username)
            throws EntityDoesNotExistException, EntityExistsException {
        Professor professor = em.find(Professor.class, username);

        if (professor == null) {
            throw new EntityDoesNotExistException("[ProfessorBean.remove]");
        }

        if (professor.getPropostas().size() > 0) {
            throw new EntityExistsException("O professor tem propostas associadas");
        }

        em.remove(professor);
    }
}
