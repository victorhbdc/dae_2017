/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.EstudanteDTO;
import dtos.SupervisorDTO;
import entities.roles.Instituicao;
import entities.roles.Supervisor;
import exceptions.EntityDoesNotExistException;
import exceptions.EntityExistsException;
import exceptions.MyConstraintViolationException;
import exceptions.Utils;
import java.util.Collection;
import java.util.LinkedList;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Stateless
@LocalBean
@Path(SupervisorBean.BASE_REST_PATH)
public class SupervisorBean extends Bean<Supervisor>{

    public static final String BASE_REST_PATH = "/supervisores";

    public static final String REST_PATH_PUT_CREATE = BASE_REST_PATH + "/create";
    
    public static final String REST_PATH_GET_ALL_DE_INSTITUICAO(String instituicaoUsername) {
        return BASE_REST_PATH + "/all/" + instituicaoUsername;
    }
    // ####################################################
    public SupervisorBean() {
    }

    // ####################################################
    
    //FOR CONFIGBEAN
    public void createCB(SupervisorDTO supervisorDTO) throws EJBException, 
            EntityExistsException, MyConstraintViolationException{
        this.create(supervisorDTO);
    }
    
    
    @PUT
    @RolesAllowed({"Proponente"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("create")
    public void create(SupervisorDTO supervisorDTO)
                throws EntityExistsException, MyConstraintViolationException, EJBException {
        try {
            String email = supervisorDTO.getEmail();
            Supervisor supervisor = em.find(Supervisor.class, email);
            if (supervisor != null) {
                throw new EntityExistsException("O supervisor já existe");
            }
            
            Instituicao instituicao = em.find(Instituicao.class, supervisorDTO.getInstituicaoUsername());
            if (instituicao == null) {
                throw new EntityDoesNotExistException("A instituicao nao existe");
            }

            supervisor
                    = new Supervisor(supervisorDTO.getNome(), email, instituicao);
            instituicao.addSupervisor(supervisor);
            
            em.persist(supervisor);
            em.merge(instituicao);
            
        } catch (EntityExistsException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @Override
    public Collection<Supervisor> getAll() {
        try {
            return em.createNamedQuery(Supervisor.getAll, Supervisor.class)
                    .getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"Proponente", "MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all/{instituicaoUsername}")
    public Collection<SupervisorDTO> getAllDTOs(@PathParam("instituicaoUsername") String instituicaoUsername)
            throws EntityDoesNotExistException {
        try {
            Instituicao instituicao = em.find(Instituicao.class, instituicaoUsername);
            if(instituicao == null){
                throw new EntityDoesNotExistException("A instituicao nao existe.");
            }
            
            LinkedList<Supervisor> supervisoresDaInstituicao = new LinkedList<>();
            for(Supervisor supervisor : getAll()){
                if(supervisor.getInstituicao().equals(instituicao)){
                    supervisoresDaInstituicao.add(supervisor);
                }
            }
            return toDTOs(supervisoresDaInstituicao, SupervisorDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
}

