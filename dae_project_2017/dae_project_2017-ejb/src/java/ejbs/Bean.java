package ejbs;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.modelmapper.ModelMapper;

public abstract class Bean<E extends Serializable> {
    
    @PersistenceContext
    protected EntityManager em;
                     
    public static final ModelMapper mapper = new ModelMapper();

    protected abstract Collection<E> getAll();
    
    protected <Entity extends Serializable, DTO> DTO toDTO(Entity entity, Class<DTO> dtoClass) {
        return mapper.map(entity, dtoClass);
    }
    
    protected <Entity extends Serializable, DTO> Collection<DTO> toDTOs(Collection<Entity> entities, Class<DTO> dtoClass) {
        Collection<DTO> dtos = new LinkedList<>();
        for(Entity e: entities){
            dtos.add(toDTO(e, dtoClass));
        }
        return dtos;
//        return entities.parallelStream().map(e -> toDTO(e, dtoClass)).collect(Collectors.toList());
    }
    
    protected <DTO> Collection<DTO> getAll(Class<DTO> dtoClass) {
        return toDTOs(getAll(), dtoClass);
    }
}
