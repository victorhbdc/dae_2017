/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.MembroComissaoCPDTO;
import entities.roles.MembroComissaoCP;
import exceptions.EntityDoesNotExistException;
import exceptions.EntityExistsException;
import java.util.Collection;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author vsc
 */
@Stateless
@Path(MembroComissaoCPBean.BASE_REST_PATH)
public class MembroComissaoCPBean extends Bean<MembroComissaoCP> {
    
    public static final String BASE_REST_PATH = "/membrosccp";
    
    public static final String REST_PATH_GET_ALL = BASE_REST_PATH + "/all";

    public static final String REST_PATH_GET_FIND(String username){
         return BASE_REST_PATH + "/find/" + username;
    };
    
//    public static final String REST_PATH_DELETE(String username) {
//        return BASE_REST_PATH + "/delete/" + username;
//    }

    // ####################################################

    public MembroComissaoCPBean() {
    }
    
    // ####################################################
    
    public void create(
            String username,
            String password,
            String nome,
            String email)
            throws EntityExistsException {
        try {

            MembroComissaoCP membroComissaoCP 
                    = em.find(MembroComissaoCP.class, username);

            if (membroComissaoCP != null) {
                throw new EntityExistsException("O Membro da CCP já existe");
            }

            membroComissaoCP
                    = new MembroComissaoCP(username, password, nome, email);

            em.persist(membroComissaoCP);

        } catch (EntityExistsException e) {
            throw e;
        } catch (Exception e) {
            //throw new EJBException(e.getMessage());
            throw e;
        }
    }

    @Override
    protected Collection<MembroComissaoCP> getAll() {
        try {
            return em.createNamedQuery(MembroComissaoCP.getAll, MembroComissaoCP.class)
                    .getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all")
    public Collection<MembroComissaoCPDTO> getAllDTOs()
            throws EntityDoesNotExistException {
        try {
            return toDTOs(getAll(), MembroComissaoCPDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("find/{username}")
    public MembroComissaoCPDTO findMembroCCP(@PathParam("username") String username)
            throws EntityDoesNotExistException {
        try {
            MembroComissaoCP mccp = em.find(MembroComissaoCP.class, username);
            if (mccp == null) {
                throw new EntityDoesNotExistException("Não existe nenhum Membro da CP com esse username");
            }
            return toDTO(mccp, MembroComissaoCPDTO.class);
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
//    @DELETE
//    @RolesAllowed({})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Path("delete/{username}")
//    public void remove(@PathParam("username") String username) 
//            throws EntityDoesNotExistException {
//        MembroComissaoCP membro = em.find(MembroComissaoCP.class, username);
//
//        if (membro == null) {
//            throw new EntityDoesNotExistException("[MembroComissaoCPBean.remove]");
//        }
//
//        em.remove(membro);
//    }
}