/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.CandidaturaDTO;
import dtos.DocumentoDTO;
import dtos.ProfessorDTO;
import dtos.PropostaDTO;
import dtos.TrabalhoDTO;
import entities.roles.Instituicao;
import entities.roles.Professor;
import entities.roles.Proponente;
import entities.roles.Supervisor;
import entities.trabalhos.CandidaturaATrabalho;
import entities.trabalhos.Concurso;
import entities.trabalhos.Proposta;
import entities.trabalhos.Trabalho;
import exceptions.EntityDoesNotExistException;
import exceptions.MyConstraintViolationException;
import exceptions.NoProponentsException;
import exceptions.OnlyProfessorsException;
import exceptions.PeriodoInvalido;
import exceptions.SupervisorDeInstituicaoNaoProponente;
import exceptions.Utils;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.Estado;
import utils.TipoProponente;
import utils.embeddable.Documento;
import utils.embeddable.EstadoConcurso;

/**
 *
 * @author vsc
 */
@Stateless
@Path(TrabalhoBean.BASE_REST_PATH)
public class TrabalhoBean extends Bean<Trabalho> {

    public static final String BASE_REST_PATH = "/trabalhos";

//    public static final String REST_PATH_ALL = BASE_REST_PATH + "/all";
    
    public static final String REST_PATH_GET_TRABALHO(String trabalhoID) {
        return BASE_REST_PATH + "/find/trabalho/" + trabalhoID;
    }
    
    public static final String REST_PATH_GET_ALL_FASE_CANDIDATURA = BASE_REST_PATH + "/all/candidataveis";

    public static final String REST_PATH_GET_ALL_POR_SERIAR = BASE_REST_PATH + "/all/seriaveis";

    public static final String REST_PATH_GET_CANDIDATURAS(String trabalhoID) {
        return BASE_REST_PATH + "/candidaturas/" + trabalhoID;
    }

    public static final String REST_PATH_GET_ALL_SERIADOS = BASE_REST_PATH + "/all/seriados";

    public static final String REST_PATH_GET_ALL_INICIADOS = BASE_REST_PATH + "/all/iniciados";

    public static final String REST_PATH_GET_ALL_ENTREGUES = BASE_REST_PATH + "/all/entregues";

    public static final String REST_PATH_GET_ALL_FINALIZADOS = BASE_REST_PATH + "/all/finalizados";

    public static final String REST_PATH_CREATE = BASE_REST_PATH + "/create";

    public static final String REST_PATH_UPDATE_ORIENTADORES = BASE_REST_PATH + "/update/orientadores";

    public static final String REST_PATH_GET_ORIENTADORES(String trabalhoID) {
        return BASE_REST_PATH + "/orientadores/" + trabalhoID;
    }


    public static final String REST_PATH_UPDATE_ARTEFACTOS = BASE_REST_PATH + "/update/artefactos";

    @EJB
    EmailBean emailBean;

    // ####################################################
    public TrabalhoBean() {
    }

    @Override
    protected Collection<Trabalho> getAll() {
        try {
            return em.createNamedQuery(Trabalho.getAll, Trabalho.class)
                    .getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

//    @GET
//    @RolesAllowed({})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Path("all")
//    public Collection<TrabalhoDTO> getAllDtos() {
//        try {
//            return toDTOs(getAll(), TrabalhoDTO.class);
//        } catch (Exception e) {
//            throw new EJBException(e.getMessage());
//        }
//    }
    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("candidaturas/{trabalhoID}")
    public Collection<CandidaturaDTO> getCandidaturasAbertasDoTrabalho(@PathParam("trabalhoID") Long trabalhoID)
            throws EntityDoesNotExistException {
        try {
            Trabalho trabalho = em.find(Trabalho.class, trabalhoID);
            if (trabalho == null) {
                throw new EntityDoesNotExistException("Trabalho nao existe");
            }
            LinkedList<CandidaturaATrabalho> candidaturasAbertasDoTrabalho = new LinkedList<>();
            for (CandidaturaATrabalho candidatura : trabalho.getCandidaturas()) {
                if (candidatura.getEstado() == Estado.ANALISE) {
                    candidaturasAbertasDoTrabalho.add(candidatura);
                }
            }
            return toDTOs(candidaturasAbertasDoTrabalho, CandidaturaDTO.class);
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @PermitAll
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("find/trabalho/{trabalhoID}")
    public TrabalhoDTO findTrabalho(@PathParam("trabalhoID") Long trabalhoID)
            throws EntityDoesNotExistException {
        try {

            Trabalho trabalho = em.find(Trabalho.class, trabalhoID);
            if (trabalho == null) {
                throw new EntityDoesNotExistException("Trabalho nao existe");
            }

            return toDTO(trabalho, TrabalhoDTO.class);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("orientadores/{trabalhoID}")
    public Collection<ProfessorDTO> getOrientadores(@PathParam("trabalhoID") Long trabalhoID)
            throws EntityDoesNotExistException {
        try {
            Trabalho trabalho = em.find(Trabalho.class, trabalhoID);
            if (trabalho == null) {
                throw new EntityDoesNotExistException("Trabalho nao existe");
            }

            return toDTOs(trabalho.getProfessores(), ProfessorDTO.class);
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"Estudante"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all/candidataveis")
    public Collection<TrabalhoDTO> getAllDtosCandidataveis() {
        try {
            Collection<Trabalho> trabalhosCandidataveis = new LinkedList<>();
            for (Trabalho trabalho : getAll()) {
                if (trabalho.getProposta().getConcurso().getEstadoConcurso() == EstadoConcurso.PERIODO_CANDIDATURAS) {
                    trabalhosCandidataveis.add(trabalho);
                }
            }
            return toDTOs(trabalhosCandidataveis, TrabalhoDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all/seriaveis")
    public Collection<TrabalhoDTO> getAllDtosSeriaveis() {
        try {
            Collection<Trabalho> trabalhosSeriaveis = new LinkedList<>();
            for (Trabalho trabalho : getAll()) {
                if (trabalho.getProposta().getConcurso().getEstadoConcurso() == EstadoConcurso.PERIODO_SERIACAO
                        && trabalho.getEstudante() == null) {
                    for (CandidaturaATrabalho candidatura : trabalho.getCandidaturas()) {
                        if (candidatura.getEstado() == Estado.ANALISE) {
                            trabalhosSeriaveis.add(trabalho);
                            break;
                        }
                    }
                }
            }
            return toDTOs(trabalhosSeriaveis, TrabalhoDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all/seriados")
    public Collection<TrabalhoDTO> getAllDtosSeriados() {
        try {
            Collection<Trabalho> trabalhosSeriados = new LinkedList<>();
            for (Trabalho trabalho : getAll()) {
                if (trabalho.getProposta().getConcurso().getEstadoConcurso() == EstadoConcurso.PERIODO_SERIACAO
                        && trabalho.getEstudante() != null) {
                    trabalhosSeriados.add(trabalho);
                }
            }
            return toDTOs(trabalhosSeriados, TrabalhoDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all/iniciados")
    public Collection<TrabalhoDTO> getAllDtosIniciados() {
        try {
            return toDTOs(
                    em.createNamedQuery(Trabalho.getAllIniciados, Trabalho.class).getResultList(),
                    TrabalhoDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all/entregues")
    public Collection<TrabalhoDTO> getAllDtosEntregues() {
        try {
            return toDTOs(
                    em.createNamedQuery(Trabalho.getAllEntregues, Trabalho.class).getResultList(),
                    TrabalhoDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @PermitAll
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all/finalizados")
    public Collection<TrabalhoDTO> getAllDtosFinalizados() {
        try {
            return toDTOs(
                    em.createNamedQuery(Trabalho.getAllFinalizados, Trabalho.class).getResultList(),
                    TrabalhoDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void createCB(PropostaDTO propostaDTO) throws //MessagingException, 
            EntityDoesNotExistException, PeriodoInvalido, MyConstraintViolationException,
            NoProponentsException, OnlyProfessorsException, MessagingException {
        this.create(propostaDTO);
    }

    @PUT
    @RolesAllowed({"MembroCCP"})
    @Path("create")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(PropostaDTO propostaDTO) throws //MessagingException, 
            EntityDoesNotExistException, PeriodoInvalido, MyConstraintViolationException,
            NoProponentsException, OnlyProfessorsException, MessagingException {
        try {
            Proposta proposta = em.find(Proposta.class, propostaDTO.getId());
            String observacoes = propostaDTO.getObservacoes();
            if (proposta == null) {
                throw new EntityDoesNotExistException("Proposta nao existe");
            }
            //Nao e necessario ver se o concurso esta a null
            Concurso concurso = proposta.getConcurso();
            if (concurso.getEstadoConcurso() != EstadoConcurso.PERIODO_VALIDACAO) {
                throw new PeriodoInvalido("A fase de validacao de propostas não está activa");
            }

            List<Proponente> proponentes = proposta.getProponentes();
            List<Professor> orientadores = new LinkedList<>();
            Trabalho trabalho;

            if (proponentes.isEmpty()) {
                throw new NoProponentsException();
            }

            if (proponentes.size() == 1 && proponentes.get(0) instanceof Instituicao) {
                trabalho = new Trabalho(proposta);
            } else {
                for (Proponente proponente : proponentes) {
                    if (!(proponente instanceof Professor)) {
                        throw new OnlyProfessorsException();
                    } else {
                        orientadores.add((Professor) proponente);
                    }
                }
                trabalho = new Trabalho(proposta, orientadores);
            }

            proposta.setObservacoes(observacoes);
            proposta.setEstado(Estado.ACEITE);
            proposta.setTrabalho(trabalho);

            em.merge(proposta);
            em.persist(trabalho);

            em.flush();
            trabalho = em.find(Proposta.class, proposta.getId()).getTrabalho();

            for (Professor orientador : orientadores) {
                orientador.addTrabalho(trabalho);
                em.merge(orientador);
            }
            for (Proponente proponente : proponentes) {
                emailBean.send(
                        proponente.getEmail(),
                        "Proposta aceite",
                        "Ola " + proponente.getNome() + ", a sua proposta foi aceite!"
                        + "\nDetalhes: " + (observacoes == null ? " (sem observações) " : observacoes));
            }

            return;
        } catch (EntityDoesNotExistException | PeriodoInvalido | NoProponentsException | MessagingException | OnlyProfessorsException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    //UPDATE ONLY ON PROFESSORS & SUPERVISORES (MCCP)
    @PUT
    @RolesAllowed({"MembroCCP"})
    @Path("update/orientadores")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void updateOrientadores(TrabalhoDTO trabalhoDTO)
            throws EntityDoesNotExistException, MyConstraintViolationException {
        try {

            Trabalho trabalho = em.find(Trabalho.class, trabalhoDTO.getId());
            if (trabalho == null) {
                throw new EntityDoesNotExistException("Trabalho nao existe");
            }

            List<String> profUsernames = trabalhoDTO.getProfessorUsernames();
            List<Professor> professores = new LinkedList<>();
            Professor professor;
            for (String username : profUsernames) {
                professor = em.find(Professor.class, username);
                if (professor == null) {
                    throw new EntityDoesNotExistException("Professor nao existe");
                }
                professores.add(professor);
            }

            if (trabalho.getTipoProponente() == TipoProponente.INSTITUICAO) {

                Instituicao instituicao = (Instituicao) trabalho.getProposta().getProponentes().get(0);
                Supervisor supervisor = em.find(Supervisor.class, trabalhoDTO.getSupervisorEmail());
                if (supervisor == null) {
                    throw new EntityDoesNotExistException("Trabalho proposto por instituicoes tem de ter supervisores");
                }
                if (!supervisor.getInstituicao().equals(instituicao)) {
                    throw new SupervisorDeInstituicaoNaoProponente();
                }
                Supervisor supervisorAntigo = trabalho.getSupervisor();
                if (supervisorAntigo != null) {
                    supervisorAntigo.removeTrabalho(trabalho);
                }
                trabalho.setSupervisor(supervisor);
                supervisor.addTrabalho(trabalho);
                em.merge(supervisor);
            }

            List<Professor> professoresAntigos = trabalho.getProfessores();
            for (Professor professorAntigo : professoresAntigos) {
                professorAntigo.removeTrabalho(trabalho);
            }

            trabalho.setProfessores(professores);
            em.merge(trabalho);
            for (Professor p : professores) {
                p.addTrabalho(trabalho);
                em.merge(p);
            }

            em.merge(trabalho);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    //UPDATE ONLY ON ARTEFACTOS (Estudante)
    @PUT
    @RolesAllowed({"Estudante"})
    @Path("update/artefactos")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void updateArtefactos(TrabalhoDTO trabalhoDTO)
            throws EntityDoesNotExistException, MyConstraintViolationException {
        try {

            Trabalho trabalho = em.find(Trabalho.class, trabalhoDTO.getId());
            if (trabalho == null) {
                throw new EntityDoesNotExistException("Trabalho nao existe");
            }

            List<Documento> artefactos = new LinkedList<>();
            for (DocumentoDTO dto : trabalhoDTO.getArtefactos()) {
                Documento documento
                        = new Documento(
                                dto.getTipoDeDocumento(),
                                dto.getNome(),
                                dto.getPath()
                        );

                if (dto.getMimeType() != null) {
                    documento.setMimeType(dto.getMimeType());
                }
                artefactos.add(documento);
            }

            trabalho.setArtefactos(artefactos);
            em.merge(trabalho);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
}
