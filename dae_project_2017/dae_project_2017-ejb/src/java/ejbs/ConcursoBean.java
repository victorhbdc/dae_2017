/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.ConcursoDTO;
import entities.trabalhos.CandidaturaATrabalho;
import entities.trabalhos.Concurso;
import entities.trabalhos.Proposta;
import entities.trabalhos.Trabalho;
import exceptions.EntityDoesNotExistException;
import exceptions.MyConstraintViolationException;
import exceptions.PeriodIsNotBeforeException;
import exceptions.PeriodoInvalido;
import exceptions.PropostasAbertas;
import exceptions.TrabalhoPorAtribuirProfessoresException;
import exceptions.TrabalhoSemSupervisorException;
import exceptions.TrabalhosPorSeriarException;
import exceptions.Utils;
import java.util.Collection;
import java.util.GregorianCalendar;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.Estado;
import utils.EstadoTrabalho;
import utils.TipoProponente;
import utils.embeddable.EstadoConcurso;
import utils.embeddable.Periodo;

/**
 *
 * @author vsc
 */
@Stateless
@LocalBean
@Path(ConcursoBean.BASE_REST_PATH)
public class ConcursoBean extends Bean<Concurso> {

    public static final String BASE_REST_PATH = "/concursos";
    
    //TODO CREATE
    
    public static final String REST_PATH_GET_LATEST_CONCURSO
            = BASE_REST_PATH + "/latest";
    
//    public static final String REST_PATH_GET_CANDIDATURAS(EstadoConcurso estadoConcurso) {
//        return BASE_REST_PATH + "/all/estado/" + estadoConcurso;
//    }
    public static final String REST_PATH_AVANCAR_CONCURSO(Long concursoId) {
        return BASE_REST_PATH + "/avancar/" + String.valueOf(concursoId);
    }
    public static final String REST_PATH_ATRASAR_CONCURSO(Long concursoId) {
        return BASE_REST_PATH + "/atrasar/" + String.valueOf(concursoId);
    }


//    public static final String REST_PATH_DELETE(Long concursoId) {
//        return BASE_REST_PATH + "/delete/" + String.valueOf(concursoId);
//    }

    // ####################################################
    public ConcursoBean() {
    }

    // ####################################################
    public long create(
            GregorianCalendar dataAberturaDoConcurso,
            Periodo periodoPropostas,
            Periodo periodoValidacao,
            Periodo periodoCandidaturas,
            Periodo periodoSeriacao,
            Periodo periodoEntregas,
            Periodo periodoMarcacaoProvasPublicas)
            throws PeriodIsNotBeforeException, MyConstraintViolationException {
        try {

            if (!periodoPropostas.isAfter(dataAberturaDoConcurso)) {
                throw new PeriodIsNotBeforeException("Período de propostas é "
                        + "posterior à abertura do concurso");
            }

            if (!periodoPropostas.isBefore(periodoValidacao)) {
                throw new PeriodIsNotBeforeException("Período de propostas não "
                        + "é anterior ao período de validação de propostas");
            }

            if (!periodoValidacao.isBefore(periodoCandidaturas)) {
                throw new PeriodIsNotBeforeException("Período de validação não "
                        + "é anterior ao período de candidaturas");
            }

            if (!periodoCandidaturas.isBefore(periodoSeriacao)) {
                throw new PeriodIsNotBeforeException("Período de candidaturas "
                        + "não é anterior ao período de seriação");
            }

            if (!periodoSeriacao.isBefore(periodoEntregas)) {
                throw new PeriodIsNotBeforeException("Período de seriação não "
                        + "é anterior ao período de entrega");
            }

            if (!periodoEntregas.isBefore(periodoMarcacaoProvasPublicas)) {
                throw new PeriodIsNotBeforeException("Período de entregas não "
                        + "é anterior ao período de marcação de provas públicas");
            }

            Concurso concurso
                    = new Concurso(
                            dataAberturaDoConcurso,
                            periodoPropostas,
                            periodoValidacao,
                            periodoCandidaturas,
                            periodoSeriacao,
                            periodoEntregas,
                            periodoMarcacaoProvasPublicas);

            em.persist(concurso);

            return concurso.getId();

        }  catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        }catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @Override
    protected Collection<Concurso> getAll() {
        try {
            return em.createNamedQuery(Concurso.getAll, Concurso.class)
                    .getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("latest")
    public ConcursoDTO getLatestDtos() {
        try {
            Collection<Concurso> concursos = getAll();
            Concurso latest = null;
            for (Concurso concurso : concursos) {
                if (latest == null || concurso.getDataAberturaDoConcurso()
                        .after(latest.getDataAberturaDoConcurso())) {
                    latest = concurso;
                }
            }
            return toDTO(latest, ConcursoDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

//    @GET
//    @RolesAllowed({})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Path("all/estado/{estado}")
//    public Collection<ConcursoDTO>
//            getAllConcurso(@PathParam("estado") EstadoConcurso estadoConcurso) {
//        try {
//            Collection<Concurso> concursos = getAll();
//            concursos.removeIf(c -> c.getEstadoConcurso() != estadoConcurso);
//
//            //todo make a DTO
//            return toDTOs(concursos, ConcursoDTO.class);
//        } catch (Exception e) {
//            throw new EJBException(e.getMessage());
//        }
//    }

//    @DELETE
//    @RolesAllowed({})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Path("delete/{concursoId}")
//    public void remove(@PathParam("concursoId") Long concursoId)
//            throws EntityDoesNotExistException {
//        Concurso concurso = em.find(Concurso.class, concursoId);
//
//        if (concurso == null) {
//            throw new EntityDoesNotExistException("[PropostaBean.remove]");
//        }
//
//        //TODO e as suas propostas????
////        concurso.getPropostas().forEach((proposta) -> {
////            em.remove(proposta);
////        });
//        em.remove(concurso);
//    }
    
    public void avancarConcursoCB(Long concursoId) 
            throws EntityDoesNotExistException, PeriodoInvalido, PropostasAbertas, 
            TrabalhoSemSupervisorException, TrabalhosPorSeriarException, 
            TrabalhoPorAtribuirProfessoresException, MyConstraintViolationException{
        this.avancarConcurso(concursoId);
    }  

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("avancar/{concursoId}")
    public void avancarConcurso(@PathParam("concursoId") Long concursoId)
            throws EntityDoesNotExistException, PeriodoInvalido, PropostasAbertas, 
            TrabalhoSemSupervisorException, TrabalhosPorSeriarException, 
            TrabalhoPorAtribuirProfessoresException, MyConstraintViolationException{
        try{
            Concurso concurso = em.find(Concurso.class, concursoId);
            if (concurso == null) {
                throw new EntityDoesNotExistException("O Concurso nao existe!");
            }
            switch(concurso.getEstadoConcurso()){
                case ANUNCIADO:
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_PROPOSTAS);
                    break;

                case PERIODO_PROPOSTAS:
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_VALIDACAO);
                    break;

                case PERIODO_VALIDACAO:
                    for (Proposta proposta : concurso.getPropostas()) {
                        if (proposta.getEstado() == Estado.ANALISE) {
                            throw new PropostasAbertas("Ainda ha propostas que faltam avaliar!");
                        }
                    }
                     concurso.setEstadoConcurso(EstadoConcurso.PERIODO_CANDIDATURAS);
                     break;

                case PERIODO_CANDIDATURAS:
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_SERIACAO);
                    break;

                case PERIODO_SERIACAO:
                    //SO PROPOSTAS ACEITES TEM TRABALHO
                     for (Proposta proposta : concurso.getPropostas()) {
                        if (proposta.getEstado() != Estado.ACEITE) {
                            continue;
                        }
                        Trabalho trabalho = proposta.getTrabalho();
                        //SE FOR UM TRABALHO COM ESTUDANTE E PROFESSOR
                        if(trabalho.getEstudante() != null && !trabalho.getProfessores().isEmpty()){
                            //SE NAO TIVER SUPERVISOR E O PROPONENTE FOR UMA INSTITUICAO
                            if(trabalho.getTipoProponente() == TipoProponente.INSTITUICAO && trabalho.getSupervisor() == null){
                                throw new TrabalhoSemSupervisorException();
                            }
                            //SENAO ELE E INICIADO
                            trabalho.setEstadoTrabalho(EstadoTrabalho.INICIADO);
                            em.merge(trabalho);
                            continue;
                        }

                        if(trabalho.getEstudante() == null){
                            //SE ELE NAO TIVER ESTUDANTE E TIVER CANDIDATURAS ABERTAS O CONCURSO NAO PODE AVANCAR
                            for(CandidaturaATrabalho candidatura : trabalho.getCandidaturas()){
                                if(candidatura.getEstado() == Estado.ANALISE){
                                    throw new TrabalhosPorSeriarException();
                                }
                            }
                        } else {
                            //SE FOI ATRIBUIDO UM ALUNO, MAS O TRABALHO NAO TIVER PROFESSORES
                            throw new TrabalhoPorAtribuirProfessoresException();
                        }
                    }
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_ENTREGA_TRABALHOS);
                    break;

                case PERIODO_ENTREGA_TRABALHOS:
                    //SO PROPOSTAS ACEITES TEM TRABALHO
                    for (Proposta proposta : concurso.getPropostas()) {
                        if (proposta.getEstado() != Estado.ACEITE) {
                            continue;
                        }
                        Trabalho trabalho = proposta.getTrabalho();
                        if(trabalho.getEstadoTrabalho() == EstadoTrabalho.INICIADO){
                            trabalho.setEstadoTrabalho(EstadoTrabalho.ENTREGUE);
                        }
                    }
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_MARCACAO_PROVAS_PUBLICAS);
                    break;

                case PERIODO_MARCACAO_PROVAS_PUBLICAS:
                    //DO NOTHING, CONCURSO CHEGOU AO FIM
            }
            em.merge(concurso);
            
        } catch (EntityDoesNotExistException | PropostasAbertas | TrabalhoSemSupervisorException | 
                TrabalhosPorSeriarException | TrabalhoPorAtribuirProfessoresException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("atrasar/{concursoId}")
    public void atrasarConcurso(@PathParam("concursoId") Long concursoId)
            throws EntityDoesNotExistException, PeriodoInvalido, PropostasAbertas, 
            TrabalhoSemSupervisorException, TrabalhosPorSeriarException, 
            TrabalhoPorAtribuirProfessoresException, MyConstraintViolationException{
        try{
            Concurso concurso = em.find(Concurso.class, concursoId);
            if (concurso == null) {
                throw new EntityDoesNotExistException("O Concurso nao existe!");
            }
            switch(concurso.getEstadoConcurso()){
                case ANUNCIADO:
                    //DO NOTHING
                    break;

                case PERIODO_PROPOSTAS:
                    concurso.setEstadoConcurso(EstadoConcurso.ANUNCIADO);
                    break;

                case PERIODO_VALIDACAO:
                     concurso.setEstadoConcurso(EstadoConcurso.PERIODO_PROPOSTAS);
                     break;

                case PERIODO_CANDIDATURAS:
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_VALIDACAO);
                    break;

                case PERIODO_SERIACAO:
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_CANDIDATURAS);
                    break;

                case PERIODO_ENTREGA_TRABALHOS:
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_SERIACAO);
                    break;

                case PERIODO_MARCACAO_PROVAS_PUBLICAS:
                    concurso.setEstadoConcurso(EstadoConcurso.PERIODO_ENTREGA_TRABALHOS);
                    break;
            }
            
            em.merge(concurso);
            
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    
//    @GET
//    @RolesAllowed({})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Path("avancar/trabalho-finalizado/{concursoId}")
//    public void avancarTrabalhoFinalizado(@PathParam("concursoId") Long concursoId)
//            throws EntityDoesNotExistException, PeriodoInvalido {
//        Concurso concurso = em.find(Concurso.class, concursoId);
//
//       
//        if (concurso == null) {
//            throw new EntityDoesNotExistException("[Trabalho avancar para finalizado]");
//        }
//        if (concurso.getEstadoConcurso() == EstadoConcurso.PERIODO_MARCACAO_PROVAS_PUBLICAS) {
//            //Quer dizer que está no periodo de marcaçao de provas publicas e pode passar para entregue.
//            
//        }
//        for (Proposta proposta : concurso.getPropostas()) {
//            if (proposta.getEstado() != Estado.ACEITE) {
//                continue;
//            }
//            //PARA TODAS AS PROPOSTAS ACEITES
//            Trabalho trabalho = proposta.getTrabalho();
//            //SE FOR UM TRABALHO COM ESTUDANTE E PROFESSOR
//            if(trabalho.isIniciado()){
//                trabalho.setFinalizado(true);
//                continue;
//            }
//
//        }
//        concurso.setEstadoConcurso(EstadoConcurso.FINALIZADO);
//        em.merge(concurso);
//    }
}
