/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.CandidaturaDTO;
import dtos.PropostaDTO;
import dtos.SupervisorDTO;
import dtos.TrabalhoDTO;
import entities.roles.Instituicao;
import entities.trabalhos.Proposta;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.GregorianCalendar;
import javax.annotation.PostConstruct;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RunAs;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import utils.embeddable.Periodo;
import utils.TipoTrabalho;
import utils.embeddable.Orcamento;

/**
 *
 * @author vsc
 */

@Singleton
@Startup
public class ConfigBean {
    public static final String MESTRADO = "Mestrado em Cibersegurança e Informática Forense";
    
    @PersistenceContext
    EntityManager em;

    @EJB
    EstudanteBean estudanteBean;

    @EJB
    ProfessorBean professorBean;

    @EJB
    InstituicaoBean instituicaoBean;

    @EJB
    SupervisorBean supervisorBean;

    @EJB
    PropostaBean propostaBean;

    @EJB
    ConcursoBean concursoBean;

    @EJB
    CandidaturaBean candidaturaBean;

    @EJB
    MembroComissaoCPBean membroCCPBean;

    @EJB
    TrabalhoBean trabalhoBean;

    @EJB
    ProvaPublicaBean provaPublicaBean;

    @PostConstruct
    public void populateBD() {
        try {
            estudanteBean.create("vsc", "vsc", "vasco", "2151218@my.ipleiria.pt");
            estudanteBean.create("vic", "vic", "Victor", "2151151@my.ipleiria.pt");
            //estudanteBean.create("hul", "ccp", "Victor", "2151151@my.ipleiria.pt");
            estudanteBean.create("favagama", "favagama", "Pedro","favagamas@gmail.com");

            professorBean.create("grilo", "ia", "Carlos Grilo", "favagama@gmail.com");
            professorBean.create("martinho", "dae", "Ricardo Martinho", "victorhbdc@gmail.com");
            professorBean.create("pt1", "pt1", "Professor de Testes", "victorhbdc@gmail.com");

            Instituicao instituicao1 = instituicaoBean.create("ipl", "ipl", "IPLeiria",
                    "2151218@ipleiria.paa", "Politecnico");

            Instituicao instituicao2 = instituicaoBean.create("estg", "estg", "ESTG",
                    "2151218@ipleiraaia.paa", "Escola");

            supervisorBean.createCB(new SupervisorDTO("bot1", "bot1@bots.com", instituicao1.getUsername()));
            supervisorBean.createCB(new SupervisorDTO("bot2", "bot2@bots.com", instituicao2.getUsername()));
            supervisorBean.createCB(new SupervisorDTO("bot3", "bot3@bots.com", instituicao1.getUsername()));

            membroCCPBean.create("membroccp", "membroccp", "Conta 1 da CCP", "ccp_dei@ipleiria.pt");
            //membroCCPBean.create("ccp", "ccp", "Conta 2 da CCP", "ccp_dei@ipleiria.pt");
            membroCCPBean.create("mccp", "mccp", "Conta 2 da CCP", "ccp_dei@ipleiria.pt");

            concursoBean.create(
                    new GregorianCalendar(2016, 10, 15),
                    new Periodo(new GregorianCalendar(2016, 11, 1), new GregorianCalendar(2016, 11, 31)),
                    new Periodo(new GregorianCalendar(2017, 0, 1), new GregorianCalendar(2017, 0, 31)),
                    new Periodo(new GregorianCalendar(2017, 1, 1), new GregorianCalendar(2017, 1, 25)),
                    new Periodo(new GregorianCalendar(2017, 2, 1), new GregorianCalendar(2017, 2, 31)),
                    new Periodo(new GregorianCalendar(2017, 3, 1), new GregorianCalendar(2017, 3, 30)),
                    new Periodo(new GregorianCalendar(2017, 4, 1), new GregorianCalendar(2017, 4, 31))
            );

            /*long concursoId = concursoBean.create(
                    new GregorianCalendar(2017, 10, 15),
                    new Periodo(new GregorianCalendar(2017, 11, 1), new GregorianCalendar(2017, 11, 31)),
                    new Periodo(new GregorianCalendar(2018, 0, 1), new GregorianCalendar(2018, 0, 31)),
                    new Periodo(new GregorianCalendar(2018, 1, 1), new GregorianCalendar(2018, 1, 25)),
                    new Periodo(new GregorianCalendar(2018, 2, 1), new GregorianCalendar(2018, 2, 31)),
                    new Periodo(new GregorianCalendar(2018, 3, 1), new GregorianCalendar(2018, 3, 30)),
                    new Periodo(new GregorianCalendar(2018, 4, 1), new GregorianCalendar(2018, 4, 31))
            );

            // ################################################################
            // FASE PROPOSTAS
            concursoBean.avancarConcursoCB(concursoId);
            // ################################################################
            Orcamento orcamento
                    = new Orcamento(
                            BigDecimal.ONE,
                            BigDecimal.TEN,
                            BigDecimal.valueOf(2.2),
                            BigDecimal.valueOf(3)
                    );

            PropostaDTO propostaDTO1 = new PropostaDTO(
                    MESTRADO,
                    concursoId,
                    TipoTrabalho.DISSERTACAO,
                    "Proposta DAE",
                    "A primeira dissertação deste trabalho",
                    new BigDecimal(20),
                    new BigDecimal(45),
                    new BigDecimal(70),
                    new BigDecimal(12),
                    "janeiro: investigação, fevereiro: desenvolvimento do trabalho, março: relatorio",
                    Arrays.asList("Inteligência Artificial", "Metodologia e Tecnologia de Programação"),
                    Arrays.asList("martinho", "grilo")
            );
            propostaDTO1.addApoio("Estado, ha!");
            propostaDTO1.addObjectivos("Acabar o trabalho de DAE!");
            propostaDTO1.addRequisitoTrabalho("Glassfish nao rebentar");
            propostaDTO1.setObservacoes("dae!");
            propostaDTO1.addItemBibliograficoStrings("Titulo_", "Autor_");
            propostaBean.createCB(propostaDTO1);

            PropostaDTO propostaDTO2 = new PropostaDTO(
                    MESTRADO,
                    concursoId,
                    TipoTrabalho.ESTAGIO,
                    "Proposta IPLEIRIA",
                    "O primeiro estagio deste trabalho",
                    new BigDecimal(10),
                    new BigDecimal(20),
                    new BigDecimal(30),
                    new BigDecimal(12),
                    "janeiro: trabalhar, fevereiro: trabalhar, marco: trabalhar",
                    Arrays.asList("Sistemas de Informação"),
                    Arrays.asList("ipl")
            );
            propostaDTO2.setLocalRealizacao("Leiria");
            propostaDTO2.setObservacoes("muito bom!");
            propostaBean.createCB(propostaDTO2);

            PropostaDTO propostaDTO3 = new PropostaDTO(
                    MESTRADO,
                    concursoId,
                    TipoTrabalho.ESTAGIO,
                    "Proposta ESTG",
                    "O segundo estagio deste trabalho",
                    new BigDecimal(10),
                    new BigDecimal(20),
                    new BigDecimal(33),
                    new BigDecimal(12),
                    "janeiro: trabalhar, fevereiro: trabalhar, marco: trabalhar",
                    Arrays.asList("Sistemas de Bases de Dados"),                    
                    Arrays.asList("estg")
            );
            propostaDTO3.setLocalRealizacao("Pombal");
            propostaDTO3.setObservacoes("muito bom! x2");
            propostaBean.createCB(propostaDTO3);

            PropostaDTO propostaDTO4 = new PropostaDTO(
                    MESTRADO,
                    concursoId,
                    TipoTrabalho.ESTAGIO,
                    "Proposta IPLEIRIA 2",
                    "O terceiro estagio deste trabalho",
                    new BigDecimal(10),
                    new BigDecimal(20),
                    new BigDecimal(30),
                    new BigDecimal(12),
                    "janeiro: trabalhar, fevereiro: trabalhar, marco: trabalhar",
                    Arrays.asList("Excel"),
                    Arrays.asList("ipl")
            );
            propostaDTO4.setLocalRealizacao("Leiria");
            propostaDTO4.setObservacoes("muito bom!x3");
            propostaBean.createCB(propostaDTO4);

            PropostaDTO propostaDTO5 = new PropostaDTO(
                    MESTRADO,
                    concursoId,
                    TipoTrabalho.PROJECTO,
                    "Proposta Segurança",
                    "O primeiro projeto deste projeto deste projeto deste pro...",
                    new BigDecimal(20),
                    new BigDecimal(45),
                    new BigDecimal(70),
                    new BigDecimal(12),
                    "janeiro: cifrar, fevereiro: codificar, março: encriptar",
                    Arrays.asList("Segurança de Informação", "Sistemas de Informação"),
                    Arrays.asList("martinho", "grilo")
            );
            propostaDTO5.addApoio("Estado");
            propostaDTO5.addObjectivos("Acabar o trabalho de DAE!");
            propostaDTO5.addRequisitoTrabalho("Glassfish rebentar");
            propostaDTO5.setObservacoes("dae!");
            propostaBean.createCB(propostaDTO5);

            PropostaDTO propostaDTO6 = new PropostaDTO(
                    MESTRADO,
                    concursoId,
                    TipoTrabalho.DISSERTACAO,
                    "Proposta Redes",
                    "A segunda dissertacao deste trabalho",
                    new BigDecimal(20),
                    new BigDecimal(45),
                    new BigDecimal(70),
                    new BigDecimal(12),
                    "janeiro: passar cabo, fevereiro: passar cabo, março: passar cabo",
                    Arrays.asList("Redes nao tem nada de cientifico"),
                    Arrays.asList("martinho", "grilo")
            );
            propostaDTO6.addApoio("Cisco");
            propostaDTO6.addRequisitoTrabalho("ser bom a passar cabo");
            propostaDTO6.setObservacoes("RC!");
            propostaBean.createCB(propostaDTO6);

            // ################################################################
            // FASE VALIDACAO PROPOSTAS
            concursoBean.avancarConcursoCB(concursoId);
            // ################################################################

            propostaDTO1.setId(1l);
            propostaDTO2.setId(2l);
            propostaDTO3.setId(3l);
            propostaDTO4.setId(4l);
            propostaDTO5.setId(5l);
            propostaDTO6.setId(6l);
            
            trabalhoBean.createCB(propostaDTO1);
            trabalhoBean.createCB(propostaDTO2);
            trabalhoBean.createCB(propostaDTO3);
            trabalhoBean.createCB(propostaDTO4);
            trabalhoBean.createCB(propostaDTO5);
            trabalhoBean.createCB(propostaDTO6);

            // ################################################################
            // FASE CANDIDATURAS
            concursoBean.avancarConcursoCB(concursoId); */
            // ################################################################
//
//            CandidaturaDTO candidaturaDTO1 = new CandidaturaDTO("vsc", 1l, "", new BigDecimal(9));
//            candidaturaBean.createCB(candidaturaDTO1);
//            CandidaturaDTO candidaturaDTO2 = new CandidaturaDTO("vsc", 2l, "", new BigDecimal(8));
//            candidaturaBean.createCB(candidaturaDTO2);
//
//            // ################################################################
//            // FASE SERIACAO
//            concursoBean.avancarConcursoCB(concursoId);
//            // ################################################################
//
//            //EMAILS COMMENTADOS, DESCOMENTAR ANTES DA ENTREGA
//            candidaturaBean.seriacaoAutomaticaCB();
//
//            // ################################################################
//            // FASE DESENVOLVIMENTO/ENTREGA TRABALHO
//            concursoBean.avancarConcursoCB(concursoId);
            // ################################################################

//            // ################################################################
//            // FASE MARCACAO PROVAS PUBLICAS
//            concursoBean.avancarConcursoCB(concursoId);
//            // ################################################################
//
//            provaPublicaBean.create(1L, (new GregorianCalendar(2018, 2, 10)).getTime(), "G.1.18", "mccp", "grilo", "testes1", "estes1@gmail.com");

            //   US9 TODO
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
