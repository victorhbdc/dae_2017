/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.DocumentoDTO;
import dtos.ProvaPublicaDTO;
import entities.roles.MembroComissaoCP;
import entities.roles.Professor;
import entities.roles.User;
import entities.trabalhos.Juri;
import entities.trabalhos.ProvaPublica;
import entities.trabalhos.Trabalho;
import exceptions.EntityDoesNotExistException;
import exceptions.MyConstraintViolationException;
import exceptions.PeriodoInvalido;
import exceptions.Utils;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.EstadoTrabalho;
import utils.embeddable.Documento;

/**
 *
 * @author vsc
 */
@Stateless
@Path(ProvaPublicaBean.BASE_REST_PATH)
public class ProvaPublicaBean extends Bean<ProvaPublica> {

    public static final String BASE_REST_PATH = "/provapublica";

//    public static final String REST_PATH_GET_ALL = BASE_REST_PATH + "/all";
    public static final String REST_PATH_PUT_CREATE = BASE_REST_PATH + "/create";

    // public static final String REST_PATH_PUT_UPDATE = BASE_REST_PATH + "/update";
    public static String REST_PATH_GET_PROVA_PUBLICA(String provaPublicaId) {
        return BASE_REST_PATH + "/find/" + provaPublicaId;
    }

    public static String REST_PATH_PUT_SET_ATA(String provaPublicaId) {
        return BASE_REST_PATH + "/set/ata/" + provaPublicaId;
    }

    public static final String REST_PATH_DELETE(String id) {
        return BASE_REST_PATH + "/delete/provapublica/" + id;
    }

    public static final String REST_PATH_DELETE_ATA(String id) {
        return BASE_REST_PATH + "/delete/ata/" + id;
    }

    // ####################################################
    @EJB
    EmailBean emailBean;
    // ####################################################

    public ProvaPublicaBean() {
    }

    // ####################################################
    @PUT
    @RolesAllowed({"MembroCCP"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("create")
    public void create(
            ProvaPublicaDTO provaPublicaDTO)
            throws EntityDoesNotExistException, MyConstraintViolationException, PeriodoInvalido {

        this.create(provaPublicaDTO.getTrabalhoId(),
                provaPublicaDTO.getData(),
                provaPublicaDTO.getLocal(),
                provaPublicaDTO.getJuriMembroCCPUsername(),
                provaPublicaDTO.getJuriOrientadorUsername(),
                provaPublicaDTO.getJuriProfessorNome(),
                provaPublicaDTO.getJuriProfessorEmail()
        );
    }

    public void create(
            Long trabalhoID,
            Date data,
            String local,
            String membroCCPUsername,
            String orientadorUsername,
            String professorNome,
            String professorEmail)
            throws EntityDoesNotExistException, MyConstraintViolationException {

        try {

            if (data.before(new Date())) {
                throw new PeriodoInvalido("A data indicada é inválida");
            }

            Trabalho trabalho = em.find(Trabalho.class, trabalhoID);
            if (trabalho == null) {
                throw new EntityDoesNotExistException("Não existe nenhum Trabalho com esse username");
            }

            if (trabalho.getProvaPublica() != null) {
                throw new IllegalArgumentException("O trabalho já tem uma prova pública agendada");
            }

            MembroComissaoCP mccp = em.find(MembroComissaoCP.class, membroCCPUsername);
            if (mccp == null) {
                throw new EntityDoesNotExistException("Não existe nenhum Professor com esse username");
            }

            Professor orientador = em.find(Professor.class, orientadorUsername);
            if (orientador == null) {
                throw new EntityDoesNotExistException("Não existe nenhum Orientador com esse username");
            }

            Juri juri = new Juri(mccp, orientador, professorNome, professorEmail);
            ProvaPublica provaPublica = new ProvaPublica(juri, trabalho, data, local);

            juri.setProvaPublica(provaPublica);
            trabalho.setProvaPublica(provaPublica);
            mccp.addJuri(juri);
            orientador.addJuri(juri);
            
            em.persist(juri);
            em.persist(provaPublica);
            em.merge(trabalho);
            em.merge(mccp);
            em.merge(orientador);

            // ##### SEND EMAILS
            //todo
            Hashtable<String, User> users = new Hashtable<>(10);

            users.put("Estudante", provaPublica.getTrabalho().getEstudante());
            users.put("Membro da CPP", provaPublica.getJuri().getMembroCCP());
            users.put("Orientador", provaPublica.getJuri().getOrientador());

            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy 'as' HH:mm:ss");
            fmt.setCalendar(new GregorianCalendar());

            for (String k : users.keySet()) {
                emailBean.send(
                        users.get(k).getEmail(),
                        "Prova Pública MARCADA",
                        mailTemplateProvaPublicaCriada(trabalho, provaPublica)
                );
            }

            // Email ao PREFESSOR externo
            emailBean.send(
                    professorEmail,
                    "Prova Pública MARCADA",
                    mailTemplateProvaPublicaCriada(trabalho, provaPublica)
            );

        } catch (EntityDoesNotExistException | MessagingException | PeriodoInvalido e) {
            throw new EJBException(e.getMessage());
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    

    @Override
    protected Collection<ProvaPublica> getAll() {

        return em.createNamedQuery(ProvaPublica.getAll, ProvaPublica.class)
                .getResultList();
    }

//    @GET
//    @RolesAllowed({})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Path("all")
//    public Collection<ProvaPublicaDTO> getAllDTOs()
//            throws EntityDoesNotExistException {
//        try {
//
//            return toDTOs(getAll(), ProvaPublicaDTO.class
//            );
//        } catch (Exception e) {
//            throw new EJBException(e.getMessage());
//        }
//    }
//    @GET
//    @RolesAllowed({})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Path("all/marcadas")
//    public Collection<ProvaPublicaDTO> getAllMarcadasDTOs()
//            throws EntityDoesNotExistException {
//        try {
//
//            return toDTOs(
//                    em.createNamedQuery(ProvaPublica.getAllMarcadas, ProvaPublica.class)
//                            .getResultList(),
//                    ProvaPublicaDTO.class
//            );
//        } catch (Exception e) {
//            throw new EJBException(e.getMessage());
//        }
//    }
    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("find/{id}")
    public ProvaPublicaDTO find(@PathParam("id") Long id)
            throws EntityDoesNotExistException {
        try {
            ProvaPublica provaPublica = em.find(ProvaPublica.class, id);
            if (provaPublica == null) {
                throw new EntityDoesNotExistException("A prova publica nao existe");
            }
            return toDTO(provaPublica, ProvaPublicaDTO.class);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @PUT
    @RolesAllowed({"MembroCCP"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("set/ata/{provaPublicaId}")
    public void setAta(
            @PathParam("provaPublicaId") Long provaPublicaId,
            DocumentoDTO ata)
            throws EntityDoesNotExistException, MyConstraintViolationException {

        try {
            ProvaPublica provaPublica = em.find(ProvaPublica.class, provaPublicaId);
            if (provaPublica == null) {
                throw new EntityDoesNotExistException("A prova pública não existe!");
            }

            Documento documento
                    = new Documento(Documento.TipoDeDocumento.ATA, ata.getNome(), ata.getPath());

            documento.setMimeType(ata.getMimeType());

            provaPublica.setAta(documento);

            Trabalho trabalho = provaPublica.getTrabalho();

            trabalho.setEstadoTrabalho(EstadoTrabalho.FINALIZADO);

            em.merge(provaPublica);
            em.merge(trabalho);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @DELETE
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("delete/provapublica/{id}")
    public void remove(@PathParam("id") Long id) throws EntityDoesNotExistException, PeriodoInvalido, MyConstraintViolationException, MessagingException {

        try {
            ProvaPublica provaPublica = em.find(ProvaPublica.class, id);

            if (provaPublica == null) {
                throw new EntityDoesNotExistException("[ProvaPublicaBean.remove]");
            }

            if (provaPublica.getEstadoProvaPublica() != ProvaPublica.EstadoProvaPublica.MARCADA) {
                throw new PeriodoInvalido("A prova pública não pode ser cancelada");
            }

            Trabalho trabalho = provaPublica.getTrabalho();
            if (trabalho.getEstadoTrabalho() != EstadoTrabalho.ENTREGUE) {
                throw new PeriodoInvalido("A prova publica nao pode ser removido");
            }

            provaPublica.getTrabalho().setProvaPublica(null);
            em.merge(provaPublica.getTrabalho());

            Juri juri = provaPublica.getJuri();

            juri.getMembroCCP().removeJuri(juri);
            em.merge(juri.getMembroCCP());
            juri.getOrientador().removeJuri(juri);
            em.merge(juri.getOrientador());

            em.remove(juri);
            em.remove(provaPublica);

            // ### SEND EMAILS
            List<String> emails = new LinkedList();
            //Adicionar mail dos envolvidos na prova publica estudante , membro ccp, orientador e um professor

            emails.add(provaPublica.getTrabalho().getEstudante().getEmail());
            emails.add(provaPublica.getJuri().getMembroCCP().getEmail());
            emails.add(provaPublica.getJuri().getOrientador().getEmail());
            emails.add(provaPublica.getJuri().getProfessorExternoEmail());

            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy 'as' HH:mm:ss");
            fmt.setCalendar(new GregorianCalendar());

            for (String to : emails) {
                emailBean.send(
                        to,
                        "Prova Pública CANCELADA",
                        "Gostariamos de o informar que a seguinte prova pública "
                        + "para a qual tinha sido convocado(a) foi CANCELADA. \n\n "
                        + "Detalhes \n\n"
                        + "Titulo do Trabalho: " + provaPublica.getTrabalho().getTitulo() + "\n"
                        + "Data: " + fmt.format(provaPublica.getData()) + "\n"
                        + "Local: " + provaPublica.getLocal() + "\n"
                        + "Estudante: " + provaPublica.getTrabalho().getEstudante().getNome() + "\n"
                        + "Juri:\n"
                        + "-Membro da CCP: " + provaPublica.getJuri().getMembroCCP().getNome() + "\n"
                        + "-Orientador: " + provaPublica.getJuri().getOrientador().getNome() + "\n"
                        + "-Professor: " + provaPublica.getJuri().getProfessorExternoNome() + "\n"
                );
            }
        } catch (EntityDoesNotExistException | PeriodoInvalido /*| MessagingException*/ e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @DELETE
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("delete/ata/{id}")
    public void removeAta(@PathParam("id") Long id)
            throws EntityDoesNotExistException, MyConstraintViolationException {

        try {
            ProvaPublica provaPublica = em.find(ProvaPublica.class, id);

            if (provaPublica == null) {
                throw new EntityDoesNotExistException("[ProvaPublicaBean.remove]");
            }

            if (provaPublica.getTrabalho().getEstadoTrabalho() != EstadoTrabalho.FINALIZADO
                    || provaPublica.getAta() == null) {
                throw new PeriodoInvalido("A ata prova não pode ser removida");
            }

            provaPublica.setAta(null);
            provaPublica.getTrabalho().setEstadoTrabalho(EstadoTrabalho.ENTREGUE);

            em.merge(provaPublica);
            em.merge(provaPublica.getTrabalho());

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    private String mailTemplateProvaPublicaCriada(Trabalho trabalho, ProvaPublica provaPublica) {

        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy 'as' HH:mm:ss");
        fmt.setCalendar(new GregorianCalendar());

        StringBuilder sb = new StringBuilder();
        sb.append("Gostariamos de o informar que está convicado para "
                + "a seguinte prova pública\n\n "
                + "Detalhes \n\n"
                + "Titulo do Trabalho: " + provaPublica.getTrabalho().getTitulo() + "\n"
                + "Data: " + fmt.format(provaPublica.getData()) + "\n"
                + "Local: " + provaPublica.getLocal() + "\n"
                + "Estudante: " + provaPublica.getTrabalho().getEstudante().getNome() + "\n"
                + "Juri:\n"
                + "-Membro da CCP: " + provaPublica.getJuri().getMembroCCP().getNome() + "\n"
                + "-Orientador: " + provaPublica.getJuri().getOrientador().getNome() + "\n"
                + "-Professor: " + provaPublica.getJuri().getProfessorExternoNome() + "\n"
                + "\n\n");

        if (trabalho.getArtefactos().size() > 0) {
            sb.append("Os links para os artefactos do projecto seguem a baixo: \n");

            for (Documento d : trabalho.getArtefactos()) {
                sb.append("- ");
                sb.append(linkToDocument(trabalho.getId(), d.getNome()));
                sb.append("\n");
            }
        }else{
            sb.append("O Trabalho não tem artefctos disponíveis \n");
        }
        
        return sb.toString();
    }

    private String linkToDocument(Long trabalhoId, String documentNome) {
        return "http://localhost:8080/dae_project_2017-war/faces/download_file.xhtml?"
                + "trabalhoid=" + String.valueOf(trabalhoId)
                + "&documentonome=" + documentNome;
    }

}
