/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.InstituicaoDTO;
import entities.roles.Instituicao;
import entities.roles.User;
import exceptions.EntityDoesNotExistException;
import exceptions.EntityExistsException;
import exceptions.MyConstraintViolationException;
import exceptions.Utils;
import java.util.Collection;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.CreateUserUtil;

/**
 *
 * @author vsc
 */
@Stateless
@LocalBean
@Path(InstituicaoBean.BASE_REST_PATH)
public class InstituicaoBean extends Bean<Instituicao> {

    public static final String BASE_REST_PATH = "/instituicoes";

    public static final String REST_PATH_GET_ALL = BASE_REST_PATH + "/all";

    public static final String REST_PATH_PUT_CREATE = BASE_REST_PATH + "/create";

    public static final String REST_PATH_PUT_UPDATE = BASE_REST_PATH + "/update";

    public static final String REST_PATH_DELETE(String username) {
        return BASE_REST_PATH + "/delete/" + username;
    }

    public static final String REST_PATH_GET_FIND(String username) {
        return BASE_REST_PATH + "/find/" + username;
    }

    // ####################################################
    @EJB
    EmailBean emailBean;

    // ####################################################
    public InstituicaoBean() {
    }

    // ####################################################
    public void create(
            String username,
            String nome,
            String email,
            String tipoInstituicao)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {
        String generatedPassword = CreateUserUtil.generateRandomPassword(12);
        this.create(username, generatedPassword, nome, email, tipoInstituicao);
    }

    public Instituicao create(
            String username,
            String password,
            String nome,
            String email,
            String tipoInstituicao)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {
        try {
            
            User user = em.find(User.class, username);
            if (user != null) {
                throw new EntityExistsException("O estudante já existe");
            }
            
            Instituicao instituicao
                    = new Instituicao(username, password, nome, email, tipoInstituicao);

            em.persist(instituicao);

            CreateUserUtil.sendNotificationEmail(emailBean, nome, email, username, password);

            return instituicao;
        } catch (EntityExistsException | MessagingException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @Override
    protected List<Instituicao> getAll() {
        try {
            return em.createNamedQuery(Instituicao.getAll, Instituicao.class)
                    .getResultList();
        } catch (Exception e) {
            //throw new EJBException(e.getMessage());
            return null;
        }
    }

    @PUT
    @RolesAllowed({"MembroCCP"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("create")
    public void create(InstituicaoDTO instituicaoDTO)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {

        this.create(
                instituicaoDTO.getUsername(),
                instituicaoDTO.getNome(),
                instituicaoDTO.getEmail(),
                instituicaoDTO.getTipoInstituicao());
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all")
    public Collection<InstituicaoDTO> getAllDTOs() {
        try {
            return toDTOs(getAll(), InstituicaoDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @PUT
    @RolesAllowed({"MembroCCP"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("update")
    public void update(InstituicaoDTO instituicaoDTO) {
        try {
            String username = instituicaoDTO.getUsername();
            String password = instituicaoDTO.getPassword();
            String nome = instituicaoDTO.getNome();
            String email = instituicaoDTO.getEmail();

            Instituicao instituicao = em.find(Instituicao.class, username);
            if (instituicao == null) {
                throw new EntityDoesNotExistException("A Instituição não Existe");
            }

            if (!instituicao.getEmail().equals(email)) {
                instituicao.setEmail(email);
            }

            if (!instituicao.getNome().equals(nome)) {
                instituicao.setNome(nome);
            }

            if (password != null && !password.isEmpty()) {
                instituicao.setPassword(password);
            }

            em.merge(instituicao);

        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @DELETE
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("delete/{username}")
    public void remove(@PathParam("username") String username)
            throws EntityDoesNotExistException {

        Instituicao instituicao = em.find(Instituicao.class, username);

        if (instituicao == null) {
            throw new EntityDoesNotExistException("[InstituicaoBean.remove]");
        }

        em.remove(instituicao);

    }

    @GET
    @RolesAllowed({"Proponente"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("find/{username}")
    public InstituicaoDTO findInstituicao(@PathParam("username") String username) {

        Instituicao instituicao = em.find(Instituicao.class, username);
        if (instituicao == null) {
            return null;
        }
        return toDTO(instituicao, InstituicaoDTO.class);
    }
}
