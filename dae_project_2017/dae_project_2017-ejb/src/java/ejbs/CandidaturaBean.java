/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.CandidaturaDTO;
import dtos.DocumentoDTO;
import entities.roles.Estudante;
import entities.roles.Proponente;
import entities.trabalhos.CandidaturaATrabalho;
import entities.trabalhos.Concurso;
import entities.trabalhos.Trabalho;
import exceptions.EntityDoesNotExistException;
import exceptions.EntityExistsException;
import exceptions.LimiteDeCandidaturasException;
import exceptions.EstudanteJaTemTrabalhoException;
import exceptions.MyConstraintViolationException;
import exceptions.PeriodoInvalido;
import exceptions.TrabalhoJaTemEstudanteException;
import exceptions.Utils;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.Estado;
import utils.embeddable.Documento;
import utils.embeddable.EstadoConcurso;

/**
 *
 * @author pedroalves
 */
@Stateless
@Path(CandidaturaBean.BASE_REST_PATH)
public class CandidaturaBean extends Bean<CandidaturaATrabalho> {

    public static final String BASE_REST_PATH = "/candidaturas";

//    public static final String REST_PATH_GET_ALL = BASE_REST_PATH + "/all";
    public static final String REST_PATH_GET_ALL(String username) {
        return BASE_REST_PATH + "/all/" + username;
    }
    public static final String REST_PATH_CREATE = BASE_REST_PATH + "/create";

    public static final String REST_PATH_UPDATE = BASE_REST_PATH + "/update";

    public static final String REST_PATH_DELETE(String candidaturaId) {
        return BASE_REST_PATH + "/delete/" + candidaturaId;
    }

    public static String REST_PATH_ACEITAR(String candidaturaId) {
        return BASE_REST_PATH + "/aceitar/" + candidaturaId;
    }
    public static final String REST_PATH_SERIAR_AUTO = BASE_REST_PATH + "/seriar/all";

    // ####################################################
    @EJB
    EmailBean emailBean;

    // ####################################################
    public void createCB(CandidaturaDTO candidaturaDTO)
            throws EntityExistsException, EntityDoesNotExistException,
            PeriodoInvalido, LimiteDeCandidaturasException {
        this.create(candidaturaDTO);
    }
    
    
    
    @POST
    @RolesAllowed({"Estudante"})
    @Path("create")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(CandidaturaDTO candidaturaDTO)
            throws EntityExistsException, EntityDoesNotExistException,
            PeriodoInvalido, LimiteDeCandidaturasException {
        try {
            Long trabalhoId = candidaturaDTO.getTrabalhoId();
            String estudanteUsername = candidaturaDTO.getEstudanteUsername();
            List<DocumentoDTO> documentoDTOs = candidaturaDTO.getDocumentos();
            BigDecimal media = candidaturaDTO.getMedia();

            //trabalho existe
            Trabalho trabalho = em.find(Trabalho.class, trabalhoId);
            if (trabalho == null) {
                throw new EntityDoesNotExistException("O trabalho não existe");
            }

            //verificar que o concurso se encontra na fase de candidaturas
            if (trabalho.getProposta().getConcurso().getEstadoConcurso() != EstadoConcurso.PERIODO_CANDIDATURAS) {
                throw new PeriodoInvalido("O concurso associado ao trabalho não se encontra na fase de candidaturas");
            }

            //estudante existe
            Estudante estudante = em.find(Estudante.class, estudanteUsername);
            if (estudante == null) {
                throw new EntityDoesNotExistException("O estudante não existe");
            }

            //o estudanteUsername ainda não concorreu ao trabalho
            for (CandidaturaATrabalho ca : trabalho.getCandidaturas()) {
                if (ca.getEstudante().getUsername().equals(estudanteUsername)) {
                    throw new EntityExistsException("A candidatura já existe");
                }
            }

            if (estudante.getCandidaturasATrabalhos().size() >= 5) {
                throw new LimiteDeCandidaturasException();
            }

            List<Documento> documentos = new LinkedList<>();
            for (DocumentoDTO dto : documentoDTOs) {
                Documento documento
                        = new Documento(
                                Documento.TipoDeDocumento.CANDIDATURA,
                                dto.getNome(),
                                dto.getPath()
                        );

                if (dto.getMimeType() != null) {
                    documento.setMimeType(dto.getMimeType());
                }
                documentos.add(documento);
            }

            CandidaturaATrabalho candidatura
                    = new CandidaturaATrabalho(estudante, trabalho, documentos, media);

            trabalho.addCandidaturas(candidatura);
            estudante.addCandidaturasATrabalhos(candidatura);

            em.persist(candidatura);
            em.merge(trabalho);
            em.merge(estudante);

            return;
        } catch (EntityExistsException | EntityDoesNotExistException
                | PeriodoInvalido | LimiteDeCandidaturasException e) {
            throw e;
        } catch (Exception e) {
            //throw new EJBException(e.getMessage());
            throw e;
        }
    }

    @Override
    protected List<CandidaturaATrabalho> getAll() {
        try {
            return em.createNamedQuery(
                    CandidaturaATrabalho.getAll,
                    CandidaturaATrabalho.class
            ).getResultList();

        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    protected List<CandidaturaATrabalho> getAllOrdenadoPorMediaEDataCriacao() {
        try {
            return em.createNamedQuery(
                    CandidaturaATrabalho.getAllOrdenadoPorMediaDescDataAsc,
                    CandidaturaATrabalho.class
            ).getResultList();

        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

//    @GET
//    @RolesAllowed({})
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
//    @Path("all")
//    public Collection<CandidaturaDTO> getAllDTOs()
//            throws EntityDoesNotExistException {
//        try {
//            return toDTOs(getAll(), CandidaturaDTO.class);
//        } catch (Exception e) {
//            throw new EJBException(e.getMessage());
//        }
//    }
    @GET
    @RolesAllowed({"Estudante"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all/{username}")
    public Collection<CandidaturaDTO> getAllFromUserDTOs(@PathParam("username") String username)
            throws EntityDoesNotExistException, MyConstraintViolationException {

        try {
            Estudante estudante = em.find(Estudante.class, username);
            if (estudante == null) {
                throw new EntityDoesNotExistException("[all candidaturas do estudante]");
            }
            Collection<CandidaturaATrabalho> candidaturas = getAll();
            Collection<CandidaturaATrabalho> candidaturasDoEstudante = new LinkedList<>();

            for (CandidaturaATrabalho candidatura : candidaturas) {
                if (candidatura.getEstudante().equals(estudante)) {
                    candidaturasDoEstudante.add(candidatura);
                }
            }

            return toDTOs(candidaturasDoEstudante, CandidaturaDTO.class);
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @DELETE
    @RolesAllowed({"Estudante"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("delete/{candidaturaId}")
    public void remove(@PathParam("candidaturaId") Long candidaturaId) throws
            EntityDoesNotExistException, MyConstraintViolationException, PeriodoInvalido {
        try {
            CandidaturaATrabalho candidatura = em.find(CandidaturaATrabalho.class, candidaturaId);
            if (candidatura == null) {
                throw new EntityDoesNotExistException("[CandidaturaBean.remove]");
            }
            if (candidatura.getTrabalho().getProposta().getConcurso().getEstadoConcurso() != EstadoConcurso.PERIODO_CANDIDATURAS) {
                throw new PeriodoInvalido();
            }
            candidatura.getTrabalho().removeCandidaturas(candidatura);
            em.merge(candidatura.getTrabalho());

            candidatura.getEstudante().removeCandidaturasATrabalhos(candidatura);
            em.merge(candidatura.getEstudante());

            em.remove(candidatura);
        } catch (EntityDoesNotExistException | PeriodoInvalido e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @PUT
    @RolesAllowed({"Estudante"})
    @Path("update")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void update(CandidaturaDTO candidaturaDTO)
            throws EntityDoesNotExistException, MyConstraintViolationException, PeriodoInvalido {
        try {

            CandidaturaATrabalho candidatura = em.find(CandidaturaATrabalho.class, candidaturaDTO.getId());
            if (candidatura == null) {
                throw new EntityDoesNotExistException("Candidatura nao existe");
            }

            Trabalho trabalho = em.find(Trabalho.class, candidaturaDTO.getTrabalhoId());
            if (trabalho == null) {
                throw new EntityDoesNotExistException("Trabalho nao existe");
            }

            Estudante estudante = em.find(Estudante.class, candidaturaDTO.getEstudanteUsername());
            if (estudante == null) {
                throw new EntityDoesNotExistException("Estudante nao existe");
            }

            if (trabalho.getProposta().getConcurso().getEstadoConcurso() != EstadoConcurso.PERIODO_CANDIDATURAS) {
                throw new PeriodoInvalido();
            }

            List<Documento> documentos = new LinkedList<>();
            for (DocumentoDTO dto : candidaturaDTO.getDocumentos()) {
                Documento documento
                        = new Documento(
                                Documento.TipoDeDocumento.CANDIDATURA,
                                dto.getNome(),
                                dto.getPath()
                        );

                if (dto.getMimeType() != null) {
                    documento.setMimeType(dto.getMimeType());
                }
                documentos.add(documento);
            }
            //TODO VALE A PENA REMOVER DOCUMENTOS ANTIGOS?

            candidatura.setMedia(candidaturaDTO.getMedia());
            candidatura.setDocumentos(documentos);
            candidatura.setEstado(Estado.ANALISE);
            em.merge(candidatura);
        } catch (EntityDoesNotExistException | PeriodoInvalido e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Path("aceitar/{candidaturaId}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void aceitar(@PathParam("candidaturaId") Long candidaturaId) throws //MessagingException,
            EntityDoesNotExistException, PeriodoInvalido, MyConstraintViolationException,
            EstudanteJaTemTrabalhoException, TrabalhoJaTemEstudanteException, MessagingException {
        try {
            CandidaturaATrabalho candidatura = em.find(CandidaturaATrabalho.class, candidaturaId);
            if (candidatura == null) {
                throw new EntityDoesNotExistException("Candidatura nao existe");
            }

            Trabalho trabalho = candidatura.getTrabalho();
            if (trabalho.getEstudante() != null) {
                throw new TrabalhoJaTemEstudanteException();
            }

            Estudante estudante = candidatura.getEstudante();
            if (estudante.getTrabalho() != null) {
                throw new EstudanteJaTemTrabalhoException();
            }
            Concurso concurso = trabalho.getProposta().getConcurso();
            if (concurso.getEstadoConcurso() != EstadoConcurso.PERIODO_SERIACAO) {
                throw new PeriodoInvalido("A fase de seriacao de propostas não está activa");
            }

            //REJEITAR TODAS AS OUTRAS CANDIDATURAS DO ESTUDANTE E TRABALHO
            for (CandidaturaATrabalho c : trabalho.getCandidaturas()) {
                c.setEstado(Estado.REJEITADO);
                em.merge(c);
            }
            for (CandidaturaATrabalho c : estudante.getCandidaturasATrabalhos()) {
                c.setEstado(Estado.REJEITADO);
                em.merge(c);
            }
            candidatura.setEstado(Estado.ACEITE);
            trabalho.setDataEntrega(concurso.getPeriodoEntregas().getFim().getTime());
            trabalho.setEstudante(estudante);
            estudante.setTrabalho(trabalho);

            em.merge(candidatura);
            em.merge(trabalho);
            em.merge(estudante);

            Calendar cal = concurso.getPeriodoSeriacao().getFim();
            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
            fmt.setCalendar(cal);
            //EMAIL AO ESTUDANTE ACEITE
            emailBean.send(
                    estudante.getEmail(),
                    "Candidatura aceite",
                    "Ola " + estudante.getNome() + ", a sua candidatura ao trabalho "
                    + trabalho.getTitulo() + " foi aceite. A partir de "
                    + fmt.format(cal.getTime()) + " tera acesso na plataforma a pagina de desenvolvimento do trabalho.");
            //EMAIL AOS CANDIDATOS REJEITADOS
            for (CandidaturaATrabalho c : trabalho.getCandidaturas()) {
                if (c.getEstado() == Estado.REJEITADO) {
                    emailBean.send(
                            estudante.getEmail(),
                            "Candidatura rejeitada",
                            "Ola " + estudante.getNome() + ", a sua candidatura ao trabalho "
                            + trabalho.getTitulo() + " foi rejeitada. Para mais informacoes contacte os servicos academicos.");
                }
            }
            //EMAIL AOS PROPONENTES
            for (Proponente p : trabalho.getProposta().getProponentes()) {
                emailBean.send(
                        p.getEmail(),
                        "Atribuicao de estudante",
                        p.getNome() + ", o aluno "
                        + estudante.getNome() + " foi atribuido a sua proposta de trabalho "
                        + trabalho.getTitulo() + ". Pode contactar o aluno atraves do email "
                        + estudante.getEmail() + ". Para mais informacoes contacte os servicos academicos.");
            }
        } catch (EntityDoesNotExistException | MessagingException | PeriodoInvalido
                | EstudanteJaTemTrabalhoException | TrabalhoJaTemEstudanteException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    public void seriacaoAutomaticaCB() throws //MessagingException, 
            EntityDoesNotExistException, PeriodoInvalido, MyConstraintViolationException,
            EstudanteJaTemTrabalhoException, TrabalhoJaTemEstudanteException {
        this.seriacaoAutomatica();
    }
    
    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("seriar/all")
    public void seriacaoAutomatica() throws //MessagingException, 
            EntityDoesNotExistException, PeriodoInvalido, MyConstraintViolationException,
            EstudanteJaTemTrabalhoException, TrabalhoJaTemEstudanteException {
        try {
            Boolean didSomething;
            do {
                didSomething = false;
                for (CandidaturaATrabalho candidatura : getAllOrdenadoPorMediaEDataCriacao()) {
                    if (candidatura.getEstado() == Estado.ANALISE) {
                        aceitar(candidatura.getId());
                        didSomething = true;
                        break;
                    }
                }
            } while (didSomething);
            //MessagingException
        } catch (EntityDoesNotExistException | PeriodoInvalido
                | EstudanteJaTemTrabalhoException | TrabalhoJaTemEstudanteException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
}
