/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.EstudanteDTO;
import dtos.TrabalhoDTO;
import entities.roles.Estudante;
import entities.roles.User;
import exceptions.*;
import java.util.Collection;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.mail.MessagingException;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import utils.CreateUserUtil;

/**
 *
 * @author vsc
 */
@Stateless
@LocalBean
@Path(EstudanteBean.BASE_REST_PATH)
public class EstudanteBean extends Bean<Estudante> {

    public static final String BASE_REST_PATH = "/estudantes";

    public static final String REST_PATH_ALL = BASE_REST_PATH + "/all";

    public static final String REST_PATH_PUT_CREATE = BASE_REST_PATH + "/create";

    public static final String REST_PATH_GET_TRABALHO(String username) {
        return BASE_REST_PATH + "/trabalho/" + username;
    }

    public static final String REST_PATH_GET_FIND(String username) {
        return BASE_REST_PATH + "/find/" + username;
    }

    public static final String REST_PATH_PUT_UPDATE = BASE_REST_PATH + "/update";

    public static final String REST_PATH_DELETE(String username) {
        return BASE_REST_PATH + "/delete/" + username;
    }

    /*
    public static final String REST_PATH_GET_DOCUMENTO_DE_CANDIDATURA(
            String username, String candidaturaId, String documentoId) {
        return BASE_REST_PATH
                + "/" + username
                + "/candidatura/" + candidaturaId
                + "/documentos/" + documentoId;
    }
     */
    // ####################################################
    @EJB
    EmailBean emailBean;

    // ####################################################
    public EstudanteBean() {
    }

    // ####################################################
    public void create(
            String username, 
            String nome, 
            String email)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {

        String generatedPassword = CreateUserUtil.generateRandomPassword(12);
        this.create(username, generatedPassword, nome, email);

    }

    public void create(
            String username, 
            String password,
            String nome,
            String email)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {
        try {

            User user = em.find(User.class, username);
            if (user != null) {
                throw new EntityExistsException("O estudante já existe");
            }
            
            Estudante estudante
                    = new Estudante(username, password, nome, email);

            em.persist(estudante);

            CreateUserUtil.sendNotificationEmail(emailBean, nome, email, username, password);

        } catch (EntityExistsException | MessagingException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @Override
    public Collection<Estudante> getAll() {
        try {
            return em.createNamedQuery(Estudante.getAll, Estudante.class)
                    .getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @PUT
    @RolesAllowed({"MembroCCP"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("create")
    public void create(EstudanteDTO estudanteDTO)
            throws EntityExistsException, MyConstraintViolationException, MessagingException {
        System.out.println(estudanteDTO.getUsername());
        this.create(
                estudanteDTO.getUsername(),
                estudanteDTO.getNome(),
                estudanteDTO.getEmail());
    }

    @GET
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("all")
    public Collection<EstudanteDTO> getAllDTOs()
            throws EntityDoesNotExistException {
        try {

            return toDTOs(getAll(), EstudanteDTO.class);
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"Estudante"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("trabalho/{username}")
    public TrabalhoDTO getTrabalho(@PathParam("username") String username)
            throws EntityDoesNotExistException {
        try {
            Estudante estudante = em.find(Estudante.class, username);
            if (estudante == null) {
                throw new EntityDoesNotExistException("Não existe nenhum estudante com esse username");
            }

            if (estudante.getTrabalho() == null) {
                throw new EntityDoesNotExistException("O estudante não tem um trabalho");
            }

            return toDTO(estudante.getTrabalho(), TrabalhoDTO.class);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"Estudante"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("find/{username}")
    public EstudanteDTO findEstudante(@PathParam("username") String username)
            throws EntityDoesNotExistException {
        try {
            Estudante estudante = em.find(Estudante.class, username);
            if (estudante == null) {
                throw new EntityDoesNotExistException("Não existe nenhum estudante com esse username");
            }
            return toDTO(estudante, EstudanteDTO.class);
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @PUT
    @RolesAllowed({"MembroCCP"})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("update")
    public void update(EstudanteDTO estudanteDTO)
            throws
            EntityDoesNotExistException,
            MyConstraintViolationException {
        try {

            String username = estudanteDTO.getUsername();
            String password = estudanteDTO.getPassword();
            String nome = estudanteDTO.getNome();
            String email = estudanteDTO.getEmail();

            Estudante estudante = em.find(Estudante.class, username);

            if (estudante == null) {
                throw new EntityDoesNotExistException("O estudante não existe");
            }

            if (!estudante.getEmail().equals(email)) {
                estudante.setEmail(email);
            }

            if (!estudante.getNome().equals(nome)) {
                estudante.setNome(nome);
            }

            if (password != null && !password.isEmpty()) {
                estudante.setPassword(password);
            }

            em.merge(estudante);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @DELETE
    @RolesAllowed({"MembroCCP"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("delete/{username}")
    public void remove(@PathParam("username") String username) throws EntityDoesNotExistException, MyConstraintViolationException {

        try {
            Estudante estudante = em.find(Estudante.class, username);

            if (estudante == null) {
                throw new EntityDoesNotExistException("[EstudanteBean.remove]");
            }

            //TODO REMOVE CANDIDATURAS/TRABALHOS
            em.remove(estudante);

        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (ConstraintViolationException e) {
            throw new MyConstraintViolationException(Utils.getConstraintViolationMessages(e));
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

}
