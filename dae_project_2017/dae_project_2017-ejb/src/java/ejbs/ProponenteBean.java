/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import dtos.ProponenteDTO;
import dtos.PropostaDTO;
import entities.roles.Instituicao;
import entities.roles.Proponente;
import exceptions.EntityDoesNotExistException;
import java.util.Collection;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author vsc
 */
@Stateless
@Path(ProponenteBean.BASE_REST_PATH)
public class ProponenteBean extends Bean<Proponente> {

    public static final String BASE_REST_PATH = "/proponentes";
    
    public static final String REST_PATH_GET_FIND(String username){
        return BASE_REST_PATH + "/find/" + username;
    }
    
    public static final String REST_PATH_GET_PROPOSTAS(String username){
        return BASE_REST_PATH + "/propostas/" + username;
    }
    // ####################################################

    public ProponenteBean() {
    }
    
    // ####################################################
    
    @Override
    protected Collection<Proponente> getAll() {
        try {
            return em.createNamedQuery(Proponente.getAll, Proponente.class)
                    .getResultList();
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"Proponente"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("find/{username}")
    public ProponenteDTO findProponente(@PathParam("username") String username)
            throws EntityDoesNotExistException {
        try {
            Proponente proponente = em.find(Proponente.class, username);
            if (proponente == null) {
                throw new EntityDoesNotExistException("Não existe nenhum proponente com esse username");
            }
            return toDTO(proponente, ProponenteDTO.class);
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }

    @GET
    @RolesAllowed({"Proponente"})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Path("propostas/{username}")
    public Collection<PropostaDTO> getPropostas(@PathParam("username") String username)
            throws EntityDoesNotExistException {
        try {
            Proponente proponente = em.find(Proponente.class, username);
            if (proponente == null) {
                throw new EntityDoesNotExistException("There is no proponente with such username.");
            }
            return toDTOs(proponente.getPropostas(), PropostaDTO.class);
        } catch (EntityDoesNotExistException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
}
