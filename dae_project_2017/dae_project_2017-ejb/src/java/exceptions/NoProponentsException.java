/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author vsc
 */
public class NoProponentsException extends Exception {

    public NoProponentsException() {
        super("O proponente que indidou não é válido");
    }
    
}
