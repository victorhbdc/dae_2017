/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Victor
 */
public class TrabalhoJaTemEstudanteException extends Exception {

    public TrabalhoJaTemEstudanteException() {
        super("O trabalho ja esta atribuido a um estudante!");
    }

}
