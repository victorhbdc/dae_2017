/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Victor
 */
public class TrabalhoPorAtribuirProfessoresException extends Exception {

    /**
     * Creates a new instance of
     * <code>TrabalhoPorAtribuirProfessoresException</code> without detail
     * message.
     */
    public TrabalhoPorAtribuirProfessoresException() {
        super("falta atribuir professores orientadores ao trabalho!");
    }

}
