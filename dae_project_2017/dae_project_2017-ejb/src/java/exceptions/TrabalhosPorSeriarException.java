/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Victor
 */
public class TrabalhosPorSeriarException extends Exception {
    public TrabalhosPorSeriarException() {
        super("Ainda existem trabalhos sem professores e alunos com candidaturas abertas!");
    }
}
