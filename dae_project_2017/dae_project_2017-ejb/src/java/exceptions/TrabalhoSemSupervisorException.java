/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Victor
 */
public class TrabalhoSemSupervisorException extends Exception {

    public TrabalhoSemSupervisorException() {
        super("Um trabalho proposto por uma instituicao tem de ter um supervisor");
    }

}
