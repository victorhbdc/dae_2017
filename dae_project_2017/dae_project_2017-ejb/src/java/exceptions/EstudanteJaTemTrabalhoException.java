/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Victor
 */
public class EstudanteJaTemTrabalhoException extends Exception {

    public EstudanteJaTemTrabalhoException() {
        super("O estudante ja tem um trabalho!");
    }
}
