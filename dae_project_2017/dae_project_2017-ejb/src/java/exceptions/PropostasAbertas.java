package exceptions;

public class PropostasAbertas extends Exception {

    public PropostasAbertas() {
    }

    public PropostasAbertas(String msg) {
        super(msg);
    }
}
