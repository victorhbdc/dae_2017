/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author Victor
 */
public class PropostaAceiteException extends Exception {

    /**
     * Creates a new instance of <code>PropostaAceiteException</code> without
     * detail message.
     */
    public PropostaAceiteException() {
        super("Nao se podem apagar/atualizar propostas ja aceites");
    }

}
