/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author vsc
 */
public class PeriodoInvalido extends Exception{

    public PeriodoInvalido() {
    }

    public PeriodoInvalido(String message) {
        super(message);
    }
    
}
