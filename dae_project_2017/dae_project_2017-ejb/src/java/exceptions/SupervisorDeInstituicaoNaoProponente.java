package exceptions;

public class SupervisorDeInstituicaoNaoProponente extends Exception {

    public SupervisorDeInstituicaoNaoProponente() {
        super("O supervisor tem de pertencer a Instituicao que propose o trabalho");
    }
}
