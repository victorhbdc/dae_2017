/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author vsc
 */
public class WrongTypeException extends Exception{

    public WrongTypeException() {
    }

    public WrongTypeException(String message) {
        super(message);
    }
    
}
