/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.trabalhos;

import entities.roles.Proponente;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import utils.Estado;
import utils.embeddable.ItemBibliografico;
import utils.TipoTrabalho;
import utils.embeddable.Documento;
import utils.embeddable.Orcamento;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = Proposta.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = Proposta.getAll,
            query = "SELECT t FROM " + Proposta.TABLE_NAME + " t ORDER BY t.titulo"
    )
})
public class Proposta implements Serializable {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Proposta";

    @Transient
    public final static String ID_FIELD = "ID";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    // ***
    @NotNull(message = "Proposta tem de ter um mestrado")
    private String mestrado;

    @Id
    @Column(name = Proposta.ID_FIELD)
    @GeneratedValue(
            generator = Proposta.TABLE_NAME + "GEN_KEY",
            strategy = GenerationType.AUTO
    )
    private Long id;

    @NotNull(message = "Proposta tem de estar associada a um concurso")
    @ManyToOne
    private Concurso concurso;

    @NotNull(message = "Proposta tem de ter um tipo de trabalho")
    @Enumerated(EnumType.STRING)
    private TipoTrabalho tipoTrabalho;

    @NotNull(message = "Proposta tem de ter um titulo")
    @NotEmpty(message = "O titulo não pode estar vazio")
    @Column(unique = true)
    private String titulo;

    @NotNull
    @ManyToMany(mappedBy = "propostas")
    @Size(min = 1, message = "Tem de existir pelo menos um proponente")
    private List<Proponente> proponentes;

    @NotNull
    private List<String> areasCientificas;

    @NotNull(message = "Proposta tem de ter um resumo")
    @NotEmpty(message = "O Resumo não pode estar vazio")
    private String resumo;

    @NotNull
    private List<String> objectivos;

    @ElementCollection
    @CollectionTable(
            name = "BIBLIOGRAFIA_" + Proposta.TABLE_NAME,
            joinColumns
            = @JoinColumn(
                    name = Proposta.TABLE_NAME + "_" + Proposta.ID_FIELD,
                    nullable = false
            )
    )
    private List<ItemBibliografico> bibliografia;

    @NotNull(message = "Proposta tem de ter um plano de trabalho")
    private String planoTrabalho;

    private String localRealizacao;

    @NotNull
    private List<String> requisitosTrabalho;

    @NotNull
    @Valid
    @Embedded
    private Orcamento orcamento;

    @NotNull
    private List<String> apoios; //financeiros ou de outro tipo

    // campos preenchido apos validacao pela ccp
    private String observacoes;
    
    @NotNull(message = "Proposta tem de ter um estado")
    @Enumerated(EnumType.STRING)
    private Estado estado;
    
    @OneToOne(
            // todo
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "proposta"
    )
    private Trabalho trabalho;

    public Proposta() {
        this.proponentes = new LinkedList<>();
        this.areasCientificas = new LinkedList<>();
        this.objectivos = new LinkedList<>();
        this.bibliografia = new LinkedList<>();
        this.requisitosTrabalho = new LinkedList<>();
        this.apoios = new LinkedList<>();
        this.orcamento = new Orcamento();
    }

    public Proposta(String mestrado, Concurso concurso,
            TipoTrabalho tipoTrabalho, String titulo, String resumo,
            Orcamento orcamento, String planoTrabalho, List<String> areasCientificas, List<Proponente> proponentes) {
        this();
        this.mestrado = mestrado;
        this.concurso = concurso;
        this.tipoTrabalho = tipoTrabalho;
        this.titulo = titulo;
        this.resumo = resumo;
        this.orcamento = orcamento;
        this.estado = Estado.ANALISE;
        this.planoTrabalho = planoTrabalho;
        this.areasCientificas = areasCientificas;
        this.proponentes = proponentes;
    }

    public String getMestrado() {
        return mestrado;
    }

    public void setMestrado(String mestrado) {
        this.mestrado = mestrado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Concurso getConcurso() {
        return concurso;
    }

    public void setConcurso(Concurso concurso) {
        this.concurso = concurso;
    }

    public TipoTrabalho getTipoTrabalho() {
        return tipoTrabalho;
    }

    public void setTipoTrabalho(TipoTrabalho tipoTrabalho) {
        this.tipoTrabalho = tipoTrabalho;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Proponente> getProponentes() {
        return proponentes;
    }
    
    public List<String> getProponenteUsernames(){
        List<String> usernames = new LinkedList<>();
        for(Proponente proponente : this.proponentes){
            usernames.add(proponente.getUsername());
        }
        return usernames;
    }

    public void setProponentes(List<Proponente> proponentes) {
        this.proponentes = proponentes;
    }

    public boolean addProponentes(Proponente proponente) {
        return this.proponentes.add(proponente);

    }

    public boolean removeProponentes(Proponente proponente) {
        return this.proponentes.remove(proponente);
    }
    /*
     * CAMPOS OPCIONAIS
     */
    public Estado getEstado() {
        return estado;
    }
    
    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<String> getAreasCientificas() {
        return areasCientificas;
    }

    public void setAreasCientificas(List<String> areasCientificas) {
        this.areasCientificas = areasCientificas;
    }

    public boolean addAreasCientificas(String areasCientifica) {
        return this.areasCientificas.add(areasCientifica);
    }

    public boolean removeAreasCientificas(String areasCientifica) {
        return this.areasCientificas.remove(areasCientifica);
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public List<String> getObjectivos() {
        return objectivos;
    }

    public void setObjectivos(List<String> objectivos) {
        this.objectivos = objectivos;
    }

    public boolean addObjectivos(String o) {
        return this.objectivos.add(o);
    }

    public boolean removeObjectivos(String o) {
        return this.objectivos.remove(o);
    }

    public List<ItemBibliografico> getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(List<ItemBibliografico> bibliografia) {
        this.bibliografia = bibliografia;
    }

    public boolean addItemBibliografico(ItemBibliografico itemBibliografico) {
        return this.bibliografia.add(itemBibliografico);
    }

    public boolean removeItemBibliografico(ItemBibliografico itemBibliografico) {
        return this.bibliografia.remove(itemBibliografico);
    }

    public String getPlanoTrabalho() {
        return planoTrabalho;
    }

    public void setPlanoTrabalho(String planoTrabalho) {
        this.planoTrabalho = planoTrabalho;
    }

    public String getLocalRealizacao() {
        return localRealizacao;
    }

    public void setLocalRealizacao(String localRealizacao) {
        this.localRealizacao = localRealizacao;
    }

    public List<String> getRequisitosTrabalho() {
        return requisitosTrabalho;
    }

    public void setRequisitosTrabalho(List<String> requisitosTrabalho) {
        this.requisitosTrabalho = requisitosTrabalho;
    }

    public boolean addRequisitoTrabalho(String requisitoTrabalho) {
        return this.requisitosTrabalho.add(requisitoTrabalho);
    }

    public boolean removeRequisitoTrabalho(String requisitoTrabalho) {
        return this.requisitosTrabalho.remove(requisitoTrabalho);
    }

    public Orcamento getOrcamento() {
        return orcamento;
    }

    public void setOrcamento(Orcamento orcamento) {
        this.orcamento = orcamento;
    }

    public List<String> getApoios() {
        return apoios;
    }

    public void setApoios(List<String> apoios) {
        this.apoios = apoios;
    }

    public boolean addApoio(String apoio) {
        return this.apoios.add(apoio);
    }

    public boolean removeApoio(String apoio) {
        return this.apoios.remove(apoio);
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Trabalho getTrabalho() {
        return trabalho;
    }

    public void setTrabalho(Trabalho trabalho) {
        this.trabalho = trabalho;
    }
}
