/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.trabalhos;

import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import utils.embeddable.Documento;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = ProvaPublica.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = ProvaPublica.getAll,
            query = "SELECT t FROM " + ProvaPublica.TABLE_NAME + " t ORDER BY t.data"
    )
    ,
    @NamedQuery(
            name = ProvaPublica.getAllMarcadas,
            query = "SELECT t FROM " + ProvaPublica.TABLE_NAME + " t "
            + " WHERE t.estadoProvaPublica = entities.trabalhos.ProvaPublica.EstadoProvaPublica.MARCADA "
            + " ORDER BY t.data "
    )
})
public class ProvaPublica implements Serializable {

    public enum EstadoProvaPublica {
        MARCADA("Marcada"),
        CONCLUIDA("Concluída");

        private String estado;

        private EstadoProvaPublica(String estado) {
            this.estado = estado;
        }

        public String getEstado() {
            return estado;
        }
    }

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "ProvaPublica";

    @Transient
    public final static String ID_FIELD = "ID";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    @Transient
    public final static String getAllMarcadas = PREFIX + "getAllMarcadas";

    // ***
    @Id
    @Column(name = ProvaPublica.ID_FIELD)
    @GeneratedValue(
            generator = ProvaPublica.TABLE_NAME + "GEN_KEY",
            strategy = GenerationType.AUTO
    )
    private Long id;

    @NotNull(message = "A prova deve ter uma data")
    private Date data;

    @NotNull(message = "A prova deve ter um local")
    private String local;

    @NotNull(message = "A prova deve ter um juri")
    @OneToOne(
            // todo
            fetch = FetchType.EAGER,
            cascade = CascadeType.REMOVE,
            orphanRemoval = true,
            mappedBy = "provaPublica"
    )
    private Juri juri;

    @NotNull(message = "A prova publica tem de ter um trabalho")
    @OneToOne
    private Trabalho trabalho;

    @Valid
    @Embedded
    private Documento ata;

    @NotNull(message = "A prova publica tem de ter um estado")
    @Enumerated(EnumType.STRING)
    private EstadoProvaPublica estadoProvaPublica;

    public ProvaPublica() {
        this.estadoProvaPublica = EstadoProvaPublica.MARCADA;
    }

    public ProvaPublica(Juri juri, Trabalho trabalho, Date data, String local) {
        this();
        this.data = data;
        this.local = local;
        this.juri = juri;
        this.trabalho = trabalho;

        this.juri.setProvaPublica(this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Juri getJuri() {
        return juri;
    }

    public void setJuri(Juri juri) {
        this.juri = juri;
    }

    public Trabalho getTrabalho() {
        return trabalho;
    }

    public void setTrabalho(Trabalho trabalho) {
        this.trabalho = trabalho;
    }

    public Documento getAta() {
        return ata;
    }

    public void setAta(Documento ata) {
        this.ata = ata;
    }

    public EstadoProvaPublica getEstadoProvaPublica() {
        return estadoProvaPublica;
    }

    public void setEstadoProvaPublica(EstadoProvaPublica estadoProvaPublica) {
        this.estadoProvaPublica = estadoProvaPublica;
    }
}
