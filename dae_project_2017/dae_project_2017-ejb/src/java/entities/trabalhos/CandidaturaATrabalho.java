package entities.trabalhos;

import entities.roles.Estudante;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import utils.Estado;
import utils.embeddable.Documento;

@Entity
@Table(name = CandidaturaATrabalho.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = CandidaturaATrabalho.getAll,
            query = "SELECT t FROM " + CandidaturaATrabalho.TABLE_NAME + " t"
    ),
    @NamedQuery(
            name = CandidaturaATrabalho.getAllOrdenadoPorMediaDescDataAsc,
            query = "SELECT t FROM " + CandidaturaATrabalho.TABLE_NAME + " t ORDER BY t.media DESC, t.createdAt ASC"
    )
})
public class CandidaturaATrabalho implements Serializable {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "CandidaturaATrabalho";

    @Transient
    public final static String ID_FIELD = "ID";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";
    
    @Transient
    public final static String getAllOrdenadoPorMediaDescDataAsc = PREFIX + "getAllOrdenadoPorMediaDescDataAsc";

    @Id
    @Column(name = CandidaturaATrabalho.ID_FIELD)
    @GeneratedValue(
            generator = CandidaturaATrabalho.TABLE_NAME + "GEN_KEY",
            strategy = GenerationType.AUTO
    )
    private Long id;

    // ***
    @NotNull(message = "Candidatura a trabalho tem de ter um estudante")
    @ManyToOne
    @JoinColumn(
            name = Estudante.TABLE_NAME + "_" + Estudante.ID_FIELD,
            nullable = false
    )
    private Estudante estudante;

    @NotNull(message = "Candidatura tem de estar associada a um trabalho")
    @ManyToOne
    @JoinColumn(
            name = Trabalho.TABLE_NAME + "_" + Trabalho.ID_FIELD,
            nullable = false
    )
    private Trabalho trabalho;

    @ElementCollection
    @CollectionTable(
            name = "DOCUMENTOS_" + CandidaturaATrabalho.TABLE_NAME,
            joinColumns = @JoinColumn(
                    name = CandidaturaATrabalho.TABLE_NAME
                    + "_" + CandidaturaATrabalho.ID_FIELD,
                    nullable = false
            )
    )
    private List<Documento> documentos;

    @NotNull
    @DecimalMin("0.0")
    @DecimalMax("20.0")
    BigDecimal media;
    
    @NotNull(message = "Proposta tem de ter um estado")
    @Enumerated(EnumType.STRING)
    private Estado estado;
    
    @NotNull(message = "Proposta tem de ter uma data de criacao")
    @Temporal(TemporalType.DATE)
    private GregorianCalendar createdAt;
    
    public CandidaturaATrabalho() {
        this.documentos = new LinkedList<>();
        this.estado = Estado.ANALISE;
        this.createdAt = new GregorianCalendar();
    }

    public CandidaturaATrabalho(Estudante estudante, Trabalho trabalho, List<Documento> documentos, 
            BigDecimal media) {
        this.estudante = estudante;
        this.trabalho = trabalho;
        this.documentos = documentos;
        this.media = media;
        this.estado = Estado.ANALISE;
        this.createdAt = new GregorianCalendar();
    }

    public Estudante getEstudante() {
        return estudante;
    }

    public Trabalho getTrabalho() {
        return trabalho;
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public void setTrabalho(Trabalho trabalho) {
        this.trabalho = trabalho;
    }

    public boolean addDocumento(Documento documento) {
        return this.documentos.add(documento);

    }

    public boolean removeDocumento(Documento documento) {
        return this.documentos.remove(documento);
    }

    public BigDecimal getMedia() {
        return media;
    }

    public void setMedia(BigDecimal media) {
        this.media = media;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public GregorianCalendar getCreatedAt() {
        return createdAt;
    }
}
