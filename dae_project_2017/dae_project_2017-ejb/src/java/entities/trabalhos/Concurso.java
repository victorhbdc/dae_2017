/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.trabalhos;

import java.io.Serializable;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import utils.embeddable.Periodo;
import utils.embeddable.EstadoConcurso;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = Concurso.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = Concurso.getAll,
            query = "SELECT t FROM " + Concurso.TABLE_NAME + " t "
            + "ORDER BY t.dataAberturaDoConcurso"
    )
    ,
    @NamedQuery(
            name = Concurso.getAllEmPeriodoPropostas,
            query = "SELECT t FROM " + Concurso.TABLE_NAME + " t "
            + "WHERE t.estadoConcurso = utils.embeddable.EstadoConcurso.PERIODO_PROPOSTAS "
            + "ORDER BY t.dataAberturaDoConcurso "
    )
})
@XmlRootElement(name = "Concurso") // todo crear dto? 
@XmlAccessorType(XmlAccessType.FIELD)
public class Concurso implements Serializable {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Concurso";

    @Transient
    public final static String ID_FIELD = "ID";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    @Transient
    public final static String getAllEmPeriodoPropostas = PREFIX + "getAllEmPeriodoPropostas";

    // ***
    @Id
    @Column(name = Concurso.ID_FIELD)
    @GeneratedValue(
            generator = Concurso.TABLE_NAME + "GEN_KEY",
            strategy = GenerationType.AUTO
    )
    private Long id;

    @NotNull(message = "Concurso tem de ter um estado")
    @Enumerated(EnumType.STRING)
    private EstadoConcurso estadoConcurso;

    @NotNull
    @OneToMany(
            mappedBy = "concurso",
            cascade = CascadeType.REMOVE,
            fetch = FetchType.LAZY
    )
    private List<Proposta> propostas;

    @NotNull // TODO é mesmo necessária ou corresponde ao inicio do periodo de propostas?
    @Temporal(TemporalType.DATE)
    private GregorianCalendar dataAberturaDoConcurso;

    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(
                name = "inicio",
                column = @Column(name = "periodoPropostasInicio"))
        ,
        @AttributeOverride(
                name = "fim",
                column = @Column(name = "periodoPropostasFim"))
    })
    private Periodo periodoPropostas;

    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(
                name = "inicio",
                column = @Column(name = "periodoValidacaoInicio"))
        ,
        @AttributeOverride(
                name = "fim",
                column = @Column(name = "periodoValidacaoFim"))
    })
    private Periodo periodoValidacao;

    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(
                name = "inicio",
                column = @Column(name = "periodoCandidaturasInicio"))
        ,
        @AttributeOverride(
                name = "fim",
                column = @Column(name = "periodoCandidaturasFim"))
    })
    private Periodo periodoCandidaturas;

    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(
                name = "inicio",
                column = @Column(name = "periodoSeriacaoInicio"))
        ,
        @AttributeOverride(
                name = "fim",
                column = @Column(name = "periodoSeriacaoFim"))
    })
    private Periodo periodoSeriacao;

    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(
                name = "inicio",
                column = @Column(name = "periodoEntregasInicio"))
        ,
        @AttributeOverride(
                name = "fim",
                column = @Column(name = "periodoEntregasFim"))
    })
    private Periodo periodoEntregas;

    @NotNull
    @Valid
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(
                name = "inicio",
                column = @Column(name = "periodoMarcacaoProvasPublicasInicio"))
        ,
        @AttributeOverride(
                name = "fim",
                column = @Column(name = "periodoMarcacaoProvasPublicasFim"))
    })
    private Periodo periodoMarcacaoProvasPublicas;

    public Concurso() {
        this.propostas = new LinkedList<>();
        this.estadoConcurso = EstadoConcurso.ANUNCIADO;
    }

    public Concurso(
            GregorianCalendar dataAberturaDoConcurso,
            Periodo periodoPropostas,
            Periodo periodoValidacao,
            Periodo periodoCandidaturas,
            Periodo periodoSeriacao,
            Periodo periodoEntregas,
            Periodo periodoMarcacaoProvasPublicas) {
        this();
        this.dataAberturaDoConcurso = dataAberturaDoConcurso;

        this.periodoPropostas = periodoPropostas;
        this.periodoValidacao = periodoValidacao;
        this.periodoCandidaturas = periodoCandidaturas;
        this.periodoSeriacao = periodoSeriacao;
        this.periodoEntregas = periodoEntregas;
        this.periodoMarcacaoProvasPublicas = periodoMarcacaoProvasPublicas;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EstadoConcurso getEstadoConcurso() {
        return estadoConcurso;
    }

    public void setEstadoConcurso(EstadoConcurso estadoConcurso) {
        this.estadoConcurso = estadoConcurso;
    }

    public List<Proposta> getPropostas() {
        return propostas;
    }

    public boolean addProposta(Proposta proposta) {
        return this.propostas.add(proposta);
    }

    public boolean removeProposta(Proposta proposta) {
        return this.propostas.remove(proposta);
    }

    public GregorianCalendar getDataAberturaDoConcurso() {
        return dataAberturaDoConcurso;
    }

    public void setDataAberturaDoConcurso(GregorianCalendar dataAberturaDoConcurso) {
        this.dataAberturaDoConcurso = dataAberturaDoConcurso;
    }

    public Periodo getPeriodoPropostas() {
        return periodoPropostas;
    }

    public void setPeriodoPropostas(Periodo periodoPropostas) {
        this.periodoPropostas = periodoPropostas;
    }

    public Periodo getPeriodoValidacao() {
        return periodoValidacao;
    }

    public void setPeriodoValidacao(Periodo periodoValidacao) {
        this.periodoValidacao = periodoValidacao;
    }

    public Periodo getPeriodoCandidaturas() {
        return periodoCandidaturas;
    }

    public void setPeriodoCandidaturas(Periodo periodoCandidaturas) {
        this.periodoCandidaturas = periodoCandidaturas;
    }

    public Periodo getPeriodoSeriacao() {
        return periodoSeriacao;
    }

    public void setPeriodoSeriacao(Periodo periodoSeriacao) {
        this.periodoSeriacao = periodoSeriacao;
    }

    public Periodo getPeriodoEntregas() {
        return periodoEntregas;
    }

    public void setPeriodoEntregas(Periodo periodoEntregas) {
        this.periodoEntregas = periodoEntregas;
    }

    public Periodo getPeriodoMarcacaoProvasPublicas() {
        return periodoMarcacaoProvasPublicas;
    }

    public void setPeriodoMarcacaoProvasPublicas(Periodo periodoMarcacaoProvasPublicas) {
        this.periodoMarcacaoProvasPublicas = periodoMarcacaoProvasPublicas;
    }

}
