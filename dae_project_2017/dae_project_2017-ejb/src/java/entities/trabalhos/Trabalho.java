/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.trabalhos;

import entities.roles.Estudante;
import entities.roles.Professor;
import entities.roles.Supervisor;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import utils.EstadoTrabalho;
import utils.TipoProponente;
import utils.TipoTrabalho;
import utils.embeddable.Documento;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = Trabalho.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = Trabalho.getAll,
            query = "SELECT t FROM " + Trabalho.TABLE_NAME + " t ORDER BY t.titulo"
    ),
    @NamedQuery(
            name = Trabalho.getAllIniciados,
            query = "SELECT t FROM " + Trabalho.TABLE_NAME + " t "
                    + " WHERE t.estadoTrabalho = utils.EstadoTrabalho.INICIADO "
                    + " ORDER BY t.titulo"
    ),
    @NamedQuery(
            name = Trabalho.getAllEntregues,
            query = "SELECT t FROM " + Trabalho.TABLE_NAME + " t "
                    + " WHERE t.estadoTrabalho = utils.EstadoTrabalho.ENTREGUE "
                    + " ORDER BY t.titulo"
    ),
    @NamedQuery(
            name = Trabalho.getAllFinalizados,
            query = "SELECT t FROM " + Trabalho.TABLE_NAME + " t "
                    + " WHERE t.estadoTrabalho = utils.EstadoTrabalho.FINALIZADO "
                    + " ORDER BY t.titulo"
    )
        
})
public class Trabalho implements Serializable {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Trabalho";

    @Transient
    public final static String ID_FIELD = "ID";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    @Transient
    public final static String getAllIniciados = PREFIX + "getAllIniciados";
    
    @Transient
    public final static String getAllEntregues = PREFIX + "getAllEntregues";
    
    @Transient
    public final static String getAllFinalizados = PREFIX + "getAllFinalizados";
    
    // ***
    
    
    @Id
    @Column(name = Trabalho.ID_FIELD)
    @GeneratedValue(
            generator = Trabalho.TABLE_NAME + "GEN_KEY",
            strategy = GenerationType.AUTO
    )
    private Long id;
    
    @NotNull(message = "Trabalho tem de ter um titulo")
    private String titulo;
    
    @NotNull(message = "Trabalho tem de ter um resumo")
    private String resumo;

    @NotNull
    @OneToOne
    private Proposta proposta;
    
    @NotNull(message = "Trabalho tem de ter um estado de trabalho")
    @Enumerated(EnumType.STRING)
    private EstadoTrabalho estadoTrabalho;
    
    @NotNull(message = "Trabalho tem de ter um tipo de trabalho")
    @Enumerated(EnumType.STRING)
    private TipoTrabalho tipoTrabalho;
        
    @NotNull(message = "Trabalho tem de ter um tipoProponente")
    @Enumerated(EnumType.STRING)
    private TipoProponente tipoProponente;
    
    @NotNull
    @OneToMany(mappedBy = "trabalho", cascade = CascadeType.REMOVE)
    private List<CandidaturaATrabalho> candidaturas;

    @NotNull
    @ManyToMany(mappedBy = "trabalhos")
    private List<Professor> professores;  //ORIENTADORES, PROPONENTES NA PROPOSTA

    @ManyToOne
    @JoinColumn(
            name = Supervisor.TABLE_NAME + "_" + Supervisor.ID_FIELD
    )
    private Supervisor supervisor;
    
    @Temporal(TemporalType.DATE)
    private Date dataEntrega;

    @OneToOne
    private Estudante estudante;

    @ElementCollection
    @CollectionTable(
            name = "DOCUMENTOS_" + Trabalho.TABLE_NAME,
            joinColumns
            = @JoinColumn(
                    name = Trabalho.TABLE_NAME + "_" + Trabalho.ID_FIELD,
                    nullable = false
            )
    )
    private List<Documento> artefactos;

    @OneToOne(
            // todo
            fetch = FetchType.LAZY,
            cascade = CascadeType.REMOVE,
            orphanRemoval = true,
            mappedBy = "trabalho"
    )
    private ProvaPublica provaPublica;
    
    public Trabalho() {
        this.estadoTrabalho = EstadoTrabalho.NAOINICIADO;
        this.professores = new LinkedList<>();
        this.candidaturas = new LinkedList<>();
        this.artefactos = new LinkedList<>();
    }
    
    //TRABALHO PROPOSTO POR INSTITUICAO
    public Trabalho(Proposta proposta) {
        this();
        this.tipoTrabalho = proposta.getTipoTrabalho();
        this.titulo = proposta.getTitulo();
        this.resumo = proposta.getResumo();
        this.proposta = proposta;
        this.tipoProponente = TipoProponente.INSTITUICAO;
    }

    //TRABALHO PROPOSTO POR PROFESSORES
    public Trabalho(Proposta proposta,
                                List<Professor> professores){
        this();
        this.tipoTrabalho = proposta.getTipoTrabalho();
        this.titulo = proposta.getTitulo();
        this.resumo = proposta.getResumo();
        this.proposta = proposta;
        this.tipoProponente = TipoProponente.PROFESSORES;
        this.professores = professores;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public TipoTrabalho getTipoTrabalho() {
        return tipoTrabalho;
    }

    public void setTipoTrabalho(TipoTrabalho tipoTrabalho) {
        this.tipoTrabalho = tipoTrabalho;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public EstadoTrabalho getEstadoTrabalho() {
        return estadoTrabalho;
    }

    public void setEstadoTrabalho(EstadoTrabalho estadoTrabalho) {
        this.estadoTrabalho = estadoTrabalho;
    }

    public List<Professor> getProfessores() {
        return professores;
    }
    
    public List<String> getProfessorUsernames(){
        List<String> usernames = new LinkedList<>();
        for(Professor professor : this.professores){
            usernames.add(professor.getUsername());
        }
        return usernames;
    }

    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }

    public boolean addProfessor(Professor professor) {
        return this.professores.add(professor);
    }

    public boolean removeProfessor(Professor professor) {
        return this.professores.remove(professor);
    }

    public Supervisor getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Supervisor supervisor) {
        this.supervisor = supervisor;
    }

    public Date getDataEntrega() {
        return dataEntrega;
    }

    public void setDataEntrega(Date dataEntrega) {
        this.dataEntrega = dataEntrega;
    }

    public Estudante getEstudante() {
        return estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public ProvaPublica getProvaPublica() {
        return provaPublica;
    }

    public void setProvaPublica(ProvaPublica provaPublica) {
        this.provaPublica = provaPublica;
    }

    public Proposta getProposta() {
        return proposta;
    }
    
    public void setProposta(Proposta proposta) {
        this.proposta = proposta;
    }

    public List<CandidaturaATrabalho> getCandidaturas() {
        return candidaturas;
    }
    
    public List<Long> getCandidaturaIds() {
        List<Long> candidaturaIds = new LinkedList<>();
        for(CandidaturaATrabalho candidatura : this.candidaturas){
            candidaturaIds.add(candidatura.getId());
        }
        return candidaturaIds;
    }

    public void setCandidaturas(List<CandidaturaATrabalho> candidaturas) {
        this.candidaturas = candidaturas;
    }

    public boolean addCandidaturas(CandidaturaATrabalho candidatura) {
        return this.candidaturas.add(candidatura);
    }

    public boolean removeCandidaturas(CandidaturaATrabalho candidatura) {
        return this.candidaturas.remove(candidatura);
    }

    public List<Documento> getArtefactos() {
        return artefactos;
    }

    public void setArtefactos(List<Documento> artefactos) {
        this.artefactos = artefactos;
    }

    public boolean addArtefactos(Documento documento) {
        return this.artefactos.add(documento);
    }

    public boolean removeArtefactos(Documento documento) {
        return this.artefactos.remove(documento);
    }

    public TipoProponente getTipoProponente() {
        return tipoProponente;
    }

    public void setTipoProponente(TipoProponente tipoProponente) {
        this.tipoProponente = tipoProponente;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Trabalho other = (Trabalho) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
}
