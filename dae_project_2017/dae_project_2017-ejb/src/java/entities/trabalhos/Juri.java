/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.trabalhos;

import entities.roles.MembroComissaoCP;
import entities.roles.Professor;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = Juri.TABLE_NAME)
public class Juri implements Serializable {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Juri";

    @Transient
    public final static String ID_FIELD = "ID";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    // ***
    // todo ATENCAO: pode ser utilizada uma chave composta com todos os outros campos
    @Id
    @Column(name = Juri.ID_FIELD)
    @GeneratedValue(
            generator = Juri.TABLE_NAME + "GEN_KEY",
            strategy = GenerationType.AUTO
    )
    private Long id;

    @NotNull(message = "O professor tem de ter um nome")
    private String professorExternoNome;

    @NotNull(message = "O professor tem de ter um email")
    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            + "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            + "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+"
            + "[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "{email inválido}")
    private String professorExternoEmail;

    @NotNull(message = "O Juri tem de ter um professor orientador")
    @ManyToOne
    @JoinColumn(
            table = Juri.TABLE_NAME,
            name = Professor.TABLE_NAME + "_ORIENTADOR_" + Professor.ID_FIELD,
            nullable = false
    )
    private Professor orientador;

    @NotNull(message = "O Juri tem de ter um membro da Comissao C.P.")
    @ManyToOne
    @JoinColumn(
            table = Juri.TABLE_NAME,
            name = MembroComissaoCP.TABLE_NAME + "_" + MembroComissaoCP.ID_FIELD,
            nullable = false
    )
    private MembroComissaoCP membroCCP;

    @OneToOne
    private ProvaPublica provaPublica;

    public Juri() {
    }

    public Juri(MembroComissaoCP membroCCP, Professor orientador, String professorNome, String professorEmail) {
        //this.professor = professor;
        this.membroCCP = membroCCP;
        this.orientador = orientador;
        this.professorExternoNome = professorNome;
        this.professorExternoEmail = professorEmail;
    }

    
    public String getProfessorExternoNome() {
        return professorExternoNome;
    }

    public void setProfessorExternoNome(String professorExternoNome) {
        this.professorExternoNome = professorExternoNome;
    }

    public String getProfessorExternoEmail() {
        return professorExternoEmail;
    }

    public void setProfessorExternoEmail(String professorExternoEmail) {    
        this.professorExternoEmail = professorExternoEmail;
    }

    public Professor getOrientador() {
        return orientador;
    }

    public void setOrientador(Professor orientador) {
        this.orientador = orientador;
    }

    public MembroComissaoCP getMembroCCP() {
        return membroCCP;
    }

    public void setMembroCCP(MembroComissaoCP membroCCP) {
        this.membroCCP = membroCCP;
    }

    public ProvaPublica getProvaPublica() {
        return provaPublica;
    }

    public void setProvaPublica(ProvaPublica provaPublica) {
        this.provaPublica = provaPublica;
    }
}
