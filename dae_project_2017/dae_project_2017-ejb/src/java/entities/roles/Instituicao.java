/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.roles;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = Instituicao.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = Instituicao.getAll,
            query = "SELECT t FROM " + Instituicao.TABLE_NAME + " t ORDER BY t.nome"
    )
})
public class Instituicao extends Proponente {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Instituicao";

    @Transient
    public final static String ID_FIELD = Proponente.ID_FIELD;

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    //**
    @NotNull(message = "Instituicao tem de ter tipo")
    private String tipoInstituicao;

    @OneToMany(
            fetch = FetchType.EAGER,
            mappedBy = "instituicao",
            cascade = CascadeType.REMOVE
    )
    private List<Supervisor> supervisores;

    public Instituicao() {
        super();
        this.supervisores = new LinkedList<>();
    }

    public Instituicao(String username, String password, String nome, String email, String tipoDeInstituicao) {
        super(username, password, nome, email);
        this.supervisores = new LinkedList<>();
        this.tipoInstituicao = tipoDeInstituicao;
    }

    public String getTipoInstituicao() {
        return tipoInstituicao;
    }

    public void setTipoInstituicao(String tipoInstituicao) {
        this.tipoInstituicao = tipoInstituicao;
    }

    public List<Supervisor> getSupervisores() {
        return supervisores;
    }

    public void setSupervisores(List<Supervisor> supervisores) {
        this.supervisores = supervisores;
    }

    public void addSupervisor(Supervisor supervisor) {
        this.supervisores.add(supervisor);
    }

    public void removeSupervisor(Supervisor supervisor) {
        this.supervisores.remove(supervisor);
    }

    @Override
    public String toString() {
        return "Instituicao{nome=" + this.nome + ", tipoInstituicao=" + tipoInstituicao + '}';
    }
    
}
