/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.roles;

import entities.trabalhos.Juri;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = MembroComissaoCP.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = MembroComissaoCP.getAll,
            query = "SELECT t FROM " + MembroComissaoCP.TABLE_NAME + " t ORDER BY t.nome"
    )
})
public class MembroComissaoCP extends User {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "MembroComissaoCP";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    @NotNull
    @OneToMany(
            mappedBy = "membroCCP",
            fetch = FetchType.LAZY
    )
    protected List<Juri> juris;

    public MembroComissaoCP() {
        super();
        this.juris = new LinkedList<>();
    }

    public MembroComissaoCP(String username, String password, String nome, String email) {
        super(UserGroup.GROUP.MembroCCP, username, password, nome, email);
        this.juris = new LinkedList<>();
    }

    public List<Juri> getJuris() {
        return juris;
    }

    public void setJuris(List<Juri> juris) {
        this.juris = juris;
    }

    public void addJuri(Juri juri) {
        this.juris.add(juri);
    }

    public void removeJuri(Juri juri) {
        this.juris.add(juri);
    }
}
