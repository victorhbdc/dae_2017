/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.roles;

import ejbs.ConfigBean;
import entities.trabalhos.CandidaturaATrabalho;
import entities.trabalhos.Trabalho;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vsc
 */
@Entity
@Table(
        name = Estudante.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = Estudante.getAll,
            query = "SELECT t FROM " + Estudante.TABLE_NAME + " t ORDER BY t.nome"
    )
})
public class Estudante extends User {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Estudante";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    private String mestrado;

    @OneToOne(
            // todo
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "estudante"
    )
    private Trabalho trabalho;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "estudante",
            cascade = CascadeType.REMOVE
    )
    private List<CandidaturaATrabalho> candidaturasATrabalhos;

    public Estudante() {
        super();
        this.candidaturasATrabalhos = new LinkedList<>();
    }

    public Estudante(String username, String password, String nome,
            String email) {
        super(UserGroup.GROUP.Estudante, username, password, nome, email);
        this.candidaturasATrabalhos = new LinkedList<>();
        this.mestrado = ConfigBean.MESTRADO;
    }

    public String getMestrado() {
        return mestrado;
    }

    public void setMestrado(String mestrado) {
        this.mestrado = mestrado;
    }

    public Trabalho getTrabalho() {
        return trabalho;
    }

    public void setTrabalho(Trabalho trabalho) {
        this.trabalho = trabalho;
    }

    public List<CandidaturaATrabalho> getCandidaturasATrabalhos() {
        return candidaturasATrabalhos;
    }

    public void setCandidaturasATrabalhos(List<CandidaturaATrabalho> candidaturasATrabalhos) {
        this.candidaturasATrabalhos = candidaturasATrabalhos;
    }

    public boolean addCandidaturasATrabalhos(CandidaturaATrabalho candidaturaATrabalho) {
        return this.candidaturasATrabalhos.add(candidaturaATrabalho);
    }

    public boolean removeCandidaturasATrabalhos(CandidaturaATrabalho candidaturaATrabalho) {
        return this.candidaturasATrabalhos.remove(candidaturaATrabalho);
    }

    @Override
    public void reset() {
        super.reset();
        this.candidaturasATrabalhos = new LinkedList<>();
        this.setTrabalho(null);
        this.setMestrado(null);
    }

}
