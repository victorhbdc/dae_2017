/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.roles;

import entities.trabalhos.Juri;
import entities.trabalhos.Trabalho;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = Professor.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = Professor.getAll,
            query = "SELECT t FROM " + Professor.TABLE_NAME + " t ORDER BY t.nome"
    )
})
public class Professor extends Proponente {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Professor";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    @ManyToMany
    @JoinTable(name = Professor.TABLE_NAME + "_" + Trabalho.TABLE_NAME,
            joinColumns = @JoinColumn(
                    name = Professor.TABLE_NAME + "_" + Professor.ID_FIELD,
                    referencedColumnName = Professor.ID_FIELD,
                    nullable = false
            ),
            inverseJoinColumns = @JoinColumn(
                    name = Trabalho.TABLE_NAME + "_" + Trabalho.ID_FIELD,
                    referencedColumnName = Trabalho.ID_FIELD,
                    nullable = false
            )
    )
    private List<Trabalho> trabalhos;
    
    @NotNull
    @OneToMany(
            mappedBy = "orientador",
            fetch = FetchType.LAZY
    )
    protected List<Juri> juris;

    public Professor() {
        super();
        this.trabalhos = new LinkedList<>();
        this.juris = new LinkedList<>();
    }

    public Professor(String username, String password, String nome, String email) {
        super(username, password, nome, email);
        this.juris = new LinkedList<>();        
        this.trabalhos = new LinkedList<>();
    }

    public List<Juri> getJuris() {
        return juris;
    }

    public void setJuris(List<Juri> juris) {
        this.juris = juris;
    }

    public void addJuri(Juri juri) {
        this.juris.add(juri);
    }

    public void removeJuri(Juri juri) {
        this.juris.remove(juri);
    }
    
    public List<Trabalho> getTrabalhos() {
        return trabalhos;
    }

    public void setTrabalhos(List<Trabalho> trabalhos) {
        this.trabalhos = trabalhos;
    }

    public void addTrabalho(Trabalho t) {
        this.trabalhos.add(t);
    }

    public void removeTrabalho(Trabalho t) {
        this.trabalhos.remove(t);
    }
}
