/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.roles;

import entities.trabalhos.Trabalho;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author vsc
 */
@Entity
@Table(name = Supervisor.TABLE_NAME)
@NamedQueries({
    @NamedQuery(
            name = Supervisor.getAll,
            query = "SELECT t FROM " + Supervisor.TABLE_NAME + " t ORDER BY t.nome"
    )
})
public class Supervisor implements Serializable {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Supervisor";

    @Transient
    public final static String ID_FIELD = "EMAIL";

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    @Id
    @Column(name = Supervisor.ID_FIELD)
    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            + "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            + "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+"
            + "[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "{invalid.email}")
    private String email;

    @NotNull(message = "Nome nao pode estar vazio")
    private String nome;
    
    @NotNull(message = "Supervisor tem de pertencer a uma instituicao")
    @ManyToOne
    @JoinColumn(
            name = Instituicao.TABLE_NAME + "_" + Instituicao.ID_FIELD,
            nullable = false
    )
    private Instituicao instituicao;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "supervisor"
    )
    private List<Trabalho> trabalhos;
    
    public Supervisor() {
        this.trabalhos = new LinkedList<>();
    }

    public Supervisor(String nome, String email, Instituicao instituicao) {
        this();
        this.nome = nome;
        this.email = email;
        this.instituicao = instituicao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instituicao getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Trabalho> getTrabalhos() {
        return trabalhos;
    }

    public void setTrabalhos(List<Trabalho> trabalhos) {
        this.trabalhos = trabalhos;
    }
    
    public boolean addTrabalho(Trabalho trabalho){
        return this.trabalhos.add(trabalho);
    }
    
    public boolean removeTrabalho(Trabalho trabalho){
        return this.trabalhos.remove(trabalho);
    }
    
    
}
