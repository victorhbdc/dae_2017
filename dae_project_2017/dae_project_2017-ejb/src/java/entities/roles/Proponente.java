/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.roles;

import entities.trabalhos.Proposta;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

/**
 *
 * @author vsc
 */
@Entity
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Proponente extends User {

    // for named queryes
    @Transient
    public final static String TABLE_NAME = "Proponente";

    @Transient
    public final static String ID_FIELD = User.ID_FIELD;

    @Transient
    public final static String PREFIX = "entities." + TABLE_NAME + ".";

    @Transient
    public final static String getAll = PREFIX + "getAll";

    //***
    @ManyToMany
    @JoinTable(
            name = Proponente.TABLE_NAME + "_" + Proposta.TABLE_NAME,
            joinColumns = @JoinColumn(
                    name = Proponente.TABLE_NAME + "_" + Proponente.ID_FIELD,
                    referencedColumnName = Proponente.ID_FIELD,
                    nullable = false
            ),
            inverseJoinColumns = @JoinColumn(
                    name = Proposta.TABLE_NAME + "_" + Proposta.ID_FIELD,
                    referencedColumnName = Proposta.ID_FIELD,
                    nullable = false
            )
    )
    private List<Proposta> propostas;

    public Proponente() {
        super();
        this.propostas = new LinkedList<>();
    }

    public Proponente(String username, String password, String nome, String email) {
        super(UserGroup.GROUP.Proponente, username, password, nome, email);
        this.propostas = new LinkedList<>();
    }

    public List<Proposta> getPropostas() {
        return propostas;
    }

    public void setPropostas(List<Proposta> propostas) {
        this.propostas = propostas;
    }

    public void addProposta(Proposta proposta) {
        this.propostas.add(proposta);
    }

    public void removeProposta(Proposta proposta) {
        this.propostas.remove(proposta);
    }
}
