/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.ManageBeans;

import dtos.TrabalhoDTO;
import ejbs.TrabalhoBean;
import javax.faces.component.UIComponent;
import web.FacesExceptionHandler;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import util.URILookup;

@ManagedBean
@SessionScoped
public class PublicoManagedBean implements Serializable {

    private UIComponent component;

    private TrabalhoDTO currentTrabalho;
    private Client client; 
    
    private static final Logger logger = Logger.getLogger("web.Public");

    // ####################################################
    public PublicoManagedBean() {
        currentTrabalho = new TrabalhoDTO();
        client = ClientBuilder.newClient();
    }

    
    //#########################################################
//                                      TRABALHOS
    //########################################################

    public Collection<TrabalhoDTO> getAllTrabalhosFinalizados() {
        Response response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_GET_ALL_FINALIZADOS)
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os trabalhos",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<TrabalhoDTO>>() {});
    }
    
    
    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    public TrabalhoDTO getCurrentTrabalho() {
        return currentTrabalho;
    }

    public void setCurrentTrabalho(TrabalhoDTO currentTrabalho) {
        this.currentTrabalho = currentTrabalho;
    }

}
