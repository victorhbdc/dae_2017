package web.ManageBeans;

import dtos.ConcursoDTO;
import ejbs.ConcursoBean;
import entities.roles.UserGroup;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import util.URILookup;
import utils.embeddable.EstadoConcurso;
import web.FacesExceptionHandler;

@ManagedBean
@SessionScoped
public class LoginManagedBean implements Serializable {

    private String username;
    private String password;
    
    private static final Logger logger = Logger.getLogger("web.UserManagedBean");
    
    public LoginManagedBean() {
        
    }
    
    public String redirect() {
        if (isUserInRole(UserGroup.GROUP.Estudante)) {
            return "/roles/estudante/estudante_index?faces-redirect=true";
        }
        
        if (isUserInRole(UserGroup.GROUP.Proponente)) {
            return "/roles/proponente/proponente_index?faces-redirect=true";
        }
        
        if (isUserInRole(UserGroup.GROUP.MembroCCP)) {
            return "/roles/membro_ccp/membro_ccp_index?faces-redirect=true";
        }
        
        return "error403?faces-redirect=true";
    }

    public String login() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request
                = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.login(username, password);
        } catch (ServletException e) {
            logger.log(Level.WARNING, e.getMessage());
            return "error401?faces-redirect=true";
        }
        
        return redirect();
    }

    public boolean isUserInRole(UserGroup.GROUP group) {
        return isUserInRole(group.toString());
    }
    
    public boolean isUserInRole(String role) {
        return (isSomeUserAuthenticated() && FacesContext.getCurrentInstance().getExternalContext().isUserInRole(role));
    }

    public boolean isSomeUserAuthenticated() {
        return FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal() != null;
    }

    public String logout() {
        //https://issues.jboss.org/browse/WELD-2123
        //https://stackoverflow.com/questions/26822225/glassfish-4-1-bug-session-invalidate-nullpointer-exception
        FacesContext context = FacesContext.getCurrentInstance();
        // remove data from beans:
        for (String bean : context.getExternalContext().getSessionMap().keySet()) {
            context.getExternalContext().getSessionMap().remove(bean);
        }
        // destroy session:
       FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        // using faces-redirect to initiate a new request:
        return "/login.xhtml?faces-redirect=true";
    }

    public String clearLogin() {
        if (isSomeUserAuthenticated()) {
            logout();
        }
        return "login.xhtml?faces-redirect=true";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    // #######################################################################
    // CONCURSOS
    // #######################################################################

    public String getESTADO_CONCURSO_PERIODO_MARCACAO_PROVA_PUBLICA() {
        return EstadoConcurso.PERIODO_MARCACAO_PROVAS_PUBLICAS.getNomeTipo();
    }

    public String getESTADO_CONCURSO_PERIODO_ENTREGA_TRABALHOS() {
        return EstadoConcurso.PERIODO_ENTREGA_TRABALHOS.getNomeTipo();
    }

    public String getESTADO_CONCURSO_PERIODO_SERIACAO() {
        return EstadoConcurso.PERIODO_SERIACAO.getNomeTipo();
    }
    
    public String getESTADO_CONCURSO_PERIODO_CANDIDATURAS() {
        return EstadoConcurso.PERIODO_CANDIDATURAS.getNomeTipo();
    }
    
    public String getESTADO_CONCURSO_PERIODO_VALIDACAO() {
        return EstadoConcurso.PERIODO_VALIDACAO.getNomeTipo();
    }
    
    public String getESTADO_CONCURSO_PERIODO_PROPOSTAS() {
        return EstadoConcurso.PERIODO_PROPOSTAS.getNomeTipo();
    }
    
    public ConcursoDTO getLatestConcurso() {
        ConcursoDTO latestConcurso = null;
        
        Client client = ClientBuilder.newClient();
        Response response = client.target(URILookup.getBaseAPI())
                .path(ConcursoBean.REST_PATH_GET_LATEST_CONCURSO)
                .request(MediaType.APPLICATION_XML)
                .get();
        
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            logger.warning("Erro ao carregar o concurso");            
        }
        
        return response.readEntity(new GenericType<ConcursoDTO>(){});
    }
}
