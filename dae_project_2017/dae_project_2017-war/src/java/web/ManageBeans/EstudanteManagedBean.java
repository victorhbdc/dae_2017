/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.ManageBeans;

import dtos.CandidaturaDTO;
import dtos.DocumentoDTO;
import dtos.EstudanteDTO;
import dtos.TrabalhoDTO;
import ejbs.CandidaturaBean;
import ejbs.EstudanteBean;
import ejbs.TrabalhoBean;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;
import web.FacesExceptionHandler;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import util.URILookup;
import utils.embeddable.Documento;

@ManagedBean
@SessionScoped
public class EstudanteManagedBean extends UserManagedBean<EstudanteDTO> implements Serializable {

    @ManagedProperty(value = "#{uploadManager}")
    private UploadManager uploadManager;
    
    private Response response;
    private UIComponent component;

    private TrabalhoDTO currentTrabalho;
    private CandidaturaDTO currentCandidatura;
    private CandidaturaDTO newCandidatura;

    // #######################################################################
    public EstudanteManagedBean() {
        super(EstudanteDTO.class);
        currentTrabalho = new TrabalhoDTO();
        currentCandidatura = new CandidaturaDTO();
        newCandidatura = new CandidaturaDTO();
    }

    // #######################################################################
    @Override
    protected String getRestPathToFindUser(String username) {
        return EstudanteBean.REST_PATH_GET_FIND(username);
    }

    // ####################################################
    //                                      CANDIDATURAS
    // ####################################################
    public void adicionarDocumentoNovaCandidatura(){
        if(uploadManager.getFile().getSize() == 0){
            FacesExceptionHandler.handleException(
                "Nenhum documento especificado",
                component, logger);
            return;
         }
            
        DocumentoDTO documentoDTO
                = new DocumentoDTO(
                        uploadManager.getFilename(),
                        new Date(), 
                        uploadManager.getCompletePathFile(),
                        uploadManager.getFile().getContentType()
                );
        newCandidatura.addDocumento(documentoDTO);

        uploadManager.setFile(null);
    }
    
    public void adicionarDocumentoCurrentCandidatura(){
        if(uploadManager.getFile().getSize() == 0){
            FacesExceptionHandler.handleException(
                "Nenhum documento especificado",
                component, logger);
            return;
         }            

        DocumentoDTO documentoDTO
                = new DocumentoDTO(
                        uploadManager.getFilename(),
                        new Date(), 
                        uploadManager.getCompletePathFile(),
                        uploadManager.getFile().getContentType()
                );
        currentCandidatura.addDocumento(documentoDTO);
        uploadManager.setFile(null);
    }

    public String createCandidatura() {
        newCandidatura.setEstudanteUsername(loginManagedBean.getUsername());
        newCandidatura.setTrabalhoId(currentTrabalho.getId());
        newCandidatura.setTrabalhoTitulo(currentTrabalho.getTitulo());

        if(newCandidatura.getDocumentos().isEmpty()){
            FacesExceptionHandler.handleException(
                "Nenhum documento especificado",
                component, logger);
            return null;
        }

        response = client.target(URILookup.getBaseAPI())
                .path(CandidaturaBean.REST_PATH_CREATE)
                .request(MediaType.APPLICATION_XML)
                .post(Entity.xml(newCandidatura));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response, "Erro ao tentar criar a candidatura",
                    component, logger);
            return null;
        }

        newCandidatura.reset();
        uploadManager.setFile(null);
        return "estudante_index?faces-redirect=true";
    }

    public String updateCandidatura() {
        if(currentCandidatura.getDocumentos().isEmpty()){
            FacesExceptionHandler.handleException(
                "Nenhum documento especificado",
                component, logger);
            return null;
        }

        response = client.target(URILookup.getBaseAPI())
                .path(CandidaturaBean.REST_PATH_UPDATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentCandidatura));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response, "Erro ao tentar atualizar a candidatura",
                    component, logger);
            return null;
        }
        
        currentCandidatura.reset();
        uploadManager.setFile(null);
        return "estudante_index?faces-redirect=true";
    }

    public Collection<CandidaturaDTO> getAllCandidaturasDoEstudante() {

        response = client.target(URILookup.getBaseAPI())
                .path(CandidaturaBean.REST_PATH_GET_ALL(loginManagedBean.getUsername()))
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar as candidaturas",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<CandidaturaDTO>>() {});
    }

    public void removerCandidatura(ActionEvent event) {
        UIParameter param
                = (UIParameter) event.getComponent().findComponent("candidaturaID");
        String id = param.getValue().toString();

        response = client.target(URILookup.getBaseAPI())
                .path(CandidaturaBean.REST_PATH_DELETE(id))
                .request(MediaType.APPLICATION_XML)
                .delete();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao remover a candidatura",
                    component, logger);
        }
    }
    // #######################################################################
    // TRABALHOS
    // #######################################################################

    public Collection<TrabalhoDTO> getAllTrabalhosEmPeriodoCandidatura() {
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_GET_ALL_FASE_CANDIDATURA)
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os trabalhos",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<TrabalhoDTO>>() {});
    }
    
    public TrabalhoDTO getTrabalho(){
        response = client.target(URILookup.getBaseAPI())
                .path(EstudanteBean.REST_PATH_GET_TRABALHO(loginManagedBean.getUsername()))
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar o trabalho",
                    component, logger
            );
        }
        return response.readEntity(TrabalhoDTO.class);
    }
    
    public void adicionarArtefactoCurrentTrabalho(int tipoDeDocumento){
        if(uploadManager.getFile().getSize() == 0){
               FacesExceptionHandler.handleException(
                    "Nenhum documento especificado",
                    component, logger);
            return;
        }
            
        DocumentoDTO documentoDTO
                = new DocumentoDTO(
                        uploadManager.getFilename(),
                        new Date(), 
                        uploadManager.getCompletePathFile(),
                        uploadManager.getFile().getContentType()
                );
        System.err.println(tipoDeDocumento);
        if(tipoDeDocumento == 0){
            documentoDTO.setTipoDeDocumento(Documento.TipoDeDocumento.RELATORIO);
        } else {
            documentoDTO.setTipoDeDocumento(Documento.TipoDeDocumento.ARTEFACTO);
        }
        currentTrabalho.addArtefacto(documentoDTO);        
        uploadManager.setFile(null);
    }
    
    public String updateArtefactosTrabalho(){
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_UPDATE_ARTEFACTOS)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentTrabalho));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response, "Erro ao tentar atualizar os artefactos de trabalho",
                    component, logger);
            return null;
        }
        
        currentTrabalho.reset();
        uploadManager.setFile(null);
        return "estudante_index?faces-redirect=true";
    }

    public boolean temCandidaturaATrabalho(Long trabalhoId){
        for(CandidaturaDTO c:this.getAllCandidaturasDoEstudante()){
            if(c.getTrabalhoId().equals(trabalhoId)) return  true;
        }
        
        return false;
    }
    
    // #######################################################################
    // Getter & Setters
    // #######################################################################
    public CandidaturaDTO getNewCandidatura() {
        return newCandidatura;
    }

    public void setNewCandidatura(CandidaturaDTO newCandidatura) {
        this.newCandidatura = newCandidatura;
    }

    public UploadManager getUploadManager() {
        return uploadManager;
    }

    public void setUploadManager(UploadManager uploadManager) {
        this.uploadManager = uploadManager;
    }

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    public TrabalhoDTO getCurrentTrabalho() {
        return currentTrabalho;
    }

    public void setCurrentTrabalho(TrabalhoDTO currentTrabalho) {
        this.currentTrabalho = currentTrabalho;
    }

    public CandidaturaDTO getCurrentCandidatura() {
        return currentCandidatura;
    }

    public void setCurrentCandidatura(CandidaturaDTO currentCandidatura) {
        this.currentCandidatura = currentCandidatura;
    }

   
}
