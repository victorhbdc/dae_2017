/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.ManageBeans;

import dtos.UserDTO;
import java.util.logging.Logger;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import util.URILookup;


public abstract class UserManagedBean<DTO extends UserDTO> implements Serializable {

    protected Class<DTO> dtoClass;
    protected DTO loggedUserDTO;
    protected Logger logger;
    protected Client client;
    protected HttpAuthenticationFeature feature;

    @ManagedProperty(value = "#{loginManagedBean}")
    protected LoginManagedBean loginManagedBean;

    // ####################################################
    public UserManagedBean(Class<DTO> dtoClass) {
        client = ClientBuilder.newClient();
        this.dtoClass = dtoClass;
    }

    // ####################################################
    protected abstract String getRestPathToFindUser(String username);

    // ####################################################
    @PostConstruct
    public void initClient() {
        feature = HttpAuthenticationFeature
                .basic(
                        loginManagedBean.getUsername(), 
                        loginManagedBean.getPassword()
                );
        client.register(feature);
        getLoggedUser();
    }

    // ####################################################
    protected void getLoggedUser() {
        try {

            loggedUserDTO
                    = client.target(URILookup.getBaseAPI())
                            .path(this.getRestPathToFindUser(loginManagedBean.getUsername()))
                            .request(MediaType.APPLICATION_XML)
                            .get(this.dtoClass);

            //= proponenteBean.findProponente(loginManagedBean.getUsername());
        } catch (Exception e) {
            throw e;
            //FacesExceptionHandler.handleException(e, "Erro inesperado. Tente novamente mais tarde.", logger);
        }
    }
    
     public LoginManagedBean getLoginManagedBean() {
        return loginManagedBean;
    }

    public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
        this.loginManagedBean = loginManagedBean;
    }
}
