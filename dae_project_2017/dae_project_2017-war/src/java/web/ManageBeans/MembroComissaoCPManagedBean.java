/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.ManageBeans;

import dtos.CandidaturaDTO;
import dtos.DocumentoDTO;
import dtos.EstudanteDTO;
import dtos.InstituicaoDTO;
import dtos.MembroComissaoCPDTO;
import dtos.ProfessorDTO;
import dtos.PropostaDTO;
import dtos.ProvaPublicaDTO;
import dtos.SupervisorDTO;
import dtos.TrabalhoDTO;
import ejbs.CandidaturaBean;
import ejbs.ConcursoBean;
import ejbs.EstudanteBean;
import ejbs.InstituicaoBean;
import ejbs.MembroComissaoCPBean;
import ejbs.ProfessorBean;
import ejbs.PropostaBean;
import ejbs.ProvaPublicaBean;
import ejbs.SupervisorBean;
import ejbs.TrabalhoBean;
import exceptions.MyConstraintViolationException;
import exceptions.NoDocumentException;
import exceptions.Utils;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import web.FacesExceptionHandler;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import util.URILookup;
import utils.TipoProponente;

@ManagedBean
@SessionScoped
public class MembroComissaoCPManagedBean
        extends UserManagedBean<MembroComissaoCPDTO>
        implements Serializable {

    @ManagedProperty(value = "#{uploadManager}")
    private UploadManager uploadManager;

    private UIComponent component;
    private Response response;
    private CandidaturaDTO currentCandidatura;
    private TrabalhoDTO currentTrabalho;
    private PropostaDTO currentProposta;

    private EstudanteDTO currentEstudante;
    private ProfessorDTO currentProfessor;
    private InstituicaoDTO currentInstituicao;
    private ProvaPublicaDTO currentProvaPublica;

    private EstudanteDTO newEstudante;
    private ProfessorDTO newProfessor;
    private InstituicaoDTO newInstituicao;
    private ProvaPublicaDTO newProvaPublica;

    // #######################################################################
    public MembroComissaoCPManagedBean() {
        super(MembroComissaoCPDTO.class);
        this.logger = Logger.getLogger("web.MembroMCCPManagedBean");
        currentCandidatura = new CandidaturaDTO();
        currentTrabalho = new TrabalhoDTO();
        currentProposta = new PropostaDTO();

        currentEstudante = new EstudanteDTO();
        currentProfessor = new ProfessorDTO();
        currentInstituicao = new InstituicaoDTO();

        newEstudante = new EstudanteDTO();
        newProfessor = new ProfessorDTO();
        newInstituicao = new InstituicaoDTO();
        newProvaPublica = new ProvaPublicaDTO();
    }

    // #######################################################################
    @Override
    protected String getRestPathToFindUser(String username) {
        return MembroComissaoCPBean.REST_PATH_GET_FIND(username);
    }
    
    /*###################################################################
                                                      Membros Comissao CP
    ###################################################################*/
    //ASYNC
//https://www.programcreek.com/java-api-examples/index.php?api=javax.ws.rs.client.InvocationCallback
    
    public Collection<MembroComissaoCPDTO> getAllMembrosCCP(){
        response = client.target(URILookup.getBaseAPI())
                .path(MembroComissaoCPBean.REST_PATH_GET_ALL)
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os membros da comissão CP",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<MembroComissaoCPDTO>>() {});
    }

    public String adicionarAta() {
        if (uploadManager.getFile().getSize() == 0) {
            FacesExceptionHandler.handleException(
                "Nenhuma ata especificado",
                component, logger);
            return null;
        }

        DocumentoDTO ata
                = new DocumentoDTO(
                        uploadManager.getFilename(),
                        new Date(),
                        uploadManager.getCompletePathFile(),
                        uploadManager.getFile().getContentType()
                );

        response = client.target(URILookup.getBaseAPI())
                .path(ProvaPublicaBean.REST_PATH_PUT_SET_ATA(String.valueOf(currentProvaPublica.getId())))
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(ata));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response, "Erro ao tentar fazer upload da ata",
                    component, logger);
            return null;

        }

        uploadManager.setFile(null);
        return "membro_ccp_index?faces-redirect=true";
    }

    // #######################################################################
    // CONCURSOS
    // #######################################################################
    public String avancarConcursoMaisRecente(Long concursoID) {
        response = client.target(URILookup.getBaseAPI())
                    .path(ConcursoBean.REST_PATH_AVANCAR_CONCURSO(concursoID))
                    .request(MediaType.APPLICATION_XML)
                    .get();
        
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao avancar o concurso. ",
                    component, logger
            );
        }
        return "membro_ccp_index?faces-redirect=true";
    }
    
    public String atrasarConcursoMaisRecente(Long concursoID){
        response = client.target(URILookup.getBaseAPI())
                 .path(ConcursoBean.REST_PATH_ATRASAR_CONCURSO(concursoID))
                 .request(MediaType.APPLICATION_XML)
                 .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao atrasar o concurso. ",
                    component, logger
            );
        }
        return "membro_ccp_index?faces-redirect=true";
    }
    /*###################################################################
                                                                TRABALHOS
    ###################################################################*/
    
    public Collection<TrabalhoDTO> getAllTrabalhosPorSeriar(){
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_GET_ALL_POR_SERIAR)
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os trabalhos",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<TrabalhoDTO>>(){});
    }
    
    public Collection<TrabalhoDTO> getAllTrabalhosSeriados(){
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_GET_ALL_SERIADOS)
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os trabalhos",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<TrabalhoDTO>>() {});
    }

    public String updateTrabalhoOrientadores() {
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_UPDATE_ORIENTADORES)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentTrabalho));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao tentar atribuir a professores!",
                    component, logger);
            return null;
        }
        currentTrabalho.reset();
        return "membro_ccp_index?faces-redirect=true";
    }
    
    public Collection<SupervisorDTO> getAllSupervisorsOfInstituicao(){
        if(currentTrabalho.getTipoProponente() != TipoProponente.INSTITUICAO){
            return null;
        }
        String instituicaoUsername =  currentTrabalho.getProposta().getProponenteUsernames().get(0);
       
        response = client.target(URILookup.getBaseAPI())
                .path(SupervisorBean.REST_PATH_GET_ALL_DE_INSTITUICAO(instituicaoUsername))
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os supervisores da instituicao",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<SupervisorDTO>>() {});
    } 
    
    public Collection<TrabalhoDTO> getAllTrabalhosIniciados(){
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_GET_ALL_INICIADOS)
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os trabalhos",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<TrabalhoDTO>>() {});
    }
    
    public Collection<TrabalhoDTO> getAllTrabalhosEntregues(){
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_GET_ALL_ENTREGUES)
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os trabalhos",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<TrabalhoDTO>>() {});
    }

    public Collection<TrabalhoDTO> getAllTrabalhosFinalizados() {
        response = client.target(URILookup.getBaseAPI())
                    .path(TrabalhoBean.REST_PATH_GET_ALL_FINALIZADOS)
                    .request(MediaType.APPLICATION_XML)
                    .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os trabalhos finalizados",
                    component, logger
            );
        }
        
        return response.readEntity(new GenericType<List<TrabalhoDTO>>() {});
    }

    public Collection<ProfessorDTO> getOrientadoresDoTrabalhoCurrente() {
        response = client.target(URILookup.getBaseAPI())
                    .path(TrabalhoBean.REST_PATH_GET_ORIENTADORES(String.valueOf(currentTrabalho.getId())))
                    .request(MediaType.APPLICATION_XML)
                    .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os trabalhos",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<ProfessorDTO>>() {});
    }

    // #######################################################################
    // CANDIDATURAS
    // #######################################################################
    public Collection<CandidaturaDTO> getAllCandidaturasDoTrabalho() {
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_GET_CANDIDATURAS(currentTrabalho.getId().toString()))
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar as candidaturas",
                    component, logger
            );

        }
        return response.readEntity(new GenericType<List<CandidaturaDTO>>() {});
    }

    public void aceitarCandidatura(ActionEvent event) {
        UIParameter param 
                = (UIParameter) event.getComponent().findComponent("candidaturaID");
        String id = param.getValue().toString();

        response = client.target(URILookup.getBaseAPI())
                .path(CandidaturaBean.REST_PATH_ACEITAR(id))
                .request(MediaType.APPLICATION_XML)
                .get();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao tentar aceitar a candidatura!",
                    component, logger);
        }
    }
    
    public String seriacaoAutomatica(){
        response = client.target(URILookup.getBaseAPI())
                .path(CandidaturaBean.REST_PATH_SERIAR_AUTO)
                .request(MediaType.APPLICATION_XML)
                .get();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao processar a seriacao automatica!",
                    component, logger);
            return null;
        }
        return "membro_ccp_index?faces-redirect=true";
    }

    // #######################################################################
    // PROPOSTAS
    // #######################################################################
    public Collection<PropostaDTO> getAllPropostas() {

        response = client.target(URILookup.getBaseAPI())
                    .path(PropostaBean.REST_PATH_ALL)
                    .request(MediaType.APPLICATION_XML)
                    .get();
        
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response, "Erro ao carregar as propostas!",
                    component, logger);
            return null;
        } else {
            return response.readEntity(new GenericType<List<PropostaDTO>>() {});
        }
    }

    public String aceitarProposta() {
        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_CREATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentProposta));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao tentar aceitar a proposta!",
                    component, logger);
            return null;
        }
        currentProposta.reset();
        return "membro_ccp_index?faces-redirect=true";
    }

    public String rejeitarProposta() {
        response = client.target(URILookup.getBaseAPI())
                .path(PropostaBean.REST_PATH_REJEITAR)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentProposta));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao tentar rejeitar a proposta!",
                    component, logger);
            return null;
        }
        currentProposta.reset();
        return "membro_ccp_index?faces-redirect=true";
    }

    // #######################################################################
    // ESTUDANTES
    // #######################################################################
    public String createEstudante() {
        response = client.target(URILookup.getBaseAPI())
                .path(EstudanteBean.REST_PATH_PUT_CREATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(newEstudante));

        if(response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL){
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao criar estudante!",
                    component, logger
            );
            return null;
        } 
        
        newEstudante.reset();
        return "membro_ccp_index?faces-redirect=true";
    }

    public Collection<EstudanteDTO> getAllEstudantes() {
        response = client.target(URILookup.getBaseAPI())
                    .path(EstudanteBean.REST_PATH_ALL)
                    .request(MediaType.APPLICATION_XML)
                    .get();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os estudantes!",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<EstudanteDTO>>() {});
    }

    public String updateEstudante() throws MyConstraintViolationException {

        response = client.target(URILookup.getBaseAPI())
                .path(EstudanteBean.REST_PATH_PUT_UPDATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentEstudante));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao actualizar o estudante",
                    component, logger);
            return null;
        }

        currentEstudante.reset();
        return "membro_ccp_index?faces-redirect=true";
    }

    public void removeEstudante(ActionEvent event) {

        UIParameter param
                = (UIParameter) event.getComponent().findComponent("estudanteUsername");
        String username = param.getValue().toString();

        response = client.target(URILookup.getBaseAPI())
                .path(EstudanteBean.REST_PATH_DELETE(username))
                .request(MediaType.APPLICATION_XML)
                .delete();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao remover o estudante",
                    component, logger);
        }

    }

    // #######################################################################
    // PROFESSORES
    // #######################################################################
    public String createProfessor() {
        response = client.target(URILookup.getBaseAPI())
                .path(ProfessorBean.REST_PATH_PUT_CREATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(newProfessor));


        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao criar professor!",
                    component, logger
            );
            return null;
        }

        newProfessor.reset();
        return "membro_ccp_index?faces-redirect=true";
    }

    public Collection<ProfessorDTO> getAllProfessores() {
        response = client.target(URILookup.getBaseAPI())
                .path(ProfessorBean.REST_PATH_GET_ALL)
                .request(MediaType.APPLICATION_XML)
                .get();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar todos os professores!",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<ProfessorDTO>>() {});
    }

    public String updateProfessor() throws MyConstraintViolationException {
        response = client.target(URILookup.getBaseAPI())
                .path(ProfessorBean.REST_PATH_PUT_UPDATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentProfessor));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao actualizar o professor",
                    component, logger);
            return null;
        }

        currentProfessor.reset();
        return "membro_ccp_index?faces-redirect=true";
    }

    public void removeProfessor(ActionEvent event) {

        UIParameter param = (UIParameter) event.getComponent().findComponent("professorUsername");
        String username = param.getValue().toString();

        response = client.target(URILookup.getBaseAPI())
                .path(ProfessorBean.REST_PATH_DELETE(username))
                .request(MediaType.APPLICATION_XML)
                .delete();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao tentar apagar o professor! ",
                    component, logger);
        }
    }

    // #######################################################################
    // INSTITUICOES
    // #######################################################################
    public String createInstituicao() {
        response = client.target(URILookup.getBaseAPI())
                .path(InstituicaoBean.REST_PATH_PUT_CREATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(newInstituicao));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao criar instituicao!",
                    component, logger
            );
            return null;
        }

        newInstituicao.reset();
        return "membro_ccp_index?faces-redirect=true";
    }

    public Collection<InstituicaoDTO> getAllInstituicoes() {
        response = client.target(URILookup.getBaseAPI())
                .path(InstituicaoBean.REST_PATH_GET_ALL)
                .request(MediaType.APPLICATION_XML)
                .get();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro inesperado ao carregar as instituições!",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<InstituicaoDTO>>() {});
    }

    public String updateInstituicao() {
        response = client.target(URILookup.getBaseAPI())
                .path(InstituicaoBean.REST_PATH_PUT_UPDATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentInstituicao));
        
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler
                    .handleException(
                            response,
                            "Erro ao actualizar a instituição",
                            component, logger);
            return null;
        }
        currentInstituicao.reset();
        return "membro_ccp_index?faces-redirect=true";
    }

    public void removeInstituicao(ActionEvent event) {
        UIParameter param = (UIParameter) event
                .getComponent().findComponent("instituicaoUsername");

        String id = param.getValue().toString();

        response = client.target(URILookup.getBaseAPI())
                .path(InstituicaoBean.REST_PATH_DELETE(id))
                .request(MediaType.APPLICATION_XML)
                .delete();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao remover a instituição",
                    component, logger);
        }
    }

    // #######################################################################
    // PROVA PUBLICA
    // #######################################################################
    public String createProvaPublica() {
        newProvaPublica.setTrabalhoId(currentTrabalho.getId());

        response = client.target(URILookup.getBaseAPI())
                .path(ProvaPublicaBean.REST_PATH_PUT_CREATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(newProvaPublica));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao criar prova pública!",
                    component, logger
            );
            return null;
        }

        newProvaPublica.reset();
        return "membro_ccp_index?faces-redirect=true";
    }
    
    public ProvaPublicaDTO findProvaPublica(Long provaPublicaId){
        response = client.target(URILookup.getBaseAPI())
                .path(ProvaPublicaBean.REST_PATH_GET_PROVA_PUBLICA(String.valueOf(provaPublicaId)))
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar a prova pública marcada",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<ProvaPublicaDTO>() {});
    }
    
    public void removeProvaPublica(ActionEvent event) { 
        UIParameter param = (UIParameter) event.getComponent().findComponent("trabalhos_entregues_provaPublicaId");
        String id = param.getValue().toString();

        response = client.target(URILookup.getBaseAPI())
                .path(ProvaPublicaBean.REST_PATH_DELETE(id))
                .request(MediaType.APPLICATION_XML)
                .delete();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao tentar apagar o prova pública! ",
                    component, logger);
        }
    }

    public String removerAta(Long provaPublicaId) {

        response = client.target(URILookup.getBaseAPI())
                    .path(ProvaPublicaBean.REST_PATH_DELETE_ATA(String.valueOf(provaPublicaId)))
                    .request(MediaType.APPLICATION_XML)
                    .delete();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao tentar apagar o prova pública! ",
                    component, logger);
            return null;
        }

        return "membro_ccp_index?faces-redirect=true";
    }

    public UploadManager getUploadManager() {
        return uploadManager;
    }

    // #######################################################################
    // GETTERS & SETTERS
    // #######################################################################
    public void setUploadManager(UploadManager uploadManager) {
        this.uploadManager = uploadManager;
    }

    public InstituicaoDTO getNewInstituicao() {
        return newInstituicao;
    }

    public void setNewInstituicao(InstituicaoDTO newInstituicao) {
        this.newInstituicao = newInstituicao;
    }

    public ProfessorDTO getNewProfessor() {
        return newProfessor;
    }

    public void setNewProfessor(ProfessorDTO newProfessor) {
        this.newProfessor = newProfessor;
    }

    public EstudanteDTO getNewEstudante() {
        return newEstudante;
    }

    public void setNewEstudante(EstudanteDTO newEstudante) {
        this.newEstudante = newEstudante;
    }

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    public PropostaDTO getCurrentProposta() {
        return currentProposta;
    }

    public void setCurrentProposta(PropostaDTO currentProposta) {
        this.currentProposta = currentProposta;
    }

    public EstudanteDTO getCurrentEstudante() {
        return currentEstudante;
    }

    public void setCurrentEstudante(EstudanteDTO currentEstudante) {
        this.currentEstudante = currentEstudante;
    }

    public ProfessorDTO getCurrentProfessor() {
        return currentProfessor;
    }

    public void setCurrentProfessor(ProfessorDTO currentProfessor) {
        this.currentProfessor = currentProfessor;
    }

    public InstituicaoDTO getCurrentInstituicao() {
        return currentInstituicao;
    }

    public void setCurrentInstituicao(InstituicaoDTO currentInstituicao) {
        this.currentInstituicao = currentInstituicao;
    }

    public CandidaturaDTO getCurrentCandidatura() {
        return currentCandidatura;
    }

    public void setCurrentCandidatura(CandidaturaDTO currentCandidatura) {
        this.currentCandidatura = currentCandidatura;
    }

    public TrabalhoDTO getCurrentTrabalho() {
        return currentTrabalho;
    }

    public void setCurrentTrabalho(TrabalhoDTO currentTrabalho) {
        this.currentTrabalho = currentTrabalho;
    }

    public ProvaPublicaDTO getNewProvaPublica() {
        return newProvaPublica;
    }

    public void setNewProvaPublica(ProvaPublicaDTO newProvaPublica) {
        this.newProvaPublica = newProvaPublica;
    }

    public ProvaPublicaDTO getCurrentProvaPublica() {
        return currentProvaPublica;
    }

    public void setCurrentProvaPublica(ProvaPublicaDTO currentProvaPublica) {
        this.currentProvaPublica = currentProvaPublica;
    }
}
