/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.ManageBeans;

import java.util.logging.Logger;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

@ManagedBean
@SessionScoped
public class ConnectionsManagerBean implements Serializable {

    protected Client client;
    protected HttpAuthenticationFeature feature;

    private static final Logger logger
            = Logger.getLogger("web.ConnectionsManagedBean");

    @ManagedProperty(value = "#{loginManagedBean}")
    protected LoginManagedBean loginManagedBean;

    // #######################################################################
    public ConnectionsManagerBean() {
        client = ClientBuilder.newClient();
    }

    // #######################################################################
    @PostConstruct
    public void initClient() {

        feature = HttpAuthenticationFeature
                .basic(
                        loginManagedBean.getUsername(),
                        loginManagedBean.getPassword()
                );

        client.register(feature);

    }

    // #######################################################################
    public LoginManagedBean getLoginManagedBean() {
        return loginManagedBean;
    }

    public void setLoginManagedBean(LoginManagedBean loginManagedBean) {
        this.loginManagedBean = loginManagedBean;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
