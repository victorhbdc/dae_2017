package web.ManageBeans;

import dtos.DocumentoDTO;
import dtos.TrabalhoDTO;
import ejbs.TrabalhoBean;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import util.URILookup;
import web.FacesExceptionHandler;

@ManagedBean
public class DownloadManager implements Serializable {

    private Response response;
    protected Client client;

    String trabalhoId;
    String documentoNome;

    StreamedContent fileToDownload;

    private UIComponent component;

    public DownloadManager() {
        client = ClientBuilder.newClient();
    }

    public StreamedContent getFile(DocumentoDTO documentoDTO) throws FileNotFoundException {

        InputStream in;

        try {
            in = new FileInputStream(documentoDTO.getPath());
        } catch (FileNotFoundException ex) {
            throw new FileNotFoundException("Ficheiro não disponível");
        }

        return new DefaultStreamedContent(in, documentoDTO.getMimeType(), documentoDTO.getNome());

    }

    /**
     * gets the trabalho ID and the document ID from the url needed parms:
     * trabalhoid & documentoNome
     *
     * @return
     * @throws FileNotFoundException
     */
    public StreamedContent getFile() throws FileNotFoundException {

        if (trabalhoId == null
                || trabalhoId.isEmpty()) {
            throw new IllegalArgumentException("ID do trabalho não encontrado");
        }

        if (documentoNome == null
                || documentoNome.isEmpty()) {
            throw new IllegalArgumentException("Nome do Documento não encontrado");
        }

        response = client.target(URILookup.getBaseAPI())
                .path(TrabalhoBean.REST_PATH_GET_TRABALHO(trabalhoId))
                .request(MediaType.APPLICATION_XML)
                .get();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {

            FacesExceptionHandler.handleException(response, "Erro ao tentar descarregar o ficheiro",
                    component, null);

            return null;
        }

        TrabalhoDTO trabalhoDTO = response.readEntity(TrabalhoDTO.class);

        for (DocumentoDTO d : trabalhoDTO.getArtefactos()) {
            if (d.getNome().equalsIgnoreCase(documentoNome)) {
                return getFile(d);
            }
        }

        throw new FileNotFoundException("Ficheiro não disponível");
    }


    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    private Exception NenhumUserAutenticadoException() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getTrabalhoId() {
        return trabalhoId;
    }

    public void setTrabalhoId(String trabalhoId) {
        this.trabalhoId = trabalhoId;
    }

    public String getDocumentoNome() {
        return documentoNome;
    }

    public void setDocumentoNome(String documentoNome) {
        this.documentoNome = documentoNome;
    }

    public StreamedContent getFileToDownload() {
        return fileToDownload;
    }

    public void setFileToDownload(StreamedContent fileToDownload) {
        this.fileToDownload = fileToDownload;
    }

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    
}
