/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.ManageBeans;

import dtos.InstituicaoDTO;
import dtos.ProfessorDTO;
import dtos.ProponenteDTO;
import dtos.PropostaDTO;
import dtos.SupervisorDTO;
import ejbs.ConfigBean;
import ejbs.InstituicaoBean;
import ejbs.ProfessorBean;
import ejbs.ProponenteBean;
import ejbs.PropostaBean;
import ejbs.SupervisorBean;
import java.util.logging.Logger;
import web.FacesExceptionHandler;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.faces.event.ActionEvent;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import util.URILookup;
import utils.TipoTrabalho;

/**
 *
 * @author vsc
 */
@ManagedBean
@SessionScoped
public class ProponenteManagedBean extends UserManagedBean<ProponenteDTO> implements Serializable {

    private UIComponent component;
    private Response response;
    private SupervisorDTO newSupervisor;
    private PropostaDTO currentProposta;
    private PropostaDTO newProposta;

    // #######################################################################
    public ProponenteManagedBean() {
        super(ProponenteDTO.class);
        this.logger = Logger.getLogger("web.ProponenteManagedBean");
        newSupervisor = new SupervisorDTO();
        currentProposta = new PropostaDTO();
        newProposta = new PropostaDTO();
    }
    
    // #######################################################################
    @Override
    protected String getRestPathToFindUser(String username) {
        return ProponenteBean.REST_PATH_GET_FIND(username);
    }

    // #######################################################################
    public Collection<PropostaDTO> getAllPropostasDoProponenteLoggado() {
        response = client.target(URILookup.getBaseAPI())
                    .path(ProponenteBean.REST_PATH_GET_PROPOSTAS(loginManagedBean.getUsername()))
                    .request(MediaType.APPLICATION_XML)
                    .get();
            
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar as propostas do proponente loggado!",
                    component,
                    logger
            );

        }
        List<PropostaDTO> dtos = response.readEntity(new GenericType<List<PropostaDTO>>() {});
        return dtos;
    }
    
    public String createProposta() {
        newProposta.setMestrado(ConfigBean.MESTRADO);
        
        newProposta.removeProponenteUsername(loginManagedBean.getUsername());
        
        newProposta.addProponenteUsername(loginManagedBean.getUsername());
        newProposta.setConcursoId(loginManagedBean.getLatestConcurso().getId());
        
        
        response = client.target(URILookup.getBaseAPI())
                .path(PropostaBean.REST_PATH_CREATE)
                .request(MediaType.APPLICATION_XML)
                .post(Entity.xml(newProposta));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response, "Erro ao tentar criar a proposta", 
                    component, logger);
            return null;
        }
        
        newProposta.reset();
        return "proponente_index?faces-redirect=true";
    }

    
    public String updateProposta() {
        currentProposta.setMestrado(ConfigBean.MESTRADO);
        
        currentProposta.removeProponenteUsername(loginManagedBean.getUsername());
        currentProposta.addProponenteUsername(loginManagedBean.getUsername());
        currentProposta.setConcursoId(loginManagedBean.getLatestConcurso().getId());
        
        response = client.target(URILookup.getBaseAPI())
                .path(PropostaBean.REST_PATH_UPDATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(currentProposta));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response, 
                    "Erro ao tentar atualizar a proposta!", 
                    component, logger);
            return null;
        }
        currentProposta.reset();
        return "proponente_index?faces-redirect=true";
    }
    
    public void removeProposta(ActionEvent event) {
        UIParameter param 
                = (UIParameter) event.getComponent().findComponent("propostaId");
        //propostaBean.remove((Long) param.getValue());
        String id = param.getValue().toString();

        response = client.target(URILookup.getBaseAPI())
                .path(PropostaBean.REST_PATH_DELETE(id))
                .request(MediaType.APPLICATION_XML)
                .delete();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao tentar remover a proposta",
                    component, logger);
        }
    }
    
    public List<TipoTrabalho> getAllTiposDeTrabalho() {
        try {
            return Arrays.asList(TipoTrabalho.values());
        } catch (Exception e) {
            FacesExceptionHandler.handleException(
                    e,
                    "Erro ao carregar os tipos de trabalho!",
                   component,  logger
            );
            return null;
        }
    }
    
    /*##################################################################
                                                            PROFESSORES
    #####################################################################*/
    
    public Collection<ProfessorDTO> getAllProfessores() {
        response = client.target(URILookup.getBaseAPI())
                .path(ProfessorBean.REST_PATH_GET_ALL)
                .request(MediaType.APPLICATION_XML)
                .get();

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar todos os professores!",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<ProfessorDTO>>() {});
    }
      /*##################################################################
                                                            SUPERVISORES
    #####################################################################*/
    public boolean isInstituicao(){
        try {
             InstituicaoDTO instDTO = client.target(URILookup.getBaseAPI())
                    .path(InstituicaoBean.REST_PATH_GET_FIND(loginManagedBean.getUsername()))
                    .request(MediaType.APPLICATION_XML)
                    .get(InstituicaoDTO.class);
             return instDTO != null;
        } catch (Exception e) {
            return false;
        }
    }
    
    public Collection<SupervisorDTO> getAllSupervisorsOfInstituicao(){
        response = client.target(URILookup.getBaseAPI())
                .path(SupervisorBean.REST_PATH_GET_ALL_DE_INSTITUICAO(loginManagedBean.getUsername()))
                .request(MediaType.APPLICATION_XML)
                .get();
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao carregar os supervisores da instituicao",
                    component, logger
            );
        }
        return response.readEntity(new GenericType<List<SupervisorDTO>>() {});
    } 
    
    public String createSupervisor() {
        newSupervisor.setInstituicaoUsername(loginManagedBean.getUsername());
        response = client.target(URILookup.getBaseAPI())
                .path(SupervisorBean.REST_PATH_PUT_CREATE)
                .request(MediaType.APPLICATION_XML)
                .put(Entity.xml(newSupervisor));

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            FacesExceptionHandler.handleException(
                    response,
                    "Erro ao criar supervisor!",
                    component, logger
            );
            return null; // todo erros nao aparecem : e.g. username ja existe ou mail 
        }
        
        newSupervisor.reset();
        return "proponente_index?faces-redirect=true";
    }
    
    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    public PropostaDTO getCurrentProposta() {
        return currentProposta;
    }

    public void setCurrentProposta(PropostaDTO currentProposta) {
        this.currentProposta = currentProposta;
    }

    public PropostaDTO getNewProposta() {
        return newProposta;
    }

    public void setNewProposta(PropostaDTO newProposta) {
        this.newProposta = newProposta;
    }

    public SupervisorDTO getNewSupervisor() {
        return newSupervisor;
    }

    public void setNewSupervisor(SupervisorDTO newSupervisor) {
        this.newSupervisor = newSupervisor;
    }
    
    
}
