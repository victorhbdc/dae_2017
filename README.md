# README #

### What is this repository for? ###

* DAE 2017 Project

### How do I get set up? ###

Database (Java DB)
1. Database name: DAE_PROJECT
2. User Name: dae
3. Password: dae
4. JNDI in the persistence.xml file: dae_project

jdbcRealm
1. Configurations -> server-config -> Security -> Realms -> New
2. Realm name: dae_project_realm
3. Class name: com.sun.enterprise.security.auth.realm.jdbc.JDBCRealm
4. JAAS Context: jdbcRealm
5. JNDI: dae_project
6. User Table: USERS
7. User Name Column: USERNAME
8. Password Column: PASSWORD
9. Group Table: USERS_GROUPS
10. Group Table User Name Column: USERNAME
11. Group Name Column: GROUP_NAME
12. Password Encryption Algorithm: SHA-256
13. Database User: dae
14. Database Password: dae
15. All the other fields remain empty.
16. Ok
